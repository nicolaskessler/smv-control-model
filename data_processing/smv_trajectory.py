#!/usr/bin/env python3

import argparse

import pandas as pd
import numpy as np
from matplotlib import pyplot as plt

FLAGS = []


def main():

    global FLAGS

    # Argument parsing
    parser = argparse.ArgumentParser()
    parser.add_argument(
        '--filename',
        type=str,
        default=None,
        required=True,
        help='File to plot')

    FLAGS = parser.parse_args()

    filename = FLAGS.filename

    df = pd.read_csv(filename, index_col="iteration")

    print(df.info)

    m = 25

    pos_x = df["output_pos_x"][m:]
    pos_y = df["output_pos_y"][m:]
    theta = df["output_theta"][m:]
    print(theta)

    def compute_front_pos(pos_x, pos_y, theta):
        # returns front_x, front_y
        n = pos_x.shape[0]
        front_x = np.zeros(n)
        front_y = np.zeros(n)
        r = 5

        for i in range(m, m + n):
            t = -theta[i] + np.pi
            front_x[i-m] = pos_x[i] + r * np.cos(t) - 0 * np.sin(t)
            front_y[i-m] = pos_y[i] + r * np.sin(t) + 0 * np.cos(t)

        return front_x, front_y
    
    front_x, front_y = compute_front_pos(pos_x, pos_y, theta)

    from matplotlib import rc
    rc('font', **{'family':'sans-serif','sans-serif':['Computer Modern']})
    rc('text', usetex=True)

    plt.plot(pos_x, pos_y, 'b', label="SMV COM Trajectory")
    plt.plot(pos_x, pos_y, 'xb')
    # for i in range(pos_x.shape[0]):
    #     plt.arrow(pos_x[20+i], pos_y[20+i], front_x[i] - pos_x[20+i], front_y[i] - pos_y[20+i], color='r')
    plt.plot(front_x, front_y, 'rx', label="SMV Front Trajectory")
    fig = plt.gcf()
    fig.canvas.set_window_title(filename)
    plt.xlabel('Star-Tracker X / mm')
    plt.ylabel('Star-Tracker Y / mm')
    plt.legend()
    plt.gca().invert_yaxis()
    plt.gca().set_aspect('equal', adjustable='box')
    plt.show(block=True)


if __name__ == '__main__':
    main()
