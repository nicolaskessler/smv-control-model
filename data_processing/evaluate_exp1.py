#!/usr/bin/env python3

import argparse

import numpy as np
import pandas as pd
from matplotlib import pyplot as plt
from scipy.optimize import curve_fit

import casadi as cs

from generated2.system_model_exp1 import next_state
from generated2.linearized_system_model_exp1 import linearized_next_state

FLAGS = []

T_PWM = 0.4


def find_exp1(df: pd.DataFrame) -> int:
    # length x 18
    data = df.to_numpy()
    length = data.shape[0]
    # search index 1, 2 and 3 for the pattern
    th_0 = data[:, 1]
    th_1 = data[:, 2]
    th_2 = data[:, 3]
    th_0_matched = np.isclose(th_0, [0.]*length)
    th_1_matched = np.isclose(th_1, [50.*T_PWM]*length)
    th_2_matched = np.isclose(th_2, [50.*T_PWM]*length)
    # iterate through the data
    # and break once the pattern matches
    for i in range(length):
        matched_0 = np.all(th_0_matched[i:i+29])
        matched_1 = np.all(th_1_matched[i:i+29])
        matched_2 = np.all(th_2_matched[i:i+29])
        if matched_0 and matched_1 and matched_2:
            return i
    raise("No Match for exp1 found!")


def fix_theta(output_theta, output_vel_theta):
     
    # identify theta jump indices
    # fix theta
    n = output_theta.shape[0]
    jumps = np.zeros(n)
    for i in range(n-2):
        if (abs(output_theta[i] - output_theta[i+1]) >= 6):
            jumps[i+1] = True
            # identify the correction of the jump
            delta = 0
            if(output_theta[i+1] - output_theta[i] > 0):
                # theta jumped from pi-epsl to 0+epsr
                delta = -2*np.pi
            else:
                # theta jumped from 0+epsl to pi-epsr
                delta = 2*np.pi
            # correct thetas
            for j in range(i+1, n):
                output_theta[j] += delta
    
    # fix velocity jumps
    for i in range(1, n):
        if(jumps[i]):
            # calculate new value
            old_v = output_vel_theta[i-1]
            next_v = output_vel_theta[i+1]
            output_vel_theta[i] = (old_v + next_v) / 2.

    return output_theta, output_vel_theta


def solve_weighted_thruster_nlp(n: float, x_m_k, u_k, weights):
    # weights: [n_data, n_states]
    # create the parameters
    f_thruster = cs.SX.sym("f_thruster")

    e_k = []
    e_list = []
    for i in range(n):
        elist = []
        for j in range(x_m_k.shape[1]):
            e = cs.SX.sym("e_{}_{}".format(i, j))
            elist.append(e)
            e_list.append(e)
        e_k.append(elist)

    # formulate the optimization problem

    objective = 0
    for i in range(0, n):
        for j in range(x_m_k.shape[1]):
            objective += e_k[i][j]**2 * weights[i][j]

    g_k = []
    # 1..n
    for i in range(1,n):
        e_i = e_k[i-1]
        print(e_i)
        e_nxt = e_k[i]
        print(e_nxt)
        u_i = u_k[i-1]
        print(u_i)
        x_i = [x_m_k[i-1, count] + e_i[count] for count in range(x_m_k.shape[1])]
        print(x_i)
        x_nxt = [x_m_k[i, count] + e_nxt[count] for count in range(x_m_k.shape[1])]
        print(x_nxt)

        g_i = linearized_next_state(x_i, u_i, f_thruster)
        for i, g in enumerate(g_i):
            g_k.append(x_nxt[i] - g)

    # forbid theta and pos_y to deviate
    # for i in range(30):
    #     g_k.append(e_k[i][2])

    g_k = cs.vcat(g_k)

    # solve it
    print(objective)
    #cs.nlpsol()
    nlp = {'x': cs.vcat(e_list + [f_thruster]), 'f': objective, 'g': g_k}
    S = cs.nlpsol('S', 'ipopt', nlp)

    # present the solution
    print(S)
    # x0=([0]*len(e_list) + [2]), .
    res = S(lbg=0, ubg=0)
    print(res["x"])
    print(objective)
    #cs.nlpsol()
    nlp = {'x': cs.vcat(e_list + [f_thruster]), 'f': objective, 'g': g_k}
    S = cs.nlpsol('S', 'ipopt', nlp)

    # present the solution
    print(S)
    # x0=([0]*len(e_list) + [2]), .
    res = S(x0=([0]*len(e_list) + [5]), lbg=0, ubg=0)
    print(res["x"])
    errors = cs.vertsplit(res["x"], 1)
    print(errors)
    print("errors in x:")
    e_x = errors[0:-1:8]
    print(np.array(e_x))

    f_thruster = res["x"][-1]
    print("f_thruster: {}".format(f_thruster))

    return f_thruster, np.array(e_x)


def solve_weighted_thruster_miso_ls(n: float, x_m_k, u_k):
    # weights = [[1, 0, 0, 0, 0, 0, 0, 0]] * 30
    # for i in range(3):
    #     f_thruster, e_x = solve_weighted_thruster_nlp(n, x_m_k, u_k, weights)
    #     #print(e_x)
    #     E = np.outer(e_x, e_x)
    #     #print(E)
    #     E = np.linalg.inv(E)
    #     #print(E)
    #     w, _ = np.linalg.eig(E)
    #     #print(w)
    #     w_min = np.min(w)
    #     E = E - np.ones((n, n)) * (np.real(w_min) - 1)
    #     # adjust weights
    #     weights = []
    #     for j in range(n):
    #         w_i = [E[j,j],0,0,0,0,0,0,0]
    #         weights.append(w_i)
    #     print("f_thruster")
    #     print(f_thruster)
    # print(u_k)
    # print(np.expand_dims(u_k[:-1,1],1))
    print("xdata")
    xdata = np.concatenate((x_m_k[:-1,0],x_m_k[:-1,4],np.expand_dims(u_k[:-1,1],1)),axis=1)
    print(xdata)
    print("ydata")
    ydata_p = np.squeeze(np.asarray(x_m_k[0:,0]))
    ydata_v = np.squeeze(np.asarray(x_m_k[0:,4]))
    ydata_e_p = np.zeros(30)
    ydata_e_v = np.zeros(30)
    ydata = np.concatenate([ydata_p, ydata_v, ydata_e_p, ydata_e_v])
    print(ydata)
    print(ydata_p.shape)
    print(ydata.shape)
    # print(xdata)
    # print("ydata")
    # print(ydata.shape)
    # print(ydata)

    def siso_next_state(xdata, *args):
    #     print("ns")
    #     print(xdata)
    #     print(xdata.shape)
        e_p = args[0:30]
        e_v = args[30:60]
        thruster_force = args[60]
        res_p = []
        res_v = []
        #initial state x_0 position needs to be fixed
        res_p.append(xdata[0, 0] + e_p[0])
        res_v.append(xdata[0, 1] + e_v[0])
        for i in range(xdata.shape[0]):
            pos_x = xdata[i, 0]
            vel_x = xdata[i, 1]
            u_k = xdata[i, 2]
            state = [pos_x + e_p[i], 0, 0, 0, vel_x + e_v[i], 0, 0, 0, 0]
            # print(state)
            u_k = [0, u_k, u_k]
            # print(u_k)
            x_nxt = linearized_next_state(state, u_k, [thruster_force])
            # print(x_nxt)
            res_p.append(x_nxt[0] + e_p[i+1])
            res_v.append(x_nxt[4] + e_v[i+1])
        res = res_p + res_v + list(e_p) + list(e_v)
        # print(state)
        # print(u_k)
        # print(thruster_force)
        # print(x_nxt)
        # print(np.array(res))
        return np.array(res)

    p_opt, p_cov = curve_fit(siso_next_state, xdata, ydata, p0=np.zeros(61))
    f_thruster = p_opt[-1]
    print(p_opt)
    print(p_cov)
    p_opt, p_cov = curve_fit(siso_next_state, xdata, ydata, p0=np.zeros(61), absolute_sigma=True)
    f_thruster = p_opt[-1]
    print(p_opt)
    print(p_cov)
    return f_thruster


def main():

    global FLAGS

    # Argument parsing
    parser = argparse.ArgumentParser()
    parser.add_argument(
        '--filename',
        type=str,
        default=None,
        required=True,
        help='Logfile')

    FLAGS = parser.parse_args()

    filename = FLAGS.filename

    df = pd.read_csv(filename, index_col="iteration")

    print(df.info)

    # locate experiment 1

    exp1_index = find_exp1(df)
    print(exp1_index)
    exp1_df = df[exp1_index: exp1_index + 30]

    # plot the data

    exp1_df.plot(subplots=True, sharex=True, use_index=True)
    fig = plt.gcf()
    fig.canvas.set_window_title(filename)
    plt.show(block=True)

    # extract the relevant columns

    # u_k, the inputs
    input_th_time_0 = exp1_df["input_th_time_0"].to_numpy() / 50.
    input_th_time_1 = exp1_df["input_th_time_1"].to_numpy() / 50.
    input_th_time_2 = exp1_df["input_th_time_2"].to_numpy() / 50.
    u_k = np.stack([input_th_time_0, input_th_time_1, input_th_time_2], axis=1)
    print(u_k.shape)

    # x_m_k, the measured states
    output_pos_x = exp1_df["output_pos_x"].to_numpy() / 1000.
    output_pos_y = - 1 * exp1_df["output_pos_y"].to_numpy() / 1000.
    output_theta = exp1_df["output_theta"].to_numpy() + np.pi # From star tracker
    output_alpha = np.zeros(output_theta.shape)
    output_vel_x = exp1_df["output_vel_x"].to_numpy() / 1000.
    output_vel_y = - 1 * exp1_df["output_vel_y"].to_numpy() / 1000.
    output_vel_theta = exp1_df["output_vel_theta"].to_numpy()
    output_vel_alpha = exp1_df["output_motor_speed"].to_numpy() * (-2.*np.pi / 60.)
    output_theta, output_vel_theta = fix_theta(output_theta, output_vel_theta)
    x_m_k = np.stack([output_pos_x, output_pos_y, output_theta, output_alpha, output_vel_x, output_vel_y, output_vel_theta, output_vel_alpha], axis=1)
    print(x_m_k.shape)

    # analyze and print mass

    # condition the data
    # theta is assumed to be constant
    # theta also indicates the direction of the smv
    # first include theta in the optimization and provide good initial guess

    # create the parameters
    f_thruster = cs.SX.sym("f_thruster")

    e_k = []
    e_list = []
    for i in range(30):
        elist = []
        for j in range(x_m_k.shape[1]):
            e = cs.SX.sym("e_{}_{}".format(i, j))
            elist.append(e)
            e_list.append(e)
        e_k.append(elist)

    # formulate the optimization problem

    objective = 0
    # weights = [10, 10, 0.1 , 10, 10, 0.1]
    weights = [1, 1, 1, 0, 0, 0, 0, 1]
    for i in range(0, 30):
        for j in range(x_m_k.shape[1]):
            objective += e_k[i][j]**2 * weights[j]

    g_k = []
    # 1..30
    for i in range(1,30):
        e_i = e_k[i-1]
        print(e_i)
        e_nxt = e_k[i]
        print(e_nxt)
        u_i = u_k[i-1]
        print(u_i)
        x_i = [x_m_k[i-1, count] + e_i[count] for count in range(x_m_k.shape[1])]
        print(x_i)
        x_nxt = [x_m_k[i, count] + e_nxt[count] for count in range(x_m_k.shape[1])]
        print(x_nxt)

        g_i = next_state(x_i, u_i, f_thruster)
        for i, g in enumerate(g_i):
            g_k.append(x_nxt[i] - g)

    g_k = cs.vcat(g_k)
    print(g_k[0])

    # solve it
    print(objective)
    #cs.nlpsol()
    nlp = {'x': cs.vcat(e_list + [f_thruster]), 'f': objective, 'g': g_k}
    S = cs.nlpsol('S', 'ipopt', nlp)

    # present the solution
    print(S)
    # x0=([0]*len(e_list) + [2]), .
    res = S(x0=([0]*len(e_list) + [5]), lbg=0, ubg=0)
    print(res["x"])
    errors = cs.vertsplit(res["x"], 1)
    print(errors)
    print(errors[2:-1:8])

    print(output_theta)
    print(np.array(errors[2:-1:8]))
    print(output_theta + np.array(errors[2:-1:8]))

    print(output_alpha)
    print(np.array(errors[3:-1:8]))
    print(output_alpha + np.array(errors[3:-1:8]))

    theta = (output_theta + np.array(errors[2:-1:8]))[0]
    print("Theta: {}".format(theta))

    alpha = (output_alpha + np.array(errors[3:-1:8]))[0]
    print("Alpha: {}".format(alpha))

    f_thruster = res["x"][-1]
    print("f_thruster: {}".format(f_thruster))

    # Rotate coordinates? Yes, then we have true SISO
    # NLP solution already makes sure that we can ignore the trajectory offset

    T = np.matrix([[np.cos(theta), np.sin(theta)],[-np.sin(theta), np.cos(theta)]])
    # output_pos_x
    # output_pos_y
    transformed_pos = np.stack([T@np.array([output_pos_x[i], output_pos_y[i]]) for i in range(30)], axis=0)
    # print(transformed_pos)
    transformed_vel = np.stack([T@np.array([output_vel_x[i], output_vel_y[i]]) for i in range(30)], axis=0)
    # print(transformed_vel)
    # print(transformed_pos[:, 0].shape)
    # print(transformed_vel[:, 0].shape)
    # print(transformed_pos[:, 1].shape)
    # print(transformed_vel[:, 1].shape)
    transformed_vel_theta = np.zeros((30, 1))
    transformed_theta = np.zeros((30, 1))
    transformed_vel_alpha = np.zeros((30, 1))
    transformed_alpha = np.zeros((30, 1))
    # print(transformed_alpha.shape)
    x_m_k = np.concatenate([
        transformed_pos[:, 0], 
        transformed_pos[:, 1], 
        transformed_theta, 
        transformed_alpha, 
        transformed_vel[:, 0], 
        transformed_vel[:, 1], 
        transformed_vel_theta, 
        transformed_vel_alpha], axis=1)

    weights = [[1, 0, 0, 0, 0, 0, 0, 0]] * 30
    f_thruster, e_x = solve_weighted_thruster_nlp(30, x_m_k, u_k, weights)
    f_thruster_scipy = solve_weighted_thruster_miso_ls(30, x_m_k, u_k)
    print("f_thruster: {}".format(f_thruster))
    print("f_thruster with scipy: {}".format(f_thruster_scipy))

if __name__ == '__main__':
    main()
