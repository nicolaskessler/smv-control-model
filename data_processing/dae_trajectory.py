#!/usr/bin/env python3

import argparse

import pandas as pd
from matplotlib import pyplot as plt

FLAGS = []


def main():

    global FLAGS

    # Argument parsing
    parser = argparse.ArgumentParser()
    parser.add_argument(
        '--filename',
        type=str,
        default=None,
        required=True,
        help='File to plot')

    FLAGS = parser.parse_args()

    filename = FLAGS.filename

    df = pd.read_csv(filename, index_col="iteration")

    print(df.info)

    m = 200

    x1 = df["qx_1"][20:-m]
    y1 = df["qy_1"][20:-m]
    t1 = df["qtheta_1"][20:-m]

    x2 = df["qx_2"][20:-m]
    y2 = df["qy_2"][20:-m]
    t2 = df["qtheta_2"][20:-m]

    from matplotlib import rc
    rc('font', **{'family':'sans-serif','sans-serif':['Computer Modern']})
    rc('text', usetex=True)

    plt.plot(x1, y1, 'b', label="SMV 1 COM Trajectory")
    # plt.plot(x1, y1, 'xb')
    plt.plot(x2, y2, 'r', label="SMV 2 COM Trajectory")
    # plt.plot(x2, y2, 'xr')
    plt.scatter((x1+x2)/2, (y1+y2)/2, marker='x', color='orange', label="Docking Port Trajectory")
    # for i in range(pos_x.shape[0]):
    #     plt.arrow(pos_x[20+i], pos_y[20+i], front_x[i] - pos_x[20+i], front_y[i] - pos_y[20+i], color='r')
    fig = plt.gcf()
    fig.canvas.set_window_title(filename)
    plt.xlabel('Star-Tracker X / mm')
    plt.ylabel('Star-Tracker Y / mm')
    plt.legend()
    plt.gca().set_aspect('equal', adjustable='box')
    plt.show(block=True)


if __name__ == '__main__':
    main()
