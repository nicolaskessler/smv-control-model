#!/usr/bin/env python3

import argparse

import pandas as pd
from matplotlib import pyplot as plt

FLAGS = []


def main():

    global FLAGS

    # Argument parsing
    parser = argparse.ArgumentParser()
    parser.add_argument(
        '--filename',
        type=str,
        default=None,
        required=True,
        help='File to plot')

    FLAGS = parser.parse_args()

    filename = FLAGS.filename

    df = pd.read_csv(filename, index_col="iteration")

    print(df.info)

    df.plot(subplots=True, sharex=True, use_index=True)
    fig = plt.gcf()
    fig.canvas.set_window_title(filename)
    plt.show(block=True)


if __name__ == '__main__':
    main()
