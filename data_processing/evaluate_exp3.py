#!/usr/bin/env python3

import argparse

import numpy as np
import pandas as pd
from matplotlib import pyplot as plt
from scipy.optimize import curve_fit

import casadi as cs

from generated2.system_model_exp3 import next_state

FLAGS = []

RPM = 8000


def find_exp3(df: pd.DataFrame, n) -> int:
    # length x 18
    data = df.to_numpy()
    length = data.shape[0]
    # search index 1, 2 and 3 for the pattern
    rpm = data[:, 6]
    rpm_matched = np.isclose(rpm, [RPM]*length)
    # iterate through the data
    # and break once the pattern matches
    for i in range(length):
        matched_0 = np.all(rpm_matched[i:i+n])
        if matched_0:
            return i
    raise("No Match for exp1 found!")


def fix_theta(output_theta, output_vel_theta):
     
    # identify theta jump indices
    # fix theta
    n = output_theta.shape[0]
    jumps = np.zeros(n)
    for i in range(n-2):
        if (abs(output_theta[i] - output_theta[i+1]) >= 6):
            jumps[i+1] = True
            # identify the correction of the jump
            delta = 0
            if(output_theta[i+1] - output_theta[i] > 0):
                # theta jumped from pi-epsl to 0+epsr
                delta = -2*np.pi
            else:
                # theta jumped from 0+epsl to pi-epsr
                delta = 2*np.pi
            # correct thetas
            for j in range(i+1, n):
                output_theta[j] += delta
    
    # fix velocity jumps
    for i in range(1, n):
        if(jumps[i]):
            # calculate new value
            old_v = output_vel_theta[i-1]
            next_v = output_vel_theta[i+1]
            output_vel_theta[i] = (old_v + next_v) / 2.

    return output_theta, output_vel_theta


def solve_weighted_thruster_nlp(n: float, x_m_k, u_k, weights, f_thruster, j_smv):
    # weights: [n_data, n_states]
    # create the parameters
    t_rwmax = cs.SX.sym("T_rwmax")
    mu_rw = cs.SX.sym("mu_rw")

    e_k = []
    e_list = []
    for i in range(n):
        elist = []
        for j in range(x_m_k.shape[1]):
            e = cs.SX.sym("e_{}_{}".format(i, j))
            elist.append(e)
            e_list.append(e)
        e_k.append(elist)

    # formulate the optimization problem

    objective = 0
    for i in range(0, n):
        for j in range(x_m_k.shape[1]):
            objective += e_k[i][j]**2 * weights[i][j]

    g_k = []
    # 1..n
    for i in range(1,n):
        e_i = e_k[i-1]
        e_nxt = e_k[i]
        u_i = u_k[i-1]
        x_i = [x_m_k[i-1, count] + e_i[count] for count in range(x_m_k.shape[1])]
        x_nxt = [x_m_k[i, count] + e_nxt[count] for count in range(x_m_k.shape[1])]

        g_i = next_state(x_i, u_i, [f_thruster, t_rwmax, mu_rw])
        for i, g in enumerate(g_i):
            g_k.append(x_nxt[i] - g)

    g_k = cs.vcat(g_k)

    # solve it
    #cs.nlpsol()
    nlp = {'x': cs.vcat(e_list + [t_rwmax, mu_rw]), 'f': objective, 'g': g_k}
    S = cs.nlpsol('S', 'ipopt', nlp)

    # present the solution
    print(S)
    # x0=([0]*len(e_list) + [2]), .
    res = S(x0=([0]*len(e_list) + [0.3] + [0]), lbg=0, ubg=0)
    print(res["x"])
    errors = cs.vertsplit(res["x"], 1)

    t_rwmax = res["x"][-2]
    mu_rw = res["x"][-1]
    print("t_rwmax: {}".format(t_rwmax))
    print("mu_rw: {}".format(mu_rw))

    return t_rwmax, mu_rw


def main():

    global FLAGS

    # Argument parsing
    parser = argparse.ArgumentParser()
    parser.add_argument(
        '--filename',
        type=str,
        default=None,
        required=True,
        help='Logfile')

    FLAGS = parser.parse_args()

    filename = FLAGS.filename

    df = pd.read_csv(filename, index_col="iteration")

    print(df.info)

    # locate experiment 1

    num = 600
    exp3_index = find_exp3(df, num)
    print(exp3_index)
    num = 300
    exp3_df = df[exp3_index-100: exp3_index-100 + num]

    # plot the data

    exp3_df.plot(subplots=True, sharex=True, use_index=True)
    fig = plt.gcf()
    fig.canvas.set_window_title(filename)
    plt.show(block=True)

    # extract the relevant columns

    # x_m_k, the measured states
    output_pos_x = exp3_df["output_pos_x"].to_numpy() / 1000.
    output_pos_y = - 1 * exp3_df["output_pos_y"].to_numpy() / 1000.
    output_theta = exp3_df["output_theta"].to_numpy() + np.pi # From star tracker
    output_alpha = np.zeros(output_theta.shape)
    output_vel_x = exp3_df["output_vel_x"].to_numpy() / 1000.
    output_vel_y = - 1 * exp3_df["output_vel_y"].to_numpy() / 1000.
    output_vel_theta = exp3_df["output_vel_theta"].to_numpy()
    output_vel_alpha = exp3_df["output_motor_speed"].to_numpy() * (-2.*np.pi / 60.)

    output_pos_x = np.zeros(output_theta.shape)
    output_pos_y = np.zeros(output_theta.shape)
    output_vel_x = np.zeros(output_theta.shape)
    output_vel_y = np.zeros(output_theta.shape)

    output_theta, output_vel_theta = fix_theta(output_theta, output_vel_theta)

    x_m_k = np.stack([output_pos_x, output_pos_y, output_theta, output_alpha, output_vel_x, output_vel_y, output_vel_theta, output_vel_alpha], axis=1)
    print(x_m_k.shape)

    # estimate the controller output
    controller_kp = 1
    input_rpm = exp3_df["input_rpm"].to_numpy()
    output_motor_speed_raw = exp3_df["output_motor_speed"].to_numpy()
    input_rw_motor = controller_kp * (input_rpm - output_motor_speed_raw) / 1000.
    input_rw_motor = np.clip(input_rw_motor, -1., 1.)
    u_k = np.stack([input_rw_motor], axis=1)
    print(input_rw_motor)
    print(output_theta)
    print(output_vel_alpha)
    print(u_k.shape)

    weights = [[0, 0, 1, 0, 0, 0, 0, 0.001]] * num
    # TODO: set f_thruster
    t_rwmax, mu_rw = solve_weighted_thruster_nlp(num, x_m_k, u_k, weights, 7.359, 0.099)
    print("t_rwmax: {}".format(t_rwmax))
    print("mu_rw: {}".format(mu_rw))

if __name__ == '__main__':
    main()
