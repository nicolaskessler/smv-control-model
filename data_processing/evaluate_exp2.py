#!/usr/bin/env python3

import argparse

import numpy as np
import pandas as pd
from matplotlib import pyplot as plt
from scipy.optimize import curve_fit

import casadi as cs

from generated2.system_model_exp2 import next_state

FLAGS = []

T_PWM = 0.2


def find_exp2(df: pd.DataFrame, num: int) -> int:
    data = df.to_numpy()
    length = data.shape[0]
    # search index 1, 2 and 3 for the pattern
    th_0 = data[:, 1]
    th_1 = data[:, 2]
    th_2 = data[:, 3]
    th_0_matched = np.isclose(th_0, [50.*T_PWM]*length)
    th_1_matched = np.isclose(th_1, [0.]*length)
    th_2_matched = np.isclose(th_2, [50.*T_PWM]*length)
    print(th_0_matched)
    print(th_1_matched)
    print(th_2_matched)
    # iterate through the data
    # and break once the pattern matches
    for i in range(length):
        matched_0 = np.all(th_0_matched[i:i+num-1])
        matched_1 = np.all(th_1_matched[i:i+num-1])
        matched_2 = np.all(th_2_matched[i:i+num-1])
        if matched_0 and matched_1 and matched_2:
            return i
    raise("No Match for exp2 found!")


def fix_theta(output_theta, output_vel_theta):
     
    # identify theta jump indices
    # fix theta
    n = output_theta.shape[0]
    jumps = np.zeros(n)
    for i in range(n-2):
        if (abs(output_theta[i] - output_theta[i+1]) >= 6):
            jumps[i+1] = True
            # identify the correction of the jump
            delta = 0
            if(output_theta[i+1] - output_theta[i] > 0):
                # theta jumped from pi-epsl to 0+epsr
                delta = -2*np.pi
            else:
                # theta jumped from 0+epsl to pi-epsr
                delta = 2*np.pi
            # correct thetas
            for j in range(i+1, n):
                output_theta[j] += delta
    
    # fix velocity jumps
    for i in range(1, n):
        if(jumps[i]):
            # calculate new value
            old_v = output_vel_theta[i-1]
            next_v = output_vel_theta[i+1]
            output_vel_theta[i] = (old_v + next_v) / 2.

    return output_theta, output_vel_theta


def solve_weighted_thruster_nlp(n: float, x_m_k, u_k, weights, f_thruster):
    # weights: [n_data, n_states]
    # create the parameters
    j_smv = cs.SX.sym("j_smv")

    e_k = []
    e_list = []
    for i in range(n):
        elist = []
        for j in range(x_m_k.shape[1]):
            e = cs.SX.sym("e_{}_{}".format(i, j))
            elist.append(e)
            e_list.append(e)
        e_k.append(elist)

    # formulate the optimization problem

    objective = 0
    for i in range(0, n):
        for j in range(x_m_k.shape[1]):
            objective += e_k[i][j]**2 * weights[i][j]

    g_k = []
    # 1..n
    for i in range(1,n):
        e_i = e_k[i-1]
        print(e_i)
        e_nxt = e_k[i]
        print(e_nxt)
        u_i = u_k[i-1]
        print(u_i)
        x_i = [x_m_k[i-1, count] + e_i[count] for count in range(x_m_k.shape[1])]
        print(x_i)
        x_nxt = [x_m_k[i, count] + e_nxt[count] for count in range(x_m_k.shape[1])]
        print(x_nxt)

        g_i = next_state(x_i, u_i, [f_thruster, j_smv])
        for i, g in enumerate(g_i):
            g_k.append(x_nxt[i] - g)

    g_k = cs.vcat(g_k)

    # solve it
    print(objective)
    #cs.nlpsol()
    nlp = {'x': cs.vcat(e_list + [j_smv]), 'f': objective, 'g': g_k}
    S = cs.nlpsol('S', 'ipopt', nlp)

    # present the solution
    print(S)
    # x0=([0]*len(e_list) + [2]), .
    res = S(x0=([0]*len(e_list) + [0.1]), lbg=0, ubg=0)
    print(res["x"])
    errors = cs.vertsplit(res["x"], 1)

    j_smv = res["x"][-1]
    print("j_smv: {}".format(j_smv))

    return j_smv


def main():

    global FLAGS

    # Argument parsing
    parser = argparse.ArgumentParser()
    parser.add_argument(
        '--filename',
        type=str,
        default=None,
        required=True,
        help='Logfile')

    FLAGS = parser.parse_args()

    filename = FLAGS.filename

    df = pd.read_csv(filename, index_col="iteration")

    print(df.info)

    # locate experiment 2

    num = 39
    exp2_index = find_exp2(df, num)
    num *= 2
    print(exp2_index)
    exp2_df = df[exp2_index: exp2_index + num]

    # plot the data

    exp2_df.plot(subplots=True, sharex=True, use_index=True)
    fig = plt.gcf()
    fig.canvas.set_window_title(filename)
    plt.show(block=True)

    # extract the relevant columns

    # u_k, the inputs
    input_th_time_0 = exp2_df["input_th_time_0"].to_numpy() / 50.
    input_th_time_1 = exp2_df["input_th_time_1"].to_numpy() / 50.
    input_th_time_2 = exp2_df["input_th_time_2"].to_numpy() / 50.
    u_k = np.stack([input_th_time_0, input_th_time_1, input_th_time_2], axis=1)
    print(u_k.shape)

    # x_m_k, the measured states
    output_pos_x = exp2_df["output_pos_x"].to_numpy() / 1000.
    output_pos_y = - 1 * exp2_df["output_pos_y"].to_numpy() / 1000.
    output_theta = exp2_df["output_theta"].to_numpy() + np.pi # From star tracker
    output_alpha = np.zeros(output_theta.shape)
    output_vel_x = exp2_df["output_vel_x"].to_numpy() / 1000.
    output_vel_y = - 1 * exp2_df["output_vel_y"].to_numpy() / 1000.
    output_vel_theta = exp2_df["output_vel_theta"].to_numpy()
    output_vel_alpha = exp2_df["output_motor_speed"].to_numpy() * (-2.*np.pi / 60.)

    output_pos_x = np.zeros(output_theta.shape)
    output_pos_y = np.zeros(output_theta.shape)
    output_vel_x = np.zeros(output_theta.shape)
    output_vel_y = np.zeros(output_theta.shape)
    output_vel_alpha = np.zeros(output_theta.shape)

    # fix theta
    print(output_theta)
    output_theta, output_vel_theta = fix_theta(output_theta, output_vel_theta)
    print(output_theta)
    print(output_vel_theta)

    x_m_k = np.stack([output_pos_x, output_pos_y, output_theta, output_alpha, output_vel_x, output_vel_y, output_vel_theta, output_vel_alpha], axis=1)
    print(x_m_k.shape)

    weights = [[0, 0, 1, 0, 0, 0, 0, 0]] * num
    # TODO: set f_thruster
    j_smv = solve_weighted_thruster_nlp(num, x_m_k, u_k, weights, 7.359)
    print("j_smv: {}".format(j_smv))


if __name__ == '__main__':
    main()
