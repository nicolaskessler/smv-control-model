/*****************************************************************
topics.cpp

Original Created by: Atheel Redah @ University of Würzburg
Original Creation Date: March 8, 2015

Development environment specifics:
	Software Platform: Rodos (Realtime Onboard Dependable Operating System).
	Hardware Platform: STM32F4 + Würzburg Uni Informatik 8 Discovery AddOn Board Version 2.0.
*****************************************************************/

#include "rodos.h"
#include "topics.h"

Topic<sObstacle> ObstacleTopic(1506,"Abstract Obstacle Points");
Topic<sTelecommandData> TelecommandDataTopic(1500,"Telecommand Data");
Topic<sSensorData> SensorDataTopic(1505,"Sensor Data");
Topic<sControl> ControlTopic(1507,"Control Data");

Topic<sInitComp> InitCompTopic(1501,"Component Data");
Topic<sVehicleNet> VehicleNetTopic(1502,"Telecommand Data");

Topic<sPositionData> PositionDataTopic(1503,"Position Data");
Topic<sPositionDataVehicle2> PositionDataVehicle2Topic(1508,"Position Data Vehicle 2");
Topic<sIMUData> IMUDataTopic(1504,"IMU Data");
Topic<sDestinationData> DestinationTopic(1509,"Destination Data");
Topic<sHouseKeeping> HouseKeepingTopic(1510, "House Keeping");
Topic<sHKOtherVehicle> HKOtherVehicleTopic(1511, "HK from other FloatSat");
Topic<sTargetChaser> TargetChaserTopic(1512,"Am I Target or Chaser?");


//////////////////////////


Topic<sObstacle> ObstacleTopic2(1606,"Abstract Obstacle Points");
Topic<sTelecommandData> TelecommandDataTopic2(1600,"Telecommand Data");
Topic<sSensorData> SensorDataTopic2(1605,"Sensor Data");
Topic<sControl> ControlTopic2(1607,"Control Data");

Topic<sInitComp> InitCompTopic2(1601,"Component Data");
Topic<sVehicleNet> VehicleNetTopic2(1602,"Telecommand Data");

Topic<sPositionData> PositionDataTopic2(1603,"Position Data");
Topic<sPositionDataVehicle2> PositionDataVehicle2Topic2(1608,"Position Data Vehicle 2");
Topic<sIMUData> IMUDataTopic2(1604,"IMU Data");
Topic<sDestinationData> DestinationTopic2(1609,"Destination Data");
Topic<sHouseKeeping> HouseKeepingTopic2(1610, "House Keeping");
Topic<sHKOtherVehicle> HKOtherVehicleTopic2(1611, "HK from other FloatSat");
Topic<sTargetChaser> TargetChaserTopic2(1612,"Am I Target or Chaser?");
