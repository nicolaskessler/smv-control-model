/**************************************************************//**
*topics.h
*
* Original Created by: Atheel Redah @ University of Wrzburg
* Original Creation Date: March 8, 2015
* Extended by TEAM Floatsat Feb 2020
*
* Development environment specifics:
* Software Platform: Rodos (Realtime Onboard Dependable Operating System).
* Hardware Platform: STM32F4 + Wrzburg Uni Informatik 8 Discovery AddOn Board Version 2.0.
*****************************************************************/

#ifndef __topics_h__
#define __topics_h__

/* Includes ------------------------------------------------------------------*/
#include "matlib.h"
// #include "Misc/datatypes_RODOS.h"

/* Define Debug --------------------------------------------------------------*/
#define DEBUG

#define MAX_SIZE 100;	// FIXME Jonas : is someone else using this?
/* Exported types ------------------------------------------------------------*/

/**
 * Struct for general system properties like the different internal Mode of different Threads
 * and Thread periode
 */
struct sTelecommandData
{
	bool Telemetry;
	uint8_t SystemMode; ///< define the actual state of the Modes Thread (Modes.cpp)
	uint8_t SensorMode; ///< define the actual state of the Sensor Thread (Sensor.cpp)
	uint64_t ThreadPeriod; ///< define the Thread periode for all Threads
	uint8_t WiFiMode; ///< define the actual state of the SendingWifi Thread (WifiTelemetry.cpp)
	int32_t testvalue; ///< general Testvalue, can be used for multiple purposes e.g. see WifiTelemetry.cpp
	bool master = false; /// define the hirarchy for the docking procedure, if true = master
	char sender[4]; ///< keeps the sender of a HK message
	char msg[180]; ///< Storage for general Messages
	char Error_msg[180]; ///< Storage for Error Messages to be forwarded to GS
};


// struct sTelecommandData
// {
// 	bool Telemetry;
// 	uint8_t SystemMode;
// 	uint8_t AHRSMode;
// 	uint8_t Controller;
// 	uint8_t ControllerParameter;
// 	float ControllerParameterGain;
// 	int32_t MotorSpeed;
// 	float Velocity;
// 	float Position;
// 	uint8_t DeploymentState;
// 	float DockingServo;
// 	float Servo[2];
// 	uint8_t TelecommandFlag;
// 	uint64_t ThreadPeriod;
// 	uint32_t PWMRes[2];
// 	float Bearings;
// 	float Thrusters;
// };


struct sSensorData{
	float motorSpeed;  // motor speed in RPM
	int scale;
	uint32_t Vbat;
};

// sampling rate: 50*MILLISECONDS
/**
 * Struct storing the parameter for the actuator like Thruster \n
 * should be used on the component SKITH
 */
struct sControl
{
	int32_t th_time[3];	///< time interval in which the valve of the thruster is open in ms // minimum is 10 ms
	int32_t thruster_theta[2]; // angle +/- 30 degrees for thrusters // 0 to point to COM, 30 for rendezvouz
	int32_t rpm;        ///< rotations per minute, implemented as PWM between 0..100
	int32_t rw_time; 	///< time interval in which the reaction wheel is active ()
	int8_t dock;	///< State of the Docking Port, 1 for docking (master), -1 for docking (slave), 0 for undocking
	uint16_t ambient1, ambient2; // variables necessary for atheels proximity sensor
	uint16_t range1, range2;
};

/**
 * Struct representing an obstacle Point,
 * each Point has coordinates and scaling factors
 */
struct sObstacle
{
	float x,y; ///< Coordinates of the Obstacle point
	float scale_x, scale_y; ///< scaling factor of the obstacle
};

/**
 * Struct containing the actual position  and Velocitydata
 * will be updated from sensors e.g. star tracker
 */
struct sPositionData
{
	float PosX;	///< given in mm
	float PosY;	///< given in mm
	float theta; ///< rad theta = greta
	float Velx; ///< given in mm/s?
	float Vely; ///< given in mm/s?
	float Veltheta; ///< given in rad/s?
};

/**
 * Struct containing the position data of the other vehicle
 * will be updated via telemetry
 */
struct sPositionDataVehicle2
{
	float PosX;	///< given in mm
	float PosY;	///< given in mm
	float theta; ///< rad theta = greta
};

/**
 * TODO
 * Struct containing the actual velocity data \n
 * will be updated from the IMU
 * ax / 9.81 * m/sec*sec
 * ay / 9.81 * m/sec*sec
 * az / 9.81 * m/sec*sec
 * gx deg/sec
 * gy deg/sec
 * gz deg/sec
 */
struct sIMUData
{
	float PosX;	///< given in mm
	float PosY;	///< given in mm
	float theta; ///< rad theta = greta
	float rwSpeed;  // motor speed in RPM
};

/**
 * Struct containing the actual destination data \n
 */
struct sDestinationData
{
	float PosX; ///< given in mm
	float PosY; ///< given in mm
	float theta; ///< rad theta = greta

	float dVelX; ///< desired velocity for direct controller input, given in msg
	float dVelY; ///< desired velocity for direct controller input, given in msg
};

/**
 * Struct containing component specific information like the component role
 * (possible roles: thruster [TH_n_], reaction wheel[RW],  docking port[DP], scu[SCU])
 * important for enforcing the role
 */
struct sInitComp
{
	char role[4] = {0}; ///< role of the component thruster [TH_n_], reaction wheel[RW],  docking port[DP], scu[SCU]
	int vehicleNr = 0;
	// todo brauchen wir die noch, check wo die noch benutzt werden..
	char ip_scu[16]  = {0}; // tested with pre-initialisation, reslut is the same -> data loss while publishing?! -> change in the IP adress
	uint32_t port_scu = 2001;
};

// should be overwritten for storing the postiion of other vehicles etc.
struct sVehicleNet
{
	double lastSCUsignal;

	double lastRWsignal;
	double lastTHFsignal;
	double lastTHLsignal;
	double lastTHRsignal;
	double lastDPLsignal;
	double lastDPRsignal;
};
/**
 * Struct containing Housekeeping data
 * @param TankValue gives the air in the tank in percent
 * @param BatteryVoltage gives the Voltage of the Battery
 */
struct sHouseKeeping
{
	int TankValue;
	float BatteryVoltage;
	int DockStatusLeft;
	int DockStatusRight;
};
/**
 * Struct containing Housekeeping data from other Vehicle
 * TankValue gives the air in the tank in percent
 * BatteryVoltage gives the Voltage of the Battery
 */
struct sHKOtherVehicle
{
	int TankValue;
	float BatteryVoltage;
	int DockStatusLeft;
	int DockStatusRight;
};
/**
 * Struct for storing if the vehicle is currently a target or a chaser
 */
struct sTargetChaser
{
	/*@{*/
	int Chaser; /**< if 1: Chaser, if 0: Target, if -1 undefined */
	/*@}*/
};

extern Topic<sObstacle> ObstacleTopic;
extern Topic<sTelecommandData> TelecommandDataTopic; // this is sent from ground to vehicle
extern Topic<sPositionData> PositionDataTopic; // received from on-board pi (star-tracker)
extern Topic<sPositionDataVehicle2> PositionDataVehicle2Topic;// received from another on-board pi (star-tracker)
extern Topic<sInitComp> InitCompTopic; // internal
extern Topic<sVehicleNet> VehicleNetTopic; // internal / for networking
extern Topic<sControl> ControlTopic; // internal, used by simulation
extern Topic<sIMUData> IMUDataTopic; // internal sensrodata from imu thread to another thread
extern Topic<sDestinationData> DestinationTopic; // check
extern Topic<sHouseKeeping> HouseKeepingTopic; // internal
extern Topic<sHKOtherVehicle> HKOtherVehicleTopic; // external power and fuel remaining
extern Topic<sTargetChaser> TargetChaserTopic;  // internal
//TODO: Check usage
//extern Topic<Telemetry> TM_T;
extern Topic<sSensorData> 		SensorDataTopic; 


/////////////////////////////


extern Topic<sObstacle> ObstacleTopic2;
extern Topic<sTelecommandData> TelecommandDataTopic2; // this is sent from ground to vehicle
extern Topic<sPositionData> PositionDataTopic2; // received from on-board pi (star-tracker)
extern Topic<sPositionDataVehicle2> PositionDataVehicle2Topic2;// received from another on-board pi (star-tracker)
extern Topic<sInitComp> InitCompTopic2; // internal
extern Topic<sVehicleNet> VehicleNetTopic2; // internal / for networking
extern Topic<sControl> ControlTopic2; // internal, used by simulation
extern Topic<sIMUData> IMUDataTopic2; // internal sensrodata from imu thread to another thread
extern Topic<sDestinationData> DestinationTopic2; // check
extern Topic<sHouseKeeping> HouseKeepingTopic2; // internal
extern Topic<sHKOtherVehicle> HKOtherVehicleTopic2; // external power and fuel remaining
extern Topic<sTargetChaser> TargetChaserTopic2;  // internal
//TODO: Check usage
//extern Topic<Telemetry> TM_T;
extern Topic<sSensorData> 		SensorDataTopic2; 

#endif
