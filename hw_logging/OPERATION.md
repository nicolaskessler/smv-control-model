# Operation Manual for HW Logging Module

# Content

* Supported Board
* Compiling and Flashing
* Wiring
* Test Actuation
* Logging Standalone
* Autopilot Standalone OR Combined Logging and Autopilot on one Board

# Supported Board

SKITH A connected with SKITH B. A SD-Card is required on SKITH-B.

# Compiling and Flashing

```
sudo apt install python3-vcstool
vcs import external < repos.txt
./combined.sh
./logger.sh
./pilot.sh
./test.sh
```

Patch RODOS in externa/rodos/cmake/port/skith.cmake line 2 to read "set(OSC_CLK 16000000)"

Check, if topics on SMV are setup according to thesis.

# Wiring

SKITH A U$1 connected to STM32F407G-DISC1 and USB UART BOARD

SKITH A U$8 connected to power supply

SKITH B connected to SKITH A, SD-Card inserted

# Test Actuation

* [ ] SMV powered off

* [ ] SMV fixed, unable to drive

* [ ] Board flashed

* [ ] Wire board with SMV

* [ ] Start Timer and power on SMV

* [ ] At 30 Seconds front thruster should activate for 5 seconds

* [ ] At 35 Seconds port thruster should activate for 5 seconds

* [ ] At 40 Seconds starboard thruster should activate for 5 seconds

* [ ] At 45 Seconds the reaction wheel should start spinning for 5 seconds

* [ ] At 50 Seconds the port thruster is rotated for 5 seconds

* [ ] At 55 Seconds the starboard thruster is rotated for 5 seconds

* [ ] At 60 Seconds the test is finished and the SMV is not actuated

* [ ] End flight and power off SMV

# Logging Standalone

* [ ] SMV powered off

* [ ] SMV fixed, unable to drive

* [ ] Board flashed

* [ ] Wire board with SMV

* [ ] Power on SMV

* [ ] Wait at least 3 seconds

* [ ] End flight and power off SMV

* [ ] Check SD Card latest.csv data. If all entries are zero except "iteration" and "timestamp", the test failed.

# Autopilot Standalone OR Combined Logging and Autopilot on one Board

* [ ] SMV reaction wheel PID controlller integrator disabled

* [ ] SMV powered off

* [ ] Prepare Flight

* [ ] One board flashed with combined or two boards with autopilot and logger

* [ ] Wire SMV

* [ ] Start timer and power on SMV

* [ ] SMV stable for 60 sconds, else abort

* [ ] At 60 Seconds SMV thrusts forwards and breaks (3 seconds)

* [ ] SMV stable for 60 sconds, else abort

* [ ] At 123 Seconds SMV turns with thrusters (8 seconds)

* [ ] SMV stable for 60 sconds, else abort

* [ ] At 191 Seconds SMV turns with reaction wheel (30 seconds)

* [ ] SMV stable for 60 sconds, else abort
