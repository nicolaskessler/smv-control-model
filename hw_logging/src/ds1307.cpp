#include "rodos.h"

#include "RTClib.h"

HAL_I2C i2cdev(I2C_IDX2);

Semaphore hw_sem;
CommBuffer<DateTime> date_cb;

class InitI2CForDS1307 : public Initiator {
    void init() { 
    }
} initi2cfords1307;

class DS1307Node : public Thread {

  private:

  float h = 1 * SECONDS;

  public:

  RTC_DS1307 rtc;

  DS1307Node():
  Thread("ds1307-publisher", 400, 4096),
  rtc(i2cdev)
  {}

  void init() {
        PRINTF("Init I2C for DS1307\n");
        i2cdev.init(100000); 
  }
    
  void run() {
    PRINTF("Starting Real-Time-Clock\n");
    if(rtc.isrunning() && true) {
      PRINTF("RTC is running to date! :)\n");
    }
    else {
      PRINTF("RTC is not running, let's get it a date!\n");
      uint16_t year = 2020;
      uint8_t month = 10;
      uint8_t day = 15;
      uint8_t hour = 13;
      uint8_t min = 37;
      uint8_t sec = 30;
      DateTime dateNow(year, month, day, hour, min, sec);
      rtc.adjust(dateNow);
      if(rtc.isrunning()) {
        PRINTF("Successfully started the RTC. I got it a date.\n");
      }
      else {
        PRINTF("Ups! The RTC is not running although it got a date. It will die a virgin. FIXME!\n");
      }
    }

    TIME_LOOP(NOW(), h) {
      PRINTF("Reading RTC...\n");
      DateTime dateNow;
      {
        PROTECT_IN_SCOPE(hw_sem);
        dateNow = rtc.now();
        date_cb.put(dateNow);
      }
      PRINTF("\nTime:\n"
      "Year:    %d\n"
      "Month:   %d\n"
      "Day:     %d\n"
      "Hour:    %d\n"
      "Minute:  %d\n"
      "Seconds: %d\n\n", dateNow.year(), dateNow.month(), dateNow.day(), dateNow.hour(), dateNow.minute(), dateNow.second());
    }
  }

} ds1307node;
