#include "rodos.h"

#include "beacon.hpp"
#include "ping_responder.hpp"

uint32_t local_id = 200;

static Application a("DemoNode");

class HelloWorld : public Thread {

    void run() {
        PRINTF("Hello World!\n");
    }
    
} helloworld;

// Beacon b(local_id);
// PingResponder p(local_id);
