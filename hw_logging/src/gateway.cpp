#include "rodos.h"
#include "gateway.h"

static HAL_CAN canPort(CAN_IDX0);
static LinkinterfaceCAN linkinterface(&canPort);

class InitCanForGateway : public Initiator {
    void init() { 
        PRINTF("Init CAN for Gateway\n");
        canPort.init(500000); 
    }
} initCanForGateway;

static Gateway gateway(&linkinterface, true);
