#include "rodos.h"
#include <ctime>

#include "FileSystem.h"
#include "File.h"
#include "Directory.h"

#include "RTClib.h"

#include "logging.hpp"

extern CommBuffer<DateTime> date_cb;

//global allocation to more easily keep track on the memory we use
//this can of course also be done locally (directly in main) or with alloc
FileSystem fs_;
File f_;

class WriterOnNova : public StaticThread<8192> {

    public:

    SMVCSVGenerator generator;

    WriterOnNova() : StaticThread<8192>("Writer On Nova Thread", 1), generator() {

    }

    void init() {
    }

    void run() {
        FileSystem* fileSystem = &fs_;
        File* file = &f_;
        int res;
        DateTime now;
        AT(NOW()+3*SECONDS);

        date_cb.get(now);
        char filename[80];
        sprintf(filename, "/%d-%d-%d", 
        (int32_t)now.day(), (int32_t)now.hour(), (int32_t)now.minute());
        // sprintf(filename, "/test.txt");
        PRINTF("Filename: %s\n", filename);

        // create

        //disk must be mounted before anything else can be done
        res = fileSystem->mountDisk(0);
        PRINTF("main > mount done with result: %d\n", res);
        PRINTF("Log file: %s\n", filename);

        int64_t free_space = fileSystem->freeSpace("/");
        PRINTF("Free space: %lld\n", free_space);

        //setting time changes RODOS system time
        fileSystem->setTime((int32_t)now.year(), (int32_t)now.month(), (int32_t)now.day(), (int32_t)now.hour(), (int32_t)now.minute(), (int32_t)now.second());
        PRINTF("main > set Time  done with result: UNKNOWN\n");

        res = file->open(filename, file->O_WRITENEW);
        PRINTF("main > open '%s' done with res: %d\n", filename, res);
        if(res != 0) {
            AT(END_OF_TIME);
        }

        const char * header = generator.generate_header();
        file->putString(header);
        file->sync();

        TIME_LOOP(NOW(), 50*MILLISECONDS) {
            // get all lines from FIFO
            int64_t t = NOW();
            // write them
            PRINTF("Writing............ at %2.4f\n", SECONDS_NOW());
            const char * line = generator.autolog();
            file->putString(line);
            file->sync();
            PRINTF("Finished writing... at %2.4f\n", SECONDS_NOW());
        }

    }
} won; // TODO Logger is not interruptible
