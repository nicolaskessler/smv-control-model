
/* 
 * File:   main.cpp
 * Author: gabriel reimers 2012, Nicolas Kessler 2020
 *
 * Created on November 10, 2011, 12:37 PM
 */

#include "FileSystem.h"
#include "File.h"
#include "Directory.h"
#include <stdio.h>
#include <string.h>

#include "rodos.h" 
// #include "diskio.h"

//global allocation to more easily keep track on the memory we use
//this can of course also be done locally (directly in main) or with alloc
FileSystem fs_;
File f_;

extern Semaphore hw_sem;

class Fatmos: public Thread {
public:
  Fatmos(): Thread("FATMOS", 20, 10000) {
  }

  void init() {
    //
  }


 void run() {
    //we prefer to have pointers so we create these:
    FileSystem* fileSystem = &fs_;
    File* file = &f_;
    int res;


    PRINTF("\n////////////////////////////\n"
            "main > TESTING INIT AND MOUNT\n"
            "//////////////////////\n");

    AT(NOW() + 2 * SECONDS);

    //disk must be mounted before anything else can be done
    res = fileSystem->mountDisk(0);
    PRINTF("main > mount done with result: %d\n", res);

    //format will create a new fat file system
    //this can be omitted if there already is a fs on the disk
    // res = fileSystem->formatDisk(0);
    // PRINTF("main > format done with result: %d\n", res);

    int64_t free_space = fileSystem->freeSpace("/");
    PRINTF("Free space: %lld\n", free_space);

    //setting time changes RODOS system time
    fileSystem->setTime(2012, 03, 03, 07, 31, 12);
    PRINTF("main > set Time  done with result: UNKNOWN\n");

    PRINTF("\n\n////////////////////////////\n"
            "main > FILE FUNTIONS\n"
                 "///////////////////////////\n");

    res = file->open("/test-1-2.txt", file->O_WRITENEW);
    PRINTF("main > open '/test.txt' done with res: %d\n", res);

    file->putString("This is my first file!\n");
    PRINTF("main > put string done\n");

    long t = file->tell();
    PRINTF("main > tell '/test.txt': %ld\n", t);
    //to append a string we have to go back 1 byte because a \0 has already been written
    file->lseek(t - 1);
    PRINTF("main > seek '/test.txt' to %ld - 1\n", t);

    char buf[6];
    sprintf(buf, "HELLO");
    file->write(buf, 6);
    PRINTF("main > writing HELLO to '/test.txt'\n");

    file->sync();
    PRINTF("main > sync '/test.txt' done\n");

    file->lseek(t - 6 - 1);
    file->putChar('C');
    file->putChar('H');

    PRINTF("main > put char CH to '/test.txt' done\n");

    res = file->close();
    PRINTF("main > close '/test.txt' for write done with res: %d\n", res);

    


    /////////////////READING


    res = file->open("/test-1-2.txt", file->O_READ);
    PRINTF("main > open '/test.txt' for read done with res: %d\n", res);

    char getBuf[50];
    for (int i = 0; i < 50; i++){
      getBuf[i] = 0;
    }
    file->getString(getBuf, 50);
    PRINTF("main > getstring '/test.txt': \n%s\n", getBuf);


    char readBuf[50];
    for (int i = 0; i < 50; i++){
      readBuf[i] = 0;
    }
    file->lseek(0);
    file->read(readBuf, 50);
    readBuf[49] = 0;//end string
    PRINTF("main > read 50 from '/test.txt': \n%s\n", readBuf);

    int err = file->errorNo();
    PRINTF("main > get error number '/test.txt': \n%d\n", err);

    file->close();
    PRINTF("main > close '/test.txt' for read\n");

    TIME_LOOP(NOW(), 1*SECONDS)  {
      PRINTF("idle...\n");
      yield();
    }
}

};

Fatmos fatmos;
