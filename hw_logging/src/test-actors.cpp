#include "rodos.h"
#include "matlib.h"

#include "velocity_controller_topics.hpp"
#include "topics.h"

class TestActors : public Thread {

    public:

    void init() {}

    void run() {

        sControl c;

        PRINTF("SMV actor test initializing...\n");

        PRINTF("SMV actor test starting in 30 seconds...\n");

        AT(NOW() + 30 * SECONDS);
        PRINTF("Front Thruster\n");
        c.th_time[0] = 50;
        c.th_time[1] = 0;
        c.th_time[2] = 0;
        c.thruster_theta[0] = 0;
        c.thruster_theta[1] = 0;
        c.rpm = 0;
        c.rw_time = 50;
        c.dock = 0;
        c.ambient1 = 0;
        c.ambient2 = 0;
        c.range1 = 0;
        c.range2 = 0;
        ControlTopic.publish(c);

        AT(NOW() + 5 * SECONDS);
        PRINTF("Port Thruster\n");
        c.th_time[0] = 0;
        c.th_time[1] = 50;
        c.th_time[2] = 0;
        c.thruster_theta[0] = 0;
        c.thruster_theta[1] = 0;
        c.rpm = 0;
        c.rw_time = 50;
        c.dock = 0;
        c.ambient1 = 0;
        c.ambient2 = 0;
        c.range1 = 0;
        c.range2 = 0;
        ControlTopic.publish(c);

        AT(NOW() + 5 * SECONDS);
        PRINTF("Starboard Thruster\n");
        c.th_time[0] = 0;
        c.th_time[1] = 0;
        c.th_time[2] = 50;
        c.thruster_theta[0] = 0;
        c.thruster_theta[1] = 0;
        c.rpm = 0;
        c.rw_time = 50;
        c.dock = 0;
        c.ambient1 = 0;
        c.ambient2 = 0;
        c.range1 = 0;
        c.range2 = 0;
        ControlTopic.publish(c);

        AT(NOW() + 5 * SECONDS);
        PRINTF("Reaction Wheel\n");
        c.th_time[0] = 0;
        c.th_time[1] = 0;
        c.th_time[2] = 0;
        c.thruster_theta[0] = 0;
        c.thruster_theta[1] = 0;
        c.rpm = 5000;
        c.rw_time = 50;
        c.dock = 0;
        c.ambient1 = 0;
        c.ambient2 = 0;
        c.range1 = 0;
        c.range2 = 0;
        ControlTopic.publish(c);

        AT(NOW() + 5 * SECONDS);
        PRINTF("Turn port Thruster\n");
        c.th_time[0] = 0;
        c.th_time[1] = 0;
        c.th_time[2] = 0;
        c.thruster_theta[0] = 60;
        c.thruster_theta[1] = 0;
        c.rpm = 0;
        c.rw_time = 50;
        c.dock = 0;
        c.ambient1 = 0;
        c.ambient2 = 0;
        c.range1 = 0;
        c.range2 = 0;
        ControlTopic.publish(c);

        AT(NOW() + 5 * SECONDS);
        PRINTF("Turn starboard Thruster\n");
        c.th_time[0] = 0;
        c.th_time[1] = 0;
        c.th_time[2] = 0;
        c.thruster_theta[0] = 0;
        c.thruster_theta[1] = 60;
        c.rpm = 0;
        c.rw_time = 50;
        c.dock = 0;
        c.ambient1 = 0;
        c.ambient2 = 0;
        c.range1 = 0;
        c.range2 = 0;
        ControlTopic.publish(c);

        AT(NOW() + 5 * SECONDS);
        PRINTF("Stop\n");
        c.th_time[0] = 0;
        c.th_time[1] = 0;
        c.th_time[2] = 0;
        c.thruster_theta[0] = 0;
        c.thruster_theta[1] = 0;
        c.rpm = 0;
        c.rw_time = 50;
        c.dock = 0;
        c.ambient1 = 0;
        c.ambient2 = 0;
        c.range1 = 0;
        c.range2 = 0;
        ControlTopic.publish(c);

        AT(END_OF_TIME);

    }

} ta;
