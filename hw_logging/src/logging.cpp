#include "logging.hpp"

static CommBuffer<sPositionData> cbPositionData;
static CommBuffer<sControl> cbControl;
static CommBuffer<sHouseKeeping> cbHouseKeeping;
static CommBuffer<sSensorData> ModesSensorDataBuffer;

// TODO

static Subscriber subPositionData(PositionDataTopic, cbPositionData, "Log-Subscriber-PositionDataTopic");
static Subscriber subControl(ControlTopic, cbControl, "Log-Subscriber-ControlTopic");
static Subscriber subHousekeeping(HouseKeepingTopic, cbHouseKeeping, "Log-Subscriber-HouseKeepingTopic");
static Subscriber ModesSensorSubscriber(SensorDataTopic, ModesSensorDataBuffer);

const char * SMVCSVGenerator::generate_header() const noexcept {

   // char * local_buf[LOGGING_BUFFER_SIZE];
    sprintf(const_cast<char*>(buffer),
        "iteration,"
        "time_stamp,"
        "input_th_time_0,"
        "input_th_time_1,"
        "input_th_time_2,"
        "input_thruster_theta_0,"
        "input_thruster_theta_1,"
        "input_rpm,"
        "input_rw_time,"
        "output_pos_x,"
        "output_pos_y,"
        "output_theta,"
        "output_vel_x,"
        "output_vel_y,"
        "output_vel_theta,"
        "output_tank_value,"
        "output_battery_voltage,"
        "output_docking_status_left,"
        "output_docking_status_right,"
        "output_motor_speed\n"
        );
    //strcpy(buffer, local_buf);

    return buffer;
}

const char * SMVCSVGenerator::generate_line(Item item) const noexcept {

    sprintf(const_cast<char*>(buffer),
    "%d,"
    "%lld,"
    "%d,"
    "%d,"
    "%d,"
    "%d,"
    "%d,"
    "%d,"
    "%d,"
    "%.4f,"
    "%.4f,"
    "%.4f,"
    "%.4f,"
    "%.4f,"
    "%.4f,"
    "%d,"
    "%.4f,"
    "%d,"
    "%d,"
    "%.4f\n",
    item.iteration,
    item.time_stamp,
    item.input_th_time_0,
    item.input_th_time_1,
    item.input_th_time_2,
    item.input_thruster_theta_0,
    item.input_thruster_theta_1,
    item.input_rpm,
    item.input_rw_time,
    item.output_pos_x,
    item.output_pos_y,
    item.output_theta,
    item.output_vel_x,
    item.output_vel_y,
    item.output_vel_theta,
    item.output_tank_value,
    item.output_battery_voltage,
    item.output_docking_status_left,
    item.output_docking_status_right,
    item.output_motor_speed
    );

    return buffer;
}

const char * SMVCSVGenerator::autolog() noexcept {

    sPositionData sp;
    sControl sc;
    sHouseKeeping sh;
    sSensorData ss;

    cbPositionData.get(sp);
    cbControl.get(sc);
    cbHouseKeeping.get(sh);
    ModesSensorDataBuffer.get(ss);

    Item item {
        .iteration = iteration,
        .time_stamp = NOW(),
        .input_th_time_0 = sc.th_time[0],
        .input_th_time_1 = sc.th_time[1],
        .input_th_time_2 = sc.th_time[2],
        .input_thruster_theta_0 = sc.thruster_theta[0],
        .input_thruster_theta_1 = sc.thruster_theta[1],
        .input_rpm = sc.rpm,
        .input_rw_time = sc.rw_time,
        .output_pos_x = sp.PosX,
        .output_pos_y = sp.PosY,
        .output_theta = sp.theta,
        .output_vel_x = sp.Velx,
        .output_vel_y = sp.Vely,
        .output_vel_theta = sp.Veltheta,
        .output_tank_value = sh.TankValue,
        .output_battery_voltage = sh.BatteryVoltage,
        .output_docking_status_left = sh.DockStatusLeft,
        .output_docking_status_right = sh.DockStatusRight,
        .output_motor_speed = ss.motorSpeed
    };

    iteration++;

    return generate_line(item);
}
