#!/bin/bash

tgt="test"

echo Building and flashing target $tgt ...

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
cd $DIR

rm -rf build
mkdir build
cd build

cmake -DCMAKE_TOOLCHAIN_FILE=../external/rodos/cmake/port/skith.cmake -DCMAKE_BUILD_TYPE=Debug -DEXECUTABLE=ON ..
make $tgt
arm-none-eabi-objcopy -O binary $tgt $tgt.bin
st-flash write $tgt.bin 0x8000000
