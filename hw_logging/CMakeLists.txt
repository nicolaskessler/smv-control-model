project("logger")
cmake_minimum_required(VERSION 3.13)

add_compile_options( $<$<COMPILE_LANGUAGE:CXX>:-fno-rtti>)
add_compile_options( $<$<COMPILE_LANGUAGE:CXX>:-fno-exceptions>)

add_subdirectory(external/rodos)

add_subdirectory(external/messages)

add_subdirectory(external/topics)

add_subdirectory(external/beacon)

add_subdirectory(external/rtclib)

add_subdirectory(external/SD)

add_subdirectory(external/fatfslib)

add_library(smv_topics
  topics/topics.cpp
)

target_include_directories(smv_topics PUBLIC
  $<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}/topics>
  $<INSTALL_INTERFACE:include>
)

target_link_libraries(smv_topics
  rodos
)

add_library(logging
  src/logging.cpp
)

target_include_directories(logging PUBLIC
  $<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}/include>
  $<INSTALL_INTERFACE:include>
)

target_link_libraries(logging
  smv_topics
  rodos
)

add_executable(logger
#  src/ds1307.cpp
#  src/logwriter-sdio.cpp
#  src/logger-on-nova.cpp
  src/logger-on-skith.cpp
  src/logging.cpp
  src/gateway.cpp
)

add_executable(pilot
  src/auto-experiment.cpp
  src/gateway.cpp
  src/smv_velocity_controller.cpp
  src/velocity_controller_topics.cpp
)

target_include_directories(pilot PRIVATE
  $<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}/include>
  $<INSTALL_INTERFACE:include>
)

add_executable(combined
  src/auto-experiment.cpp
  src/gateway.cpp
  src/smv_velocity_controller.cpp
  src/velocity_controller_topics.cpp
#  src/ds1307.cpp
#  src/logwriter-sdio.cpp
#  src/logger-on-nova.cpp
  src/logger-on-skith.cpp
  src/logging.cpp
  src/gateway.cpp
)

target_include_directories(combined PRIVATE
  $<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}/include>
  $<INSTALL_INTERFACE:include>
)

add_executable(test
  src/test-actors.cpp
  src/gateway.cpp
)

target_include_directories(test PRIVATE
  $<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}/include>
  $<INSTALL_INTERFACE:include>
)

target_link_libraries(logger
  kamaro-rodos-beacon
  rtclib
  spisdlib
  fatfslib
  smv_topics
  logging
)

target_link_libraries(test
  kamaro-rodos-beacon
  smv_topics
)

target_link_libraries(pilot
  kamaro-rodos-beacon
  smv_topics
)

target_link_libraries(combined
  kamaro-rodos-beacon
  rtclib
  spisdlib
  fatfslib
  smv_topics
  logging
)
