/*-----------------------------------------------------------------------*/
/* Low level disk I/O module skeleton for FatFs     (C)ChaN, 2013        */
/*-----------------------------------------------------------------------*/
/* If a working storage control module is available, it should be        */
/* attached to the FatFs via a glue function rather than modifying it.   */
/* This is an example of glue functions to attach various exsisting      */
/* storage control module to the FatFs module with a defined API.        */
/*-----------------------------------------------------------------------*/

#include "diskio.h"		/* FatFs lower layer API */
#include "rodos.h"

#ifndef NO_RODOS_NAMESPACE
namespace RODOS {
#endif

HAL_GPIO ss_pin(GPIO_064);
HAL_SPI spi_dev(SPI_IDX3);
Sd2Card sd(ss_pin, spi_dev);
int initialized = false;
int init_res = 0;

/*-----------------------------------------------------------------------*/
/* Inidialize a Drive                                                    */
/*-----------------------------------------------------------------------*/

DSTATUS disk_initialize(BYTE pdrv /* Physical drive nmuber (0..) */
) {
	if(!initialized) {

		init_res = sd.init();

		// get SD card information
		cid_t cid;
		sd.readCID(&cid);
		int32_t size = sd.cardSize();
		PRINTF(
		"SD information:\n"
		"Manufacturer ID: %d\n"
		"Product Name: not implemented\n"
		"Size: %d\n",
		cid.mid,
		cid.pnm,
		size
    );
	 initialized = true;
	}

	if (init_res)
		return RES_OK;
	else{
		PRINTF("SD Init Error Code: %d\n", init_res);
		return RES_ERROR;
	}
}

/*-----------------------------------------------------------------------*/
/* Get Disk Status                                                       */
/*-----------------------------------------------------------------------*/

DSTATUS disk_status(BYTE pdrv /* Physical drive nmuber (0..) */
) {
	uint8_t state = sd.errorCode();
	if(state){
		return RES_ERROR;
	}else{
		return RES_OK;
	}
}

/*-----------------------------------------------------------------------*/
/* Read Sector(s)                                                        */
/*-----------------------------------------------------------------------*/

DRESULT disk_read(BYTE pdrv, /* Physical drive nmuber (0..) */
BYTE *buff, /* Data buffer to store read data */
DWORD sector, /* Sector address (LBA) */
BYTE count /* Number of sectors to read (1..128) */
) {
	//xprintf("Reading from sector %d count %d...\n",sector,count);
	uint8_t res = true;
	{
	  PRIORITY_CEILER_IN_SCOPE();
	  //res = sd.readData((uint64_t)sector, 0, count, buff);
	  for (int i = 0; i < count; i++) {
		  res &= sd.readBlock(sector+i, buff+(512LL*i));
	  }
	//   for (int i = 0; i < 512; i++) {
	// 	  xprintf("%0x\n", buff[i]);
	//   }
	}

	if (res){
		return RES_OK;
	} else {
		//xprintf("SD Card: Error Code %d, check enum SD_Error",res);
		return RES_ERROR;
	}
}

/*-----------------------------------------------------------------------*/
/* Write Sector(s)                                                       */
/*-----------------------------------------------------------------------*/

#if _USE_WRITE
DRESULT disk_write(BYTE pdrv, /* Physical drive nmuber (0..) */
const BYTE *buff, /* Data to be written */
DWORD sector, /* Sector address (LBA) */
BYTE count /* Number of sectors to write (1..128) */
) {
	//xprintf("Writing to sector %d count %d...\n",sector,count);
	uint8_t res = true;
	if(res){
		PRIORITY_CEILER_IN_SCOPE();
		//xprintf("writing...\n");
		for (int i = 0; i < count; i ++) {
		    //res &= sd.erase((uint64_t)sector+i, 1);
			res &= sd.writeBlock((uint64_t)(sector+i), buff+(i*512LL), true);
			if(!res) {
				//xprintf("oh no. %d\n", i);
			}
		}
	}

	if (res){
		return RES_OK;
	} else {
		//xprintf("SD Card: Error Code %d, check enum SD_Error\n",res);
		return RES_ERROR;
	}
}
#endif

/*-----------------------------------------------------------------------*/
/* Miscellaneous Functions                                               */
/*-----------------------------------------------------------------------*/

#if _USE_IOCTL
DRESULT disk_ioctl(BYTE pdrv, /* Physical drive nmuber (0..) */
BYTE cmd, /* Control code */
void *buff /* Buffer to send/receive control data */
) {
	switch (cmd) {
	case CTRL_SYNC:
		return RES_OK;
	case GET_SECTOR_SIZE: // Get R/W page size (DWORD)
		*(DWORD*) buff = 512;
		return RES_OK;
	case GET_SECTOR_COUNT: // Get R/W page size (DWORD)
		*(DWORD*) buff =  sd.cardSize();
		return RES_OK;
	default:
		break;
	}
	return RES_PARERR;
}
#endif

#ifndef NO_RODOS_NAMESPACE
}
#endif

