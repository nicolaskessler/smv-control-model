add_library(fatfslib
  src/fatTime.cpp
  src/ff.cpp
  src/Directory.cpp
  src/File.cpp
  src/FileSystem.cpp
)

target_include_directories(fatfslib PUBLIC
  $<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}/include>
  $<INSTALL_INTERFACE:include>
)

target_link_libraries(fatfslib
  spisdlib
)
