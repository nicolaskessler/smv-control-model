# SMV-Control-Model

Model derivation and identification for control of the vehicles in the Space Maneuvering and Docking (SMD) Facility

# Useful resources

* RODOS https://gitlab.com/rodos/rodos
* LVGL https://littlevgl.com/
* Sympy https://www.youtube.com/watch?v=5jzIVp6bTy0

# Dependencies

## pip

```
vcstool
numpy
scipy
scikit-learn
pandas
sympy
matplotlib
jupyterlab
perlin-numpy
casadi
cvxpy
```

## apt

```
# rodos-dependencies
libsdl2-dev:i386
libsdl2-2.0-0:i386
xterm
```

## Setup Jupyter-Lab

```
# apt install pipenv
$ export PIPENV_VENV_IN_PROJECT=1
$ pipenv --three
$ pipenv install requests
$ pipenv shell
# apt install nodejs npm
# apt install firefox
# snap install chromium
$ jupyter labextension install @jupyter-widgets/jupyterlab-manager
$ jupyter labextension install jupyter-matplotlib
$ jupyter lab build
```
