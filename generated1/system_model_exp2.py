import numpy

def next_state(state, inputs, params):
    for i in range(6):
       qx_next = state[0]
       qy_next = state[1]
       qtheta_next = 0.000263088100490906*inputs[1]*params[0] - 0.000263088100490906*inputs[2]*params[0] + state[2] + (1/20)*state[5]
       ux_next = -0.00666666666666667*inputs[0]*params[0] + 0.00333333333333333*inputs[1]*params[0] + 0.00333333333333333*inputs[2]*params[0] + state[3]
       uy_next = -0.00577350269189626*inputs[1]*params[0] + 0.00577350269189626*inputs[2]*params[0] + state[4]
       utheta_next = 0.0105235240196363*inputs[1]*params[0] - 0.0105235240196363*inputs[2]*params[0] + state[5]
    return [qx_next, qy_next, qtheta_next, ux_next, uy_next, utheta_next]
