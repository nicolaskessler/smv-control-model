#include "system_model.h"
#include <math.h>
void system_model(double *inputs, double *state, double *next_state) {
   next_state[0] = -1.0/16000.0*sin(state[2] + (1.0/20.0)*state[5])*inputs[1] + (1.0/800.0)*sin(state[2] + (1.0/20.0)*state[5])*state[3]*state[5] - 1.0/40.0*sin(state[2] + (1.0/20.0)*state[5])*state[4] - 1.0/40.0*sin(state[2])*state[4] + (1.0/16000.0)*cos(state[2] + (1.0/20.0)*state[5])*inputs[0] + (1.0/40.0)*cos(state[2] + (1.0/20.0)*state[5])*state[3] + (1.0/800.0)*cos(state[2] + (1.0/20.0)*state[5])*state[4]*state[5] + (1.0/40.0)*cos(state[2])*state[3] + state[0];
   next_state[1] = (1.0/16000.0)*sin(state[2] + (1.0/20.0)*state[5])*inputs[0] + (1.0/40.0)*sin(state[2] + (1.0/20.0)*state[5])*state[3] + (1.0/800.0)*sin(state[2] + (1.0/20.0)*state[5])*state[4]*state[5] + (1.0/40.0)*sin(state[2])*state[3] + (1.0/16000.0)*cos(state[2] + (1.0/20.0)*state[5])*inputs[1] - 1.0/800.0*cos(state[2] + (1.0/20.0)*state[5])*state[3]*state[5] + (1.0/40.0)*cos(state[2] + (1.0/20.0)*state[5])*state[4] + (1.0/40.0)*cos(state[2])*state[4] + state[1];
   next_state[2] = (1.0/16000.0)*inputs[2] + state[2] + (1.0/20.0)*state[5];
   next_state[3] = (1.0/400.0)*inputs[0] + (1.0/6400000.0)*inputs[1]*inputs[2] + (1.0/16000.0)*inputs[1]*state[5] - 1.0/320000.0*inputs[2]*state[3]*state[5] + (1.0/16000.0)*inputs[2]*state[4] - 1.0/800.0*state[3]*pow(state[5], 2) + state[3] + (1.0/20.0)*state[4]*state[5];
   next_state[4] = -1.0/6400000.0*inputs[0]*inputs[2] - 1.0/16000.0*inputs[0]*state[5] + (1.0/400.0)*inputs[1] - 1.0/16000.0*inputs[2]*state[3] - 1.0/320000.0*inputs[2]*state[4]*state[5] - 1.0/20.0*state[3]*state[5] - 1.0/800.0*state[4]*pow(state[5], 2) + state[4];
   next_state[5] = (1.0/400.0)*inputs[2] + state[5];
}
