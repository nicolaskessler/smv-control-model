import numpy

def next_state(state, inputs, params):
    for i in range(6):
       qx_next = 0.000166666666666667*numpy.sin(state[2] + 0.523598775598299)*inputs[1]*params[0] - 1/20*numpy.sin(state[2])*state[5] + 0.000166666666666667*numpy.cos(state[2] + 1.0471975511966)*inputs[2]*params[0] - 0.000166666666666667*numpy.cos(state[2])*inputs[0]*params[0] + (1/20)*numpy.cos(state[2])*state[4] + state[0]
       qy_next = 0.000166666666666667*numpy.sin(state[2] + 1.0471975511966)*inputs[2]*params[0] - 0.000166666666666667*numpy.sin(state[2])*inputs[0]*params[0] + (1/20)*numpy.sin(state[2])*state[4] - 0.000166666666666667*numpy.cos(state[2] + 0.523598775598299)*inputs[1]*params[0] + (1/20)*numpy.cos(state[2])*state[5] + state[1]
       qtheta_next = state[2]
       qalpha_next = state[3]
       ux_next = -0.00666666666666667*inputs[0]*params[0] + 0.00333333333333333*inputs[1]*params[0] + 0.00333333333333333*inputs[2]*params[0] + state[4]
       uy_next = -0.00577350269189626*inputs[1]*params[0] + 0.00577350269189626*inputs[2]*params[0] + state[5]
       utheta_next = 0.0105235240196363*inputs[1]*params[0] - 0.0105235240196363*inputs[2]*params[0] + state[6]
       ualpha_next = -0.0105235240196363*inputs[1]*params[0] + 0.0105235240196363*inputs[2]*params[0] + state[7]
    return [qx_next, qy_next, qtheta_next, qalpha_next, ux_next, uy_next, utheta_next, ualpha_next]
