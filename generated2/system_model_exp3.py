import numpy

def next_state(state, inputs, params):
    for i in range(6):
       qx_next = state[0]
       qy_next = state[1]
       qtheta_next = 0.0126262626262626*inputs[0]*params[1] + 0.0126262626262626*params[2]*state[7] + state[2] + (1/20)*state[6]
       qalpha_next = -10.696386946387*inputs[0]*params[1] - 10.696386946387*params[2]*state[7] + state[3] + (1/20)*state[7]
       ux_next = state[4]
       uy_next = state[5]
       utheta_next = -108.044312589767*inputs[0]*params[1]*params[2] + 0.505050505050505*inputs[0]*params[1] - 108.044312589767*params[2]**2*state[7] + 0.505050505050505*params[2]*state[7] + state[6]
       ualpha_next = 91530.1549654697*inputs[0]*params[1]*params[2] - 427.855477855478*inputs[0]*params[1] + 91530.1549654697*params[2]**2*state[7] - 427.855477855478*params[2]*state[7] + state[7]
    return [qx_next, qy_next, qtheta_next, qalpha_next, ux_next, uy_next, utheta_next, ualpha_next]
