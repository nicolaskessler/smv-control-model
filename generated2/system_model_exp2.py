import numpy

def next_state(state, inputs, params):
    for i in range(6):
       qx_next = state[0]
       qy_next = state[1]
       qtheta_next = -0.00017610625*inputs[1]*params[0]/params[1] + 0.00017610625*inputs[2]*params[0]/params[1] + state[2] + 0.05*state[6] + 1.805e-8*state[7]/params[1]
       qalpha_next = 0.00017610625*inputs[1]*params[0]/params[1] - 0.00017610625*inputs[2]*params[0]/params[1] + state[3] + 0.0498457264957265*state[7] - 1.80499999999999e-8*state[7]/params[1]
       ux_next = -0.00666666666666667*inputs[0]*params[0] + 0.00666666666666667*inputs[1]*params[0] + 0.00666666666666667*inputs[2]*params[0] + state[4]
       uy_next = state[5]
       utheta_next = -0.00704425*inputs[1]*params[0]/params[1] + 2.54297425e-9*inputs[1]*params[0]/params[1]**2 + 0.00704425*inputs[2]*params[0]/params[1] - 2.54297425e-9*inputs[2]*params[0]/params[1]**2 + state[6] + 7.19772290598291e-7*state[7]/params[1] - 2.60642e-13*state[7]/params[1]**2
       ualpha_next = 0.00702251517735043*inputs[1]*params[0]/params[1] - 2.54297424999999e-9*inputs[1]*params[0]/params[1]**2 - 0.00702251517735043*inputs[2]*params[0]/params[1] + 2.54297424999999e-9*inputs[2]*params[0]/params[1]**2 + 0.993848100080357*state[7] - 7.17544581196579e-7*state[7]/params[1] + 2.60641999999998e-13*state[7]/params[1]**2
    return [qx_next, qy_next, qtheta_next, qalpha_next, ux_next, uy_next, utheta_next, ualpha_next]
