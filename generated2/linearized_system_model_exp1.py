import numpy

def linearized_next_state(state, inputs, params):
    for i in range(6):
       qx_next = -0.000166666666666667*inputs[0]*params[0] + 8.33333333333334e-5*inputs[1]*params[0] + 8.33333333333334e-5*inputs[2]*params[0] + state[0] + (1/20)*state[4]
       qy_next = -0.000144337567297406*inputs[1]*params[0] + 0.000144337567297406*inputs[2]*params[0] + state[1] + (1/20)*state[5]
       qtheta_next = state[2]
       qalpha_next = state[3]
       ux_next = -0.00666666666666667*inputs[0]*params[0] + 0.00333333333333333*inputs[1]*params[0] + 0.00333333333333333*inputs[2]*params[0] + state[4]
       uy_next = -0.00577350269189626*inputs[1]*params[0] + 0.00577350269189626*inputs[2]*params[0] + state[5]
       utheta_next = 0.0105235240196363*inputs[1]*params[0] - 0.0105235240196363*inputs[2]*params[0] + state[6]
       ualpha_next = -0.0105235240196363*inputs[1]*params[0] + 0.0105235240196363*inputs[2]*params[0] + state[7]
    return [qx_next, qy_next, qtheta_next, qalpha_next, ux_next, uy_next, utheta_next, ualpha_next]
