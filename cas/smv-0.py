#!/usr/bin/env python
# coding: utf-8

# # Model derivation for Space-Maneuvering-Vehicle using Euler-Lagrange

# In[1]:


import sys
from copy import copy, deepcopy
import random
random.seed(0, 2)
import datetime
import sympy as sp

from sympy.physics.vector import Point, ReferenceFrame
from sympy.physics.vector import Vector, outer, dynamicsymbols
from sympy.physics.vector import kinematic_equations, vprint
from sympy.physics.vector.functions import time_derivative
from sympy.physics.mechanics import LagrangesMethod, RigidBody, Lagrangian
from sympy.physics.mechanics.linearize import Linearizer
from sympy.tensor.array import derive_by_array

from sympy.utilities.codegen import codegen
from sympy.printing.cxxcode import cxxcode

from IPython.display import display, Latex

sp.init_printing(use_latex='mathjax')

print("Loaded python modules.")


# In[2]:


# define the constants

# geometry

# masses
m_smv = sp.symbols("m_smv", real=True)

# inertias
j_smv = sp.symbols("J_smv", real=True)

# g force field
g = sp.symbols("g", real=True)

# disturbance torques

# disturbance forces

# friction coefficients

# dictionary for parameters

param_dict = {
    "m_smv": m_smv,
    "j_smv": j_smv,
    "g": g,
}

# input torques
t_rw = dynamicsymbols("T_rw")

# input forces
f_x = dynamicsymbols("F_x")
f_y = dynamicsymbols("F_y")

# state variables

# x coordinate
qx = dynamicsymbols("q_x")
ux = dynamicsymbols("q_x", 1)
ux_ = dynamicsymbols("u_x")
vx = dynamicsymbols("v_x")

# y coordinate
qy = dynamicsymbols("q_y")
uy = dynamicsymbols("q_y", 1)
uy_ = dynamicsymbols("u_y")
vy = dynamicsymbols("v_y")

# rotation theta
qtheta = dynamicsymbols("q_theta")
utheta = dynamicsymbols("q_theta", 1)
utheta_ = dynamicsymbols("u_theta")
vtheta = dynamicsymbols("v_theta")
    
state = [qx, qy, qtheta, vx, vy, vtheta]

# Others

n = 6
p = 3
q = 3
L_ = sp.symbols("L")
u_ = sp.MatrixSymbol("u", p, 1)
x_ = sp.MatrixSymbol("x", n, 1)
y_ = sp.MatrixSymbol("y", q, 1)
M_ = sp.MatrixSymbol("M", n, n)
A_ = sp.MatrixSymbol("A", n, n)
B_ = sp.MatrixSymbol("B", n, p)
C_ = sp.MatrixSymbol("C", q, n)
h = sp.Rational(1, 20) # 20 Hz = 5*MILLISECONDS


# ## Variables
# 
# $m_{smv}$ - Mass of the SMV
# 
# $j_{smv}$ - Rotation Inertia of the SMV
# 
# $g$ - $9.81\frac{m}{s^2}$ or local gravitational factor
# 
# $T_{rw}$ - Torque excerted by reaction wheel
# 
# $F_x$ - Force excerted by thrusters forward
# 
# $F_y$ - Force excerted by thrusters to port
# 
# $q_x$ - X coordinate in world reference frame
# 
# $q_y$ - Y coordinate in world reference frame
# 
# $q_{\theta}$ - Total rotation of the SMV in world reference frame
# 
# $v_x$ - Velocity of SMV forward
# 
# $v_y$ - Velocity of SMV to port
# 
# $h$ - Sampling rate in $\sec$

# In[3]:


# world reference frame

N = ReferenceFrame("N")
pN = Point("N*")
pN.set_vel(N, 0)


# In[4]:


# use sympy mechanics toolbox to define smv

# create rotated smv reference frame
R_smv = ReferenceFrame("R_smv")
R_smv = N.orientnew("R_smv", "axis", [qtheta, N.z])
R_smv.set_ang_vel(N, utheta*N.z)

# locate smv in world
po_smv = pN.locatenew("po_smv", qx*N.x+qy*N.y)
po_smv.set_vel(N, ux*N.x + uy*N.y)

I_smv = outer(R_smv.z, R_smv.z) * j_smv

B_smv = RigidBody("B_SMV", po_smv, R_smv, m_smv, (I_smv, po_smv))


# In[5]:


# define the forces

F_x = f_x
F_x = (po_smv, F_x*R_smv.x)

F_y = f_y
F_y = (po_smv, F_y*R_smv.y)

T_z = t_rw
T_z = (R_smv, T_z*N.z)

forces = [F_x, F_y, T_z]


# In[6]:


Lag = sp.simplify(Lagrangian(N, B_smv))
display("Lagrangian")
display(sp.Eq(L_, Lag))
print(sp.latex(sp.Eq(L_, Lag)))


# In[7]:


LM = LagrangesMethod(Lag, [qx, qy, qtheta], forcelist=forces, frame=N)
lag_eqs = LM.form_lagranges_equations()
display("Lagrange equations")
display(sp.Eq(sp.Matrix([0, 0 ,0]), lag_eqs))
display("Nonlinear state space")
f = sp.simplify(LM.rhs())
f_el = f
display(sp.Eq(time_derivative(sp.Matrix([qx, qy, qtheta, ux, uy, utheta]), N), f))
print(sp.latex(sp.Eq(time_derivative(sp.Matrix([qx, qy, qtheta, ux, uy, utheta]), N), f)))


# In[8]:


display("Performing linearization...")
linearizer = LM.to_linearizer(q_ind=[qx, qy, qtheta], qd_ind=[ux, uy, utheta])
op_point = {qx: qx, qy: qy, qtheta: qtheta, ux: ux, uy: uy, utheta: utheta, f_x: f_x, f_y: f_y, t_rw: t_rw}
M, A, B = sp.simplify(linearizer.linearize(A_and_B=False, op_point=op_point))
display(M)
display(sp.Eq(A_, A))
display(sp.Eq(B, B_))
A, B = sp.simplify(linearizer.linearize(A_and_B=True, op_point=op_point))
display(sp.Eq(A_, A))
display(sp.Eq(B, B_))


# ## Coordinate transformation
# 
# This coordinate expression re-formulates the velocities originally given in the world reference frame with reference to the robot's reference frame using forward, port and up vectors using a known transformation $T$:
# 
# $v(t) = T(q(t), u(t))$
# 
# Thus:
# 
# $u(t) = T^{-1}(q(t), v(t))$
# 
# The resulting differential equations for $q$ are given by:
# 
# $\frac{d}{dt}q(t) = u = T^{-1}(q(t), v(t))$
# 
# And the differential equations for $v$:
# 
# $$
# \frac{d}{dt}v(t) = \frac{d}{dt}(T(q(t), u(t)) = \frac{\partial T}{\partial q}\frac{\partial q}{\partial t} + \frac{\partial T}{\partial u}\frac{\partial u}{\partial t} 
# = \frac{\partial T}{\partial q} u + \frac{\partial T}{\partial u} f(q,u)
# = \frac{\partial T}{\partial q} T^{-1}(q(t), v(t)) + \frac{\partial T}{\partial u} f(q,T^{-1}(q(t), v(t)))
# $$
# 
# The code block below does this transformation in Sympy:

# In[9]:


# perform coordinate transformation
# this moves the coordinate frame rotation from the velocity ODEs to the coordinate ODEs
# thus, velocity controllers can be fed with an easily linearized model

# variable substitution
f = f.subs(ux, ux_)
f = f.subs(uy, uy_)
f = f.subs(utheta, utheta_)

# display("f with u substitution")
# display(f)

R = sp.Matrix([
    [sp.cos(qtheta), sp.sin(qtheta), 0],
    [-sp.sin(qtheta), sp.cos(qtheta), 0],
    [0, 0, 1]])

display("Rotation Matrix from N to R")
display(R)
print(sp.latex(R))

u_dot = f[3:]

# display("u_dot")
# display(u_dot)

v_of_u = R * sp.Matrix([[ux_], [uy_], [utheta_]])

# display("v from u, aka T(q, u)")
# display(v_of_u)

u_of_v = R.T * sp.Matrix([[vx], [vy], [vtheta]])

# display("u from v, aka T^-1(q, v)")
# display(u_of_v)

# q_dot is u_dot is T^-1(q, v)
q_dot = sp.Matrix([[u_of_v[0]], [u_of_v[1]], [u_of_v[2]]])
# display("q_dot")
# display(q_dot)

# v_dot is a bit more complicated:
# Dv/Dt = dT/dq * T^-1(q, v) + dT/du * f(q, T^-1(v))

dT_by_dq = v_of_u.jacobian([qx, qy, qtheta])
# display("dT_by_dq")
# display(dT_by_dq)
term_1 = dT_by_dq * v_of_u
term_1 = term_1.subs(ux_, u_of_v[0, 0])
term_1 = term_1.subs(uy_, u_of_v[1, 0])
term_1 = term_1.subs(utheta_, u_of_v[2, 0])
term_1 = sp.simplify(term_1)
# display("Term 1")
# display(term_1)

dT_by_du = v_of_u.jacobian([ux_, uy_, utheta_])
# display("dT_by_du")
# display(dT_by_du)
term_2 = dT_by_du * sp.Matrix([[u_dot[0]], [u_dot[1]], [u_dot[2]]])
term_2 = term_2.subs(ux_, u_of_v[0, 0])
term_2 = term_2.subs(uy_, u_of_v[1, 0])
term_2 = term_2.subs(utheta_, u_of_v[2, 0])
term_2 = sp.simplify(term_2)
# display("Term 2")
# display(term_2)

v_dot = term_1 + term_2
# display("v_dot")
# display(v_dot)

f = sp.Matrix(
    [
        [q_dot[0, 0]],
        [q_dot[1, 0]],
        [q_dot[2, 0]],
        [v_dot[0, 0]],
        [v_dot[1, 0]],
        [v_dot[2, 0]],
    ])
display("Transformed f")
display(sp.Eq(time_derivative(sp.Matrix(state), N), f))
print(sp.latex(sp.Eq(time_derivative(sp.Matrix(state), N), f)))


# In[10]:


# manually linearize f (first order taylor approximation)

f_lin = deepcopy(f)

delta_qx = sp.symbols("delta_qx", real=True)
delta_qy = sp.symbols("delta_qy", real=True)
delta_qtheta = sp.symbols("delta_qtheta", real=True)

delta_vx = sp.symbols("delta_vx", real=True)
delta_vy = sp.symbols("delta_vy", real=True)
delta_vtheta = sp.symbols("delta_vtheta", real=True)

delta_fx = sp.symbols("delta_fx", real=True)
delta_fy = sp.symbols("delta_fy", real=True)
delta_trw = sp.symbols("delta_trw", real=True)

offsets = []
offset_dict = {qx: 0, qy: 0, qtheta: 0, vx: 0, vy: 0, vtheta: 0, f_x: 0, f_y: 0, t_rw: 0}
for i in range(6):
    offsets.append(f_lin[i].subs(offset_dict))
    #display(offsets[i])

# A
A = f_lin.jacobian([qx, qy, qtheta, vx, vy, vtheta])
# display("Jacobian w.r.t. states")
# display(A)
# B 
B = f_lin.jacobian([f_x, f_y, t_rw])
# display("Jacobian w.r.t. inputs")
# display(B)

x = sp.Matrix([delta_qx, delta_qy, delta_qtheta, delta_vx, delta_vy, delta_vtheta])
# display(x)

u = sp.Matrix([delta_fx, delta_fy, delta_trw])
# display(u)

offset = sp.Matrix(offsets)
# display(offset)

linearized = A*x + B*u + offset
display("linearization without operating point")
display(linearized)

# insert the operating point AFTER LINEARIZATION
# assume rotation around z axis and velocities to be small
op_point = {
    qx: delta_qx, 
    qy: delta_qy, 
    qtheta: delta_qtheta, 
    vx: 0, 
    vy: 0, 
    vtheta: 0, 
    f_x: delta_fx, 
    f_y: delta_fy, 
    t_rw: delta_trw
}
display(op_point)
print(sp.latex(op_point))

# A
A = linearized.jacobian([delta_qx, delta_qy, delta_qtheta, delta_vx, delta_vy, delta_vtheta])
for variable, substitute in op_point.items():
    A = A.subs(variable, substitute)
display(sp.Eq(A_, A))
print(sp.latex(sp.Eq(A_, A)))

# B 
B = linearized.jacobian([delta_fx, delta_fy, delta_trw])

for variable, substitute in op_point.items():
    B = B.subs(variable, substitute)
display(sp.Eq(B_, B))
print(sp.latex(sp.Eq(B_, B)))


# Linearization in form of:
# 
# $$ \dot{\vec{x}} = \underline{\underline{A}}\vec{x} + \underline{\underline{B}}\vec{u} $$

# In[11]:


# parameterize

# TODO: load parameter dictionary from .json

values = {
    "m_smv": sp.S(20),
    "j_smv": sp.S(20),
    "g": sp.S(9.81),
}

f_param = f
A_param = A
B_param = B

for param, val in values.items():
    f_param = f_param.subs(param_dict[param], val)
    A_param = A_param.subs(param_dict[param], val)
    B_param = B_param.subs(param_dict[param], val)
    
display(sp.Eq(time_derivative(sp.Matrix(state), N), f_param))
display(sp.Eq(A_, A_param))
display(sp.Eq(B_, B_param))


# In[12]:


# # numeric integration using 4th order explicit runge-kutta

# # numeric integration using heun's method

# def k_substitution(e, state, h, k1, a_ij):
#     temp_dict = {}
#     for i, v in enumerate(state):
#         temp_sym = sp.symbols("tempsym_" + str(i))
#         temp_dict[v] = temp_sym
#     e = e.subs(temp_dict)
#     for i, v in enumerate(state):
#         e = e.subs(temp_dict[v], v + a_ij*h*k1[i])
#     return e

# k1 = sp.Matrix(f_param)
# k2 = k_substitution(sp.Matrix(f_param), state, h, k1, sp.Rational(1, 2))
# k3 = k_substitution(sp.Matrix(f_param), state, h, k2, sp.Rational(1, 2))
# k4 = k_substitution(sp.Matrix(f_param), state, h, k3, sp.Rational(1, 1))

# f_numeric_ex_rk4 = sp.simplify(h * ( sp.Rational(1, 6) * k1 + sp.Rational(1, 3) * k2 + sp.Rational(1, 3) * k3 + sp.Rational(1, 6) * k4 ) )

# display(sp.Eq(time_derivative(sp.Matrix(state), N), f_numeric_ex_rk4))


# In[13]:


# numeric integration using heun's method

def k_substitution(e, state, h, k1, a_ij):
    temp_dict = {}
    for i, v in enumerate(state):
        temp_sym = sp.symbols("tempsym_" + str(i))
        temp_dict[v] = temp_sym
    e = e.subs(temp_dict)
    for i, v in enumerate(state):
        e = e.subs(temp_dict[v], v + a_ij*h*k1[i])
    return e

k1 = sp.Matrix(f_param)
k2 = k_substitution(sp.Matrix(f_param), state, h, k1, sp.Rational(1, 1))

f_numeric_ex_heun = sp.simplify(sp.Rational(1, 2) * h * (k1 + k2))

display(sp.Eq(time_derivative(sp.Matrix(state), N), f_numeric_ex_heun))


# In[14]:


# # numeric integration using heun's third order method

# def k_substitution(e, state, h, k1, a_ij):
#     temp_dict = {}
#     for i, v in enumerate(state):
#         temp_sym = sp.symbols("tempsym_" + str(i))
#         temp_dict[v] = temp_sym
#     e = e.subs(temp_dict)
#     for i, v in enumerate(state):
#         e = e.subs(temp_dict[v], v + a_ij*h*k1[i])
#     return e

# k1 = sp.Matrix(f_param)
# k2 = k_substitution(sp.Matrix(f_param), state, h, k1, sp.Rational(1, 3))
# k3 = k_substitution(sp.Matrix(f_param), state, h, k2, sp.Rational(2, 3))

# f_numeric_ex_heun3 = sp.simplify( h * (sp.Rational(1, 4)*k1 + sp.Rational(3, 4)*k3))

# display(sp.Eq(time_derivative(sp.Matrix(state), N), f_numeric_ex_heun3))


# In[15]:


# numeric integration using half-step method

def k_substitution(e, state, h, k1, a_ij):
    temp_dict = {}
    for i, v in enumerate(state):
        temp_sym = sp.symbols("tempsym_" + str(i))
        temp_dict[v] = temp_sym
    e = e.subs(temp_dict)
    for i, v in enumerate(state):
        e = e.subs(temp_dict[v], v + a_ij*h*k1[i])
    return e

k1 = sp.Matrix(f_param)
k2 = k_substitution(sp.Matrix(f_param), state, h, k1, sp.Rational(1, 2))

f_numeric_ex_halfstep = sp.simplify(sp.Rational(1, 1) * h * (k2))

display(sp.Eq(time_derivative(sp.Matrix(state), N), f_numeric_ex_halfstep))


# In[16]:


# generic numeric integration
# https://www.tu-braunschweig.de/Medien-DB/iwr/ODEII_SS17/lecture2.pdf
# https://en.wikipedia.org/wiki/Runge%E2%80%93Kutta_methods#Implicit_Runge%E2%80%93Kutta_methods

def insert_for_x(f, state, new_x):
    
    temp_dict = {}
    for i, v in enumerate(state):
        temp_sym = sp.symbols("tempsym_" + str(i))
        temp_dict[v] = temp_sym
    
    subs_dict = {}
    for j in range(len(state)):
        subs_dict[temp_dict[state[j]]] = new_x[j]
       
    f = f.subs(temp_dict)
    return f.subs(subs_dict)

# test
# display(f_param)
# new_x = [0, 1, 0, 2, 2, 0.5]
# new_f = insert_for_x(f_param, state, new_x)
# display(new_f)
        
def error_of_k(f, h, x, A, c, k):
    # f: sympy expression list for x_dot
    # h: sampling rate
    # x: states
    # u: inputs
    # A
    # c
    # k: a given sympy expression for k
    s = A.shape[0]
    errors = sp.Matrix.zeros(s, len(x))

    for i in range(s):
        step = sp.zeros(len(x), 1)
            
        for l in range(len(x)):
            for m in range(s):
                step[l] += h * A[i, m] * k[m, l]
            step[l] += x[l]
            
        for l in range(len(x)):
#             display(step)
#             display(insert_for_x(f, x, step)[l])
            errors[i, l] = insert_for_x(f, x, step)[l] - k[i, l]
                     
    return errors

def numeric_integration(f_param, state, h, A, b, c):

    k = []
    for i in range(s):
        temp = []
        for j in range(len(state)):
            kij = sp.symbols("k_" + str(i) + str(j))
            temp.append(kij)
        k.append(temp)
    k = sp.Matrix(k)

#     display(k)

    e = error_of_k(sp.Matrix(f_param), h, state, A, c, k)
#     display(e)

    eqn_list = []
    k_list = []
    for i in range(s):
        for j in range(len(state)):
            eqn_list.append(e[i,j])
            k_list.append(k[i,j])

    sol = sp.solve(eqn_list, k_list, dict = True)
    sol = sp.simplify(sol[0])
    # for i in range(s):
    #     for j in range(len(x)):
    #         display(sol[k[i,j]])

    delta_x = []
    for i in range(len(state)):
        temp = 0
        for j in range(s):
            temp += h * b[j] * sol[k[j, i]]
        delta_x.append(sp.simplify(temp))
        #display(delta_x[i])

    delta_x = sp.Matrix(delta_x)
    return delta_x


# In[17]:


# # Lobatto-3A

# A = sp.Matrix([
#     [sp.Rational(0, 1), sp.Rational(0, 1), sp.Rational(0, 1)],
#     [sp.Rational(5, 24), sp.Rational(1, 3), sp.Rational(-1, 24)],
#     [sp.Rational(1, 6), sp.Rational(2, 3), sp.Rational(1, 6)]
# ])
# b = sp.Matrix([sp.Rational(1, 6), sp.Rational(2, 3), sp.Rational(1, 6)])
# c = sp.Matrix([sp.Rational(0, 1), sp.Rational(1, 2), sp.Rational(1, 1)])
# s = A.shape[0]

# delta_x_l = numeric_integration(f_param, state, h, A, b, c)
# display(sp.Eq(time_derivative(sp.Matrix(state), N), delta_x_l))


# In[18]:


# # Forth-order explicit Runge-Kutta

# A = sp.Matrix([
#     [sp.Rational(0, 1), sp.Rational(0, 1), sp.Rational(0, 1), sp.Rational(0, 1)],
#     [sp.Rational(1, 2), sp.Rational(0, 1), sp.Rational(0, 1), sp.Rational(0, 1)],
#     [sp.Rational(0, 1), sp.Rational(1, 2), sp.Rational(0, 1), sp.Rational(0, 1)],
#     [sp.Rational(0, 1), sp.Rational(0, 1), sp.Rational(1, 1), sp.Rational(0, 1)],
# ])
# b = sp.Matrix([sp.Rational(1, 6), sp.Rational(1, 3), sp.Rational(1, 3), sp.Rational(1, 6)])
# c = sp.Matrix([sp.Rational(0, 1), sp.Rational(1, 2), sp.Rational(1, 2), sp.Rational(1, 1)])
# s = A.shape[0]

# delta_x_rk4 = numeric_integration(f_param, state, h, A, b, c)
# display(sp.Eq(time_derivative(sp.Matrix(state), N), delta_x_rk4))

# # test
# for i in range(len(state)):
#     display(sp.simplify(f_numeric_ex_rk4[i] - delta_x_rk4[i]))


# In[19]:


# Second-order explicit Heun

A = sp.Matrix([
    [sp.Rational(0, 1), sp.Rational(0, 1)],
    [sp.Rational(1, 1), sp.Rational(0, 1)]
])
b = sp.Matrix([sp.Rational(1, 2), sp.Rational(1, 2)])
c = sp.Matrix([sp.Rational(0, 1), sp.Rational(1, 1)])
s = A.shape[0]

delta_x_h2 = numeric_integration(f_param, state, h, A, b, c)
display(sp.Eq(time_derivative(sp.Matrix(state), N), delta_x_h2))

# test
for i in range(len(state)):
    display(sp.simplify(f_numeric_ex_heun[i] - delta_x_h2[i]))


# In[20]:


# # Third-order explicit Heun

# A = sp.Matrix([
#     [sp.Rational(0, 1), sp.Rational(0, 1), sp.Rational(0, 1)],
#     [sp.Rational(1, 3), sp.Rational(0, 1), sp.Rational(0, 1)],
#     [sp.Rational(0, 1), sp.Rational(2, 3), sp.Rational(0, 1)]
# ])
# b = sp.Matrix([sp.Rational(1, 4), sp.Rational(0, 1), sp.Rational(3, 4)])
# c = sp.Matrix([sp.Rational(0, 1), sp.Rational(1, 3), sp.Rational(2, 3)])
# s = A.shape[0]

# delta_x_h3 = numeric_integration(f_param, state, h, A, b, c)
# display(sp.Eq(time_derivative(sp.Matrix(state), N), delta_x_h3))

# # test
# for i in range(len(state)):
#     display(sp.simplify(f_numeric_ex_heun3[i] - delta_x_h3[i]))


# In[21]:


# Euler half-step

A = sp.Matrix([
    [sp.Rational(0, 1), sp.Rational(0, 1)],
    [sp.Rational(1, 2), sp.Rational(0, 1)]
])
b = sp.Matrix([sp.Rational(0, 1), sp.Rational(1, 1)])
c = sp.Matrix([sp.Rational(0, 1), sp.Rational(1, 1)])
s = A.shape[0]

delta_x_e_half = numeric_integration(f_param, state, h, A, b, c)
display(sp.Eq(time_derivative(sp.Matrix(state), N), delta_x_e_half))

# test
for i in range(len(state)):
    display(sp.simplify(f_numeric_ex_halfstep[i] - delta_x_e_half[i]))


# In[22]:


# generate c code for simulation, uses the non-linear model
# uses the discretization stored in delta_x

delta_x = delta_x_h2

n = len(state)
x = sp.MatrixSymbol("state", n, 1)
next_x = sp.MatrixSymbol("next_state", n, 1)

q = 3
u = sp.MatrixSymbol("inputs", q, 1)

subs_dict = {}
for i, var in enumerate(state):
    subs_dict[var] = x[i,0]
subs_dict[f_x] = u[0,0]
subs_dict[f_y] = u[1,0]
subs_dict[t_rw] = u[2,0]
display(subs_dict)

system_model = delta_x.subs(subs_dict)
system_model = sp.expand(system_model)
system_model = sp.Eq(next_x, x + system_model)

display(system_model)

[(c_name, c_code), (h_name, c_header)] = codegen(('system_model', system_model), "C99", "system_model", header=False, empty=False)
print(c_name)
print(c_code)
print(h_name)
print(c_header)

with open("../generated/" + h_name, "w+") as f:
    f.write(c_header)
with open("../generated/" + c_name, "w+") as f:
    f.write(c_code)


# In[23]:


# define parameters for multiple SMVs

# constants

k = sp.symbols("k")

# geometry

d_px = sp.symbols("d_{p\,x}")
d_py = sp.symbols("d_{p\,y}")
d_ptheta = sp.symbols("d_{p\,\\theta}")

d_sx = sp.symbols("d_{s\,x}")
d_sy = sp.symbols("d_{s\,y}")
d_stheta = sp.symbols("d_{s\,\\theta}")

d_rx = sp.symbols("d_{r\,x}")
d_ry = sp.symbols("d_{r\,y}")
d_rtheta = sp.symbols("d_{r\,\\theta}")

param_dict = {
    "m_smv": m_smv,
    "j_smv": j_smv,
    "g": g,
    "d_{p\,x}": d_px,
    "d_{p\,y}": d_py,
    "d_{p\,\\theta}": d_ptheta,
    "d_{s\,x}": d_sx,
    "d_{s\,y}": d_sy,
    "d_{s\,\\theta}": d_stheta,
    "d_{r\,x}": d_rx,
    "d_{r\,y}": d_ry,
    "d_{r\,\\theta}": d_rtheta,
}

# input torques
t_rw1, t_rw2 = dynamicsymbols("T_rw(1:3)")

# input forces
f_x1, f_x2 = dynamicsymbols("F_x(1:3)")
f_y1, f_y2 = dynamicsymbols("F_y(1:3)")

# state variables

# x coordinate
qx1, qx2 = dynamicsymbols("q_x(1:3)")
ux1, ux2 = dynamicsymbols("q_x(1:3)", 1)
ux1_, ux2_ = dynamicsymbols("u_x(1:3)")
vx1, vx2 = dynamicsymbols("v_x(1:3)")

# y coordinate
qy1, qy2 = dynamicsymbols("q_y(1:3)")
uy1, uy2 = dynamicsymbols("q_y(1:3)", 1)
uy1_, uy2_ = dynamicsymbols("u_y(1:3)")
vy1, vy2 = dynamicsymbols("v_y(1:3)")

# rotation theta
qtheta1, qtheta2 = dynamicsymbols("q_{\\theta\,(1:3)}")
utheta1, utheta2 = dynamicsymbols("q_{\\theta\,(1:3)}", 1)
utheta1_, utheta2_ = dynamicsymbols("u_{\\theta\,(1:3)}")
vtheta1, vtheta2 = dynamicsymbols("v_{\\theta\,(1:3)}")
    
state1 = [qx1, qy1, qtheta1, vx1, vy1, vtheta1]
state2 = [qx2, qy2, qtheta2, vx2, vy2, vtheta2]

list_q1 = [qx1, qy1, qtheta1]
list_q2 = [qx2, qy2, qtheta2]
list_q = [list_q1, list_q2]
vec_q1 = sp.Matrix(list_q1)
vec_q2 = sp.Matrix(list_q2)
vec_q = [vec_q1, vec_q2]

list_u1 = [ux1, uy1, utheta1]
list_u2 = [ux2, uy2, utheta2]
list_u = [list_u1, list_u2]
vec_u1 = sp.Matrix(list_u1)
vec_u2 = sp.Matrix(list_u2)
vec_u = [vec_u1, vec_u2]

# Lagrange multiplier

lambda_ = sp.MatrixSymbol("lambda", 3, 1)

# Others

L1_ = sp.symbols("L_1")
f1_ = sp.MatrixSymbol("f_1", int(n/2), 1)
u1_ = sp.MatrixSymbol("u_1", p, 1)
x1_ = sp.MatrixSymbol("x_1", n, 1)
y1_ = sp.MatrixSymbol("y_1", q, 1)
M1_ = sp.MatrixSymbol("M_1", int(n/2), int(n/2))
A1_ = sp.MatrixSymbol("A_1", n, n)
B1_ = sp.MatrixSymbol("B_1", n, p)
C1_ = sp.MatrixSymbol("C_1", q, n)

L2_ = sp.symbols("L_2")
f2_ = sp.MatrixSymbol("f_2", int(n/2), 1)
u2_ = sp.MatrixSymbol("u_2", p, 1)
x2_ = sp.MatrixSymbol("x_2", n, 1)
y2_ = sp.MatrixSymbol("y_2", q, 1)
M2_ = sp.MatrixSymbol("M_2", int(n/2), int(n/2))
A2_ = sp.MatrixSymbol("A_2", n, n)
B2_ = sp.MatrixSymbol("B_2", n, p)
C2_ = sp.MatrixSymbol("C_2", q, n)

A_ = sp.MatrixSymbol("A", 2*n, 2*n)
B_ = sp.MatrixSymbol("B", 2*n, 2*p)
C_ = sp.MatrixSymbol("C", 2*q, 2*n)


# In[24]:


# use sympy mechanics toolbox to define smv

# create rotated smv reference frame
R_smv1 = ReferenceFrame("R_smv1")
R_smv1 = N.orientnew("R_smv1", "axis", [qtheta1, N.z])
R_smv1.set_ang_vel(N, utheta1*N.z)

# locate smv in world
po_smv1 = pN.locatenew("po_smv1", qx1*N.x+qy1*N.y)
po_smv1.set_vel(N, ux1*N.x + uy1*N.y)

I_smv1 = outer(R_smv1.z, R_smv1.z) * j_smv

B_smv1 = RigidBody("B_SMV1", po_smv1, R_smv1, m_smv, (I_smv, po_smv1))


# In[25]:


# define the forces

F_x1 = f_x1
F_x1 = (po_smv1, F_x1*R_smv1.x)

F_y1 = f_y1
F_y1 = (po_smv1, F_y1*R_smv1.y)

T_z1 = t_rw1
T_z1 = (R_smv1, T_z1*N.z)

forces1 = [F_x1, F_y1, T_z1]


# In[26]:


Lag1 = sp.simplify(Lagrangian(N, B_smv1))
display("Lagrangian")
# display(sp.Eq(L1_, Lag1))
# print(sp.latex(sp.Eq(L1_, Lag1)))


# In[27]:


# use sympy mechanics toolbox to define smv

# create rotated smv reference frame
R_smv2 = ReferenceFrame("R_smv2")
R_smv2 = N.orientnew("R_smv2", "axis", [qtheta2, N.z])
R_smv2.set_ang_vel(N, utheta2*N.z)

# locate smv in world
po_smv2 = pN.locatenew("po_smv2", qx2*N.x+qy2*N.y)
po_smv2.set_vel(N, ux2*N.x + uy2*N.y)

I_smv2 = outer(R_smv2.z, R_smv2.z) * j_smv

B_smv2 = RigidBody("B_SMV2", po_smv2, R_smv2, m_smv, (I_smv, po_smv2))


# In[28]:


# define the forces

F_x2 = f_x2
F_x2 = (po_smv2, F_x2*R_smv2.x)

F_y2 = f_y2
F_y2 = (po_smv2, F_y2*R_smv2.y)

T_z2 = t_rw2
T_z2 = (R_smv2, T_z2*N.z)

forces2 = [F_x2, F_y2, T_z2]


# In[29]:


Lag2 = sp.simplify(Lagrangian(N, B_smv2))
display("Lagrangian")
# display(sp.Eq(L2_, Lag2))
# print(sp.latex(sp.Eq(L2_, Lag2)))


# In[30]:


# get M, A and B
LM1 = LagrangesMethod(Lag1, [qx1, qy1, qtheta1], forcelist=forces1, frame=N)
lag_eqs1 = LM1.form_lagranges_equations()
M1 = LM1.mass_matrix
f1 = LM1.forcing
display(sp.Eq(M1_, M1))
display(sp.Eq(f1_, f1))
print(sp.latex(sp.Eq(M1_, M1)))
print(sp.latex(sp.Eq(f1_, f1)))

# linearizer1 = LM1.to_linearizer(q_ind=[qx1, qy1, qtheta1], qd_ind=[ux1, uy1, utheta1])
# op_point1 = {qx1: qx1, qy1: qy1, qtheta1: qtheta1, ux1: ux1, uy1: uy1, utheta1: utheta1, f_x1: f_x1, f_y1: f_y1, t_rw1: t_rw1}
# M1, A1, B1 = sp.simplify(linearizer1.linearize(A_and_B=False, op_point=op_point1))
# display(sp.Eq(M1_, M1))
# display(sp.Eq(A1_, A1))
# display(sp.Eq(B1, B1_))
# u1 = sp.Matrix([f_x1, f_y1, t_rw1])
# x1 = sp.Matrix(state1)
# f1 = sp.simplify(A1@x1 + B1@u1)
# display(f1)


# In[31]:


# get M, A and B
LM2 = LagrangesMethod(Lag2, [qx2, qy2, qtheta2], forcelist=forces2, frame=N)
lag_eqs2 = LM2.form_lagranges_equations()
M2 = LM2.mass_matrix
f2 = LM2.forcing
display(sp.Eq(M2_, M2))
display(sp.Eq(f2_, f2))
print(sp.latex(sp.Eq(M2_, M2)))
print(sp.latex(sp.Eq(f2_, f2)))


# In[32]:


# formulate the docking port's positions

po_r1 = po_smv1.locatenew("po_r1", d_rx*R_smv1.x+d_ry*R_smv1.y)
display(po_r1.pos_from(pN).express(N))
po_r1_x = po_r1.pos_from(pN).express(N).dot(N.x)
po_r1_y = po_r1.pos_from(pN).express(N).dot(N.y)
po_r1_theta = qtheta1

po_r2 = po_smv2.locatenew("po_r2", d_rx*R_smv2.x+d_ry*R_smv2.y)
display(po_r2.pos_from(pN).express(N))
po_r2_x = po_r2.pos_from(pN).express(N).dot(N.x)
po_r2_y = po_r2.pos_from(pN).express(N).dot(N.y)
po_r2_theta = qtheta2

# formulate algebraic constraint
eq_x = po_r1_x - po_r2_x
eq_y = po_r1_y - po_r2_y
eq_theta = po_r1_theta - po_r2_theta + (2*k+1) * sp.pi

g = sp.Matrix([eq_x, eq_y, eq_theta])
display(sp.Eq(sp.Matrix([0, 0, 0]), g))
print(sp.latex(sp.Eq(sp.Matrix([0, 0, 0]), g)))


# In[33]:


def det_3x3(M):
    p1 = M[0, 0] * M[1, 1] * M[2, 2]
    p2 = M[0, 1] * M[1, 2] * M[2, 0]
    p3 = M[0, 2] * M[1, 0] * M[2, 1]
    p1 = sp.simplify(p1)
    p2 = sp.simplify(p2)
    p3 = sp.simplify(p3)
    print ("simplifying p")
    p = sp.simplify(p1 + p2)
    print(sp.count_ops(p))
    p = sp.simplify(p + p3)
    n1 = M[2, 0] * M[1, 1] * M[0, 2]
    n2 = M[2, 1] * M[1, 2] * M[0, 0]
    n3 = M[2, 2] * M[1, 0] * M[0, 1]
    print("simplifying n")
    n1 = sp.simplify(n1)
    n2 = sp.simplify(n2)
    n3 = sp.simplify(n3)
    n = sp.simplify(n1 + n2)
    print(sp.count_ops(n))
    n = sp.simplify(n + n3)
    det = p-n
    return det

# A1 = sp.Matrix([[1, 0, 0], [0, 1, 0], [0, 0, 1]])
# print(det_3x3(A1))
# A2 = sp.Matrix([[1, 1, 1], [3, -3, 2], [-1, 2, -2]])
# print(det_3x3(A2))
# A3 = sp.Matrix([[1, -1, -3], [2, 1, 0], [-1, 2, 7]])
# print(det_3x3(A3))
# A4 = sp.Matrix([[2, 1, 1], [1, -1, 1], [1, 2, 1]])
# print(det_3x3(A4))


# In[34]:


def adj_inv(M):
    M_ = M.adjugate()
    #print(M_)
    #print(sp.count_ops(M_))

    M_inv = sp.zeros(M.shape[0], M.shape[1])
    for i in range(M_.shape[0]):
        print("Row {} of {}".format(i, M_.shape[0]))
        for j in range(M_.shape[1]):
            print("Column {} of {}".format(j, M_.shape[0]))
            #print(str(i) + " " + str(j))
            #print(sp.count_ops(M_[i, j]))
            M_inv[i, j] = sp.simplify(M_[i, j])
            #print(sp.count_ops(M_inv[i, j]))
    #print(M_inv)
    #print(sp.count_ops(M_inv))

    print("Calculating determinant...")
    # determinant_M = det_3x3(M)
    determinant_M = M.det()
#     print(determinant_M)
    print(sp.count_ops(determinant_M))
    print("Simplifying determinant...")
#     print("Trigexpand")
#     print(sp.count_ops(determinant_M))
#     sp.expand_trig(determinant_M)
#     print("Trigsimp")
#     print(sp.count_ops(determinant_M))
#     sp.trigsimp(determinant_M)
    determinant_M = sp.simplify(determinant_M, rational=True, )
    #print(determinant_M)
    print(sp.count_ops(determinant_M))

    print("Calculating inverse...")
    M_inv = 1/determinant_M * M_inv
    print(sp.count_ops(M_inv))
    print("Simplifying inverse...")
    M_inv = sp.simplify(M_inv)
    
    return M_inv


# In[35]:


# formulate the equation for lambda from the index reduction used in the thesis

print("This may take 2h or longer if you change the seed in cell index 0.")
print(datetime.datetime.now())

list_m = [M1, M2]
list_f = [f1, f2]

def inner_sum(j):
    sum_ = sp.Matrix([0, 0, 0])
    for l in range(2):
        grad_g_qj = g.jacobian(list_q[j])
        #display(grad_g_qj)
        left = grad_g_qj @ vec_u[j]
        #display(left)
        left = left.jacobian(list_q[l])
        #display(left)
        sum_ += left @ vec_u[l]
        #display(sum_)
    return sp.simplify(sum_)
# display(inner_sum(1))

left_sum = sp.Matrix([0, 0, 0])
for j in range(2):
    grad_g_q_j = g.jacobian(list_q[j])
    left_sum += grad_g_q_j @ list_m[j]**-1 @ list_f[j]
    left_sum += inner_sum(j)
# display(left_sum)
# print(sp.latex(left_sum))

right_sum = sp.zeros(3)
for j in range(2):
    grad_g_q_j = g.jacobian(list_q[j])
    right_sum += grad_g_q_j @ list_m[j]**-1 @ grad_g_q_j.T
right_sum = sp.simplify(right_sum)
# display(right_sum)
# print(sp.latex(right_sum))

lhs = sp.Matrix([0, 0, 0])
rhs = sp.simplify(left_sum - right_sum@lambda_)
# display(sp.Eq(lhs, rhs))
# print(sp.latex(sp.Eq(lhs, rhs)))

# manually solve for lambda
lambda_sol = adj_inv(right_sum)@left_sum
print("Solution for lambda found")
# display(lambda_sol)
# print(sp.latex(lambda_sol))

# solve for lambda
# display(sp.solve(sp.Eq(lhs, rhs), lambda_))
print(datetime.datetime.now())


# In[ ]:





# In[37]:


# # save formula for lambda:
# with open("lambda.txt", "w") as outf:
#     outf.write(str(lambda_sol))
# # check
# with open("lambda.txt", "r") as inf:
#     text = inf.read()
#     lambda_check = sp.sympify(text)
# print(lambda_sol)


# In[38]:


# calculate reaction forces
print("calculating forces...")
s1 = sp.simplify(g.jacobian(list_q[0]))
# display(s1)
s2 = sp.simplify(g.jacobian(list_q[1]))
# display(s2)
print("f1_react")
# display(s1.T@lambda_sol)
f1_react = sp.simplify(s1.T@lambda_sol)
#print(sp.latex(f1_react))
# display(f1)
print("f2_react")
# display(s2.T@lambda_sol)
f2_react = sp.simplify(s2.T@lambda_sol)
# display(f2)
#print(sp.latex(f2_react))


# In[39]:


print("calculating nonlinear sytem functions")
forcing_1 = f1 + f1_react
forcing_2 = f2 + f2_react
sys1 = M1.inv() * forcing_1
sys2 = M2.inv() * forcing_2
print("simplifying f1")
sys1 = sp.simplify(sys1)
print("simplifying f2")
sys2 = sp.simplify(sys2)
# forcing gives change of u, we need to add the integrators
sys1 = sp.Matrix([ux1, uy1, utheta1, sys1[0], sys1[1], sys1[2]])
sys2 = sp.Matrix([ux2, uy2, utheta2, sys2[0], sys2[1], sys2[2]])
# construct one ODE system from the two coupled SMVs:
f = sp.Matrix([sys1[0], sys1[1], sys1[2], sys2[0], sys2[1], sys2[2], sys1[3], sys1[4], sys1[5], sys2[3], sys2[4], sys2[5]])
# reminder:
# state1 = [qx1, qy1, qtheta1, vx1, vy1, vtheta1]
# state2 = [qx2, qy2, qtheta2, vx2, vy2, vtheta2]
state = [qx1, qy1, qtheta1, qx2, qy2, qtheta2, vx1, vy1, vtheta1, vx2, vy2, vtheta2]
# display(f)


# In[40]:


print("coordinate transformation")
print(datetime.datetime.now())
# perform coordinate transformation
# this moves the coordinate frame rotation from the velocity ODEs to the coordinate ODEs
# thus, velocity controllers can be fed with an easily linearized model

# variable substitution
f = f.subs(ux1, ux1_)
f = f.subs(uy1, uy1_)
f = f.subs(utheta1, utheta1_)
f = f.subs(ux2, ux2_)
f = f.subs(uy2, uy2_)
f = f.subs(utheta2, utheta2_)

# display("f with u substitution")
# display(f)

R = sp.Matrix([
    [sp.cos(qtheta1), sp.sin(qtheta1), 0, 0, 0, 0],
    [-sp.sin(qtheta1), sp.cos(qtheta1), 0, 0, 0, 0],
    [0, 0, 1, 0, 0, 0],
    [0, 0, 0, sp.cos(qtheta1), sp.sin(qtheta1), 0],
    [0, 0, 0, -sp.sin(qtheta1), sp.cos(qtheta1), 0],
    [0, 0, 0, 0, 0, 1]
])

display("Rotation Matrix from N to R")
display(R)
print(sp.latex(R))

u_dot = f[6:]

# display("u_dot")
# display(u_dot)

v_of_u = R * sp.Matrix([[ux1_], [uy1_], [utheta1_], [ux2_], [uy2_], [utheta2_]])

# display("v from u, aka T(q, u)")
# display(v_of_u)

u_of_v = R.T * sp.Matrix([[vx1], [vy1], [vtheta1], [vx2], [vy2], [vtheta2]])

# display("u from v, aka T^-1(q, v)")
# display(u_of_v)

# q_dot is u_dot is T^-1(q, v)
q_dot = sp.Matrix([[u_of_v[0]], [u_of_v[1]], [u_of_v[2]], [u_of_v[3]], [u_of_v[4]], [u_of_v[5]]])
# display("q_dot")
# display(q_dot)

# v_dot is a bit more complicated:
# Dv/Dt = dT/dq * T^-1(q, v) + dT/du * f(q, T^-1(v))

dT_by_dq = v_of_u.jacobian([qx1, qy1, qtheta1, qx2, qy2, qtheta2])
# display("dT_by_dq")
# display(dT_by_dq)
term_1 = dT_by_dq * v_of_u
term_1 = term_1.subs(ux1_, u_of_v[0, 0])
term_1 = term_1.subs(uy1_, u_of_v[1, 0])
term_1 = term_1.subs(utheta1_, u_of_v[2, 0])
term_1 = term_1.subs(ux2_, u_of_v[3, 0])
term_1 = term_1.subs(uy2_, u_of_v[4, 0])
term_1 = term_1.subs(utheta2_, u_of_v[5, 0])
term_1 = sp.simplify(term_1)
# display("Term 1")
# display(term_1)

dT_by_du = v_of_u.jacobian([ux1_, uy1_, utheta1_, ux2_, uy2_, utheta2_])
# display("dT_by_du")
# display(dT_by_du)
term_2 = dT_by_du * sp.Matrix([[u_dot[0]], [u_dot[1]], [u_dot[2]], [u_dot[3]], [u_dot[4]], [u_dot[5]]])
term_2 = term_2.subs(ux1_, u_of_v[0, 0])
term_2 = term_2.subs(uy1_, u_of_v[1, 0])
term_2 = term_2.subs(utheta1_, u_of_v[2, 0])
term_2 = term_2.subs(ux2_, u_of_v[3, 0])
term_2 = term_2.subs(uy2_, u_of_v[4, 0])
term_2 = term_2.subs(utheta2_, u_of_v[5, 0])
term_2 = sp.simplify(term_2)
# display("Term 2")
# display(term_2)

v_dot = term_1 + term_2
# display("v_dot")
# display(v_dot)

f = sp.Matrix(
    [
        [q_dot[0, 0]],
        [q_dot[1, 0]],
        [q_dot[2, 0]],
        [q_dot[3, 0]],
        [q_dot[4, 0]],
        [q_dot[5, 0]],
        [v_dot[0, 0]],
        [v_dot[1, 0]],
        [v_dot[2, 0]],
        [v_dot[3, 0]],
        [v_dot[4, 0]],
        [v_dot[5, 0]],
    ])
display("Transformed f")
display(sp.Eq(time_derivative(sp.Matrix(state), N), f))
print(sp.latex(sp.Eq(time_derivative(sp.Matrix(state), N), f)))
print(datetime.datetime.now())


# In[41]:


# manually linearize f (first order taylor approximation)

f_lin = deepcopy(f)

delta_qx1 = sp.symbols("delta_qx1", real=True)
delta_qy1 = sp.symbols("delta_qy1", real=True)
delta_qtheta1 = sp.symbols("delta_qtheta1", real=True)
delta_qx2 = sp.symbols("delta_qx2", real=True)
delta_qy2 = sp.symbols("delta_qy2", real=True)
delta_qtheta2 = sp.symbols("delta_qtheta2", real=True)

delta_vx1 = sp.symbols("delta_vx1", real=True)
delta_vy1 = sp.symbols("delta_vy1", real=True)
delta_vtheta1 = sp.symbols("delta_vtheta1", real=True)
delta_vx2 = sp.symbols("delta_vx2", real=True)
delta_vy2 = sp.symbols("delta_vy2", real=True)
delta_vtheta2 = sp.symbols("delta_vtheta2", real=True)

delta_fx1 = sp.symbols("delta_fx1", real=True)
delta_fy1 = sp.symbols("delta_fy1", real=True)
delta_trw1 = sp.symbols("delta_trw1", real=True)
delta_fx2 = sp.symbols("delta_fx2", real=True)
delta_fy2 = sp.symbols("delta_fy2", real=True)
delta_trw2 = sp.symbols("delta_trw2", real=True)

offsets = []
offset_dict = {
    qx1: 0, 
    qy1: 0, 
    qtheta1: 0, 
    qx2: 0, 
    qy2: 0, 
    qtheta2: 0, 
    vx1: 0, 
    vy1: 0, 
    vtheta1: 0, 
    vx2: 0, 
    vy2: 0, 
    vtheta2: 0, 
    f_x1: 0, 
    f_y1: 0, 
    t_rw1: 0,
    f_x2: 0, 
    f_y2: 0, 
    t_rw2: 0,
}
for i in range(12):
    offsets.append(f_lin[i].subs(offset_dict))
    #display(offsets[i])

# A
A = f_lin.jacobian([qx1, qy1, qtheta1, qx2, qy2, qtheta2, vx1, vy1, vtheta1, vx2, vy2, vtheta2])
# display("Jacobian w.r.t. states")
# display(A)
# B 
B = f_lin.jacobian([f_x1, f_y1, t_rw1, f_x2, f_y2, t_rw2])
# display("Jacobian w.r.t. inputs")
# display(B)

x = sp.Matrix([
    delta_qx1, 
    delta_qy1, 
    delta_qtheta1,
    delta_qx2, 
    delta_qy2, 
    delta_qtheta2, 
    delta_vx1, 
    delta_vy1, 
    delta_vtheta1, 
    delta_vx2, 
    delta_vy2, 
    delta_vtheta2
])
# display(x)

u = sp.Matrix([delta_fx1, delta_fy1, delta_trw1, delta_fx2, delta_fy2, delta_trw2])
# display(u)

offset = sp.Matrix(offsets)
# display(offset)

linearized = A*x + B*u + offset
# display("linearization without operating point")
# display(linearized)

# insert the operating point AFTER LINEARIZATION
# assume rotation around z axis and velocities to be small
op_point = {
    qx1: delta_qx1, 
    qy1: delta_qy1, 
    qtheta1: delta_qtheta1, 
    qx2: delta_qx2, 
    qy2: delta_qy2, 
    qtheta2: delta_qtheta2, 
    vx1: 0, 
    vy1: 0, 
    vtheta1: 0, 
    vx2: 0, 
    vy2: 0, 
    vtheta2: 0, 
    f_x1: delta_fx1, 
    f_y1: delta_fy1, 
    t_rw1: delta_trw1, 
    f_x2: delta_fx2, 
    f_y2: delta_fy2, 
    t_rw2: delta_trw2
}
display(op_point)
print(sp.latex(op_point))

# A
A = linearized.jacobian([
    delta_qx1, 
    delta_qy1, 
    delta_qtheta1, 
    delta_qx2, 
    delta_qy2, 
    delta_qtheta2, 
    delta_vx1, 
    delta_vy1, 
    delta_vtheta1, 
    delta_vx2, 
    delta_vy2, 
    delta_vtheta2
])
for variable, substitute in op_point.items():
    A = A.subs(variable, substitute)
# display(sp.Eq(A_, A))
# print(sp.latex(sp.Eq(A_, A)))

# B 
B = linearized.jacobian([delta_fx1, delta_fy1, delta_trw1, delta_fx2, delta_fy2, delta_trw2])

for variable, substitute in op_point.items():
    B = B.subs(variable, substitute)
# display(sp.Eq(B_, B))
# print(sp.latex(sp.Eq(B_, B)))


# In[42]:


# parameterize
print(datetime.datetime.now())

values = {
    "m_smv": sp.S(7.5),
    "j_smv": sp.S(0.099),
    "g": sp.S(9.81),
    "d_{r\,x}": sp.S(0.12),
    "d_{r\,y}": sp.S(0),
    "d_{r\,\\theta}": -sp.pi,
}

f_param = f
A_param = A
B_param = B

for param, val in values.items():
    print(param)
    print("f")
    f_param = sp.simplify(f_param.subs(param_dict[param], val))
    # print("A")
    # A_param = sp.simplify(A_param.subs(param_dict[param], val))
    # print("B")
    # B_param = sp.simplify(B_param.subs(param_dict[param], val))
    
display(sp.Eq(time_derivative(sp.Matrix(state), N), f_param))
# display(sp.Eq(A_, A_param))
# display(sp.Eq(B_, B_param))
print(datetime.datetime.now())


# In[43]:


# numeric integration using heun's method

def k_substitution(e, state, h, k1, a_ij):
    temp_dict = {}
    for i, v in enumerate(state):
        temp_sym = sp.symbols("tempsym_" + str(i))
        temp_dict[v] = temp_sym
    e = e.subs(temp_dict)
    for i, v in enumerate(state):
        e = e.subs(temp_dict[v], v + a_ij*h*k1[i])
    return e

k1 = sp.Matrix(f_param)
k2 = k_substitution(sp.Matrix(f_param), state, h, k1, sp.Rational(1, 1))

f_numeric_ex_heun = (sp.Rational(1, 2) * h * (k1 + k2))

# display(sp.Eq(time_derivative(sp.Matrix(state), N), f_numeric_ex_heun))


# In[ ]:


# generate c code for simulation, uses the non-linear model
# uses the discretization stored in f_numeric_ex_heun

delta_x = f_numeric_ex_heun

n = len(state)
x = sp.MatrixSymbol("state", n, 1)
next_x = sp.MatrixSymbol("next_state", n, 1)

q = 6
u = sp.MatrixSymbol("inputs", q, 1)

display(state)
subs_dict = {}
for i, var in enumerate(state):
    subs_dict[var] = x[i,0]
subs_dict[f_x1] = u[0,0]
subs_dict[f_y1] = u[1,0]
subs_dict[t_rw1] = u[2,0]
subs_dict[f_x2] = u[3,0]
subs_dict[f_y2] = u[4,0]
subs_dict[t_rw2] = u[5,0]
display(subs_dict)

system_model = delta_x.subs(subs_dict)
system_model = sp.expand(system_model)
system_model = sp.Eq(next_x, x + system_model)

display(system_model)

[(c_name, c_code), (h_name, c_header)] = codegen(('system_model_dae', system_model), "C99", "system_model_dae", header=False, empty=False)
# print(c_name)
# print(c_code)
# print(h_name)
# print(c_header)

with open("../generated/" + h_name, "w+") as f:
    f.write(c_header)
with open("../generated/" + c_name, "w+") as f:
    f.write(c_code)
    
print("done")


# In[ ]:




