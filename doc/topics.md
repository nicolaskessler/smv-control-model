# Topics used

| Topic Name | Datatype | ID | Command or Output? | Rate / Hz |
|---|---|---|---|---|
| "Telecommand Data" | sTelecommandData | 1500 | Command | 50 |
| "Sensor Data" | sSensorData | 1505 | Output | 50 |
| "Component Data" | sInitComp | 1501 | Command | 50 |
| "VehicleNetTopic" | sVehicleNet | 1502 | Output | 50 |
| "Position Data" | sPositionData | 1503 | Output | 50 |
| "IMU Data" | sIMUData | 1504 | Output | 50 |
