#include "rodos.h"
#include "hw_specific.h"
#include <math.h>

#include "tutorial_twist_command.hpp"
#include "tutorial_pose.hpp"
#include "topics.h"

#include "rw_velocity_control.hpp"

extern "C" {
    #include "system_model.h"
}

static Application a("SMVSimulation");

static CommBuffer<sControl> command_cb1;
static Subscriber command_sub1(ControlTopic, command_cb1, "sControl Command Subscriber 1");

static CommBuffer<sControl> command_cb2;
static Subscriber command_sub2(ControlTopic2, command_cb2, "sControl Command Subscriber 2");

class RobotSimulation : public Thread {

    private:

    CommBuffer<sControl> &command_cb;
    Topic<sSensorData> &SensorDataTopic;
    Topic<pose> &tutorial_pose;

    int id;

    public:

    RobotSimulation(
        CommBuffer<sControl> &command_cb_ref, 
        Topic<sSensorData> &SensorDataTopicRef, 
        Topic<pose> &tutorial_pose_ref,
        float x0,
        float y0,
        float theta0,
        int id
        ): 
    Thread(name="Robot Simulation", 10),
    SensorDataTopic(SensorDataTopicRef),
    tutorial_pose(tutorial_pose_ref),
    command_cb(command_cb_ref),
    id(id),
    x{x0,y0,theta0,0,0,0,0,0} 
    {}

    void run() {
        PRINTF("Hello World! "
        "I am a robot simulator and I am having a great day!\n");

        int64_t i = 0;
        int64_t iteration_begin = NOW();
        int64_t nextbeat = iteration_begin;
        sControl c;

        while (1) {

            i++;
            iteration_begin = NOW();

            command_cb.get(c);
            sControl_to_u(c, u);

            for(int i = 0; i < n; i++) {
                old_x[i] = x[i];
            }
            system_model(u, old_x, x);

            // publish the executed motor thrust
            publish_executed_u(u);
            // finally publish the resulting IMU message
            publish_state(x);
            nextbeat = iteration_begin + 50*MILLISECONDS;

            // print the new state
            // PRINTF("Iteration %lld\n", i);
            // PRINTF("Started: %lld, Now: %lld\n", iteration_begin, NOW());
            // PRINTF("Next beat %lld\n", nextbeat);
            // PRINTF("Inputs: f_0: %2.2f f_1: %2.2f, f_2: %2.2f\n", u[0], u[1], u[2]);
            // PRINTF("Inputs: rpm: %2.2f t_1: %2.2f, t_2: %2.2f\n", u[3], u[4], u[5]);
            // PRINTF("Observed state: X: %2.2f Y: %2.2f, t: %2.2f, va: %2.2f\n", x[0], x[1], x[2], x[7]);

            // time delay
            if (nextbeat < iteration_begin + 10*MILLISECONDS) {
                // PRINTF("%s is starving!\n", this->name);
            }
            AT(nextbeat);
        }
    }

    int n = 8;
    double old_x[8];
    double x[8];
    double u[6];

    void sControl_to_u(sControl c, double u[6]) {
        // times are given in Milliseconds
        // c.rpm is a PWM value in %
        u[0] = ((double) c.th_time[0]) / 50.;
        u[1] = ((double) c.th_time[1]) / 50.;
        u[2] = ((double) c.th_time[2]) / 50.;
        u[3] = get_current_PWM(id) * (((double) c.rw_time) / 50); // c.rpm/100. previously
        u[4] = c.thruster_theta[0] * 3.141 / 180.;
        u[5] = c.thruster_theta[1] * 3.141 / 180.;
    }

    void publish_state(double x[8]) {
        pose p;

        point pt{0};
        pt.x = x[0];
        pt.y = x[1];
        p.position = pt;

        // set the orientation
        quaternion o(cos(x[2]/2), 0, 0, sin(x[2]/2));
        p.orientation = o;

        tutorial_pose.publish(p);

        // publish RW speed as sSensorData in SensorDataTopic
        sSensorData s;
        s.scale = 0;
        s.Vbat = 0;
        s.motorSpeed = -60. * x[7] / (2.*M_PI);
        // PRINTF("Motorspeed: %2.2f\n", s.motorSpeed);
        SensorDataTopic.publish(s);

    }

    void publish_executed_u(double u[3]) {
        twist t = {
            .vel_fb = u[0],
            .vel_lr = u[1],
            .anv_z = u[2]
        };
        tutorial_twist_command_executed.publish(t);
    }
    
};

RobotSimulation ss1(command_cb1, SensorDataTopic, tutorial_pose, -0.4, -0.4, 3.141, 1);
RobotSimulation ss2(command_cb2, SensorDataTopic2, tutorial_pose2, 0.4, -0.4, 0., 2);
