#include "rodos.h"
#include "rw_velocity_control.hpp"
#include <math.h>

#include "topics.h"

static Application a("SMVRPMControl");

static CommBuffer<sControl> command_cb;
static Subscriber command_sub(ControlTopic, command_cb, "sControl Command Subscriber");

static CommBuffer<sSensorData> ModesSensorDataBuffer;
static Subscriber ModesSensorSubscriber(SensorDataTopic, ModesSensorDataBuffer);

static CommBuffer<sControl> command_cb2;
static Subscriber command_sub2(ControlTopic2, command_cb2, "sControl Command Subscriber 2");

static CommBuffer<sSensorData> ModesSensorDataBuffer2;
static Subscriber ModesSensorSubscriber2(SensorDataTopic2, ModesSensorDataBuffer2);

uint32_t ModesPeriod = 50;

// RESET PI 1, 0.5
// we disable the I-controller and anti-windup
sPID_Data PID_MotorSpeed = {1., 0.00, 0,                               // float Kp, Ki, Kd;
					       0, 0, 0, 							     // float P, I, D;
					       0, 0, 0, 0, 0, 0, 1,                      // float e, e_1, Upid, Upid_1, Usat, Usat_1, Kr;
					       1000, -1000, (float)ModesPeriod/1000,     // float Umax, Umin, T;
                           0};                                       // bool AntiWindup;
sPID_Data PID_MotorSpeed2 = {1., 0.00, 0,                               // float Kp, Ki, Kd;
					       0, 0, 0, 							     // float P, I, D;
					       0, 0, 0, 0, 0, 0, 1,                      // float e, e_1, Upid, Upid_1, Usat, Usat_1, Kr;
					       1000, -1000, (float)ModesPeriod/1000,     // float Umax, Umin, T;
                           0};                                       // bool AntiWindup;

void PID(sPID_Data* PID, float error)
{
	PID->e = (float) error;

	PID->P = PID->Kp*PID->e;

	if (PID->AntiWindup)
	{
		PID->I+= (PID->Ki*PID->e + PID->Kr * (PID->Usat_1 - PID->Upid_1)) * PID->T;
	}

	else {PID->I+= PID->Ki * PID->e * PID->T;}

	PID->D = PID->Kd * (PID->e - PID->e_1) / PID->T;

	PID->Upid = PID->P + PID->I + PID->D;

	if (PID->Upid >= PID->Umax)
	{
		PID->Usat = PID->Umax;
	}

	else if (PID->Upid <= PID->Umin)
	{
		PID->Usat = PID->Umin;
	}

	else
	{
		PID->Usat = PID->Upid;
	}

	PID->Upid_1 = PID->Upid;
	PID->Usat_1 = PID->Usat;
	PID->e_1 = PID->e;
}

sSensorData SensorDataReceiver;
sControl ControlDataReceiver;

float get_current_PWM1() {
    ModesSensorDataBuffer.getOnlyIfNewData(SensorDataReceiver);
    command_cb.getOnlyIfNewData(ControlDataReceiver);
    float MotorSpeedError = ControlDataReceiver.rpm - SensorDataReceiver.motorSpeed;
    // PRINTF("ControlDataReceiver.rpm: %2.2f\nSensorDataReceiver.motorSpeed: %2.2f\n", ControlDataReceiver.rpm, SensorDataReceiver.motorSpeed);
    PID(&PID_MotorSpeed, MotorSpeedError);
    // PRINTF("Usat: %2.2f\n", PID_MotorSpeed.Usat/1000.);
    return PID_MotorSpeed.Usat / 1000.;
}

float get_current_PWM2() {
    ModesSensorDataBuffer2.getOnlyIfNewData(SensorDataReceiver);
    command_cb2.getOnlyIfNewData(ControlDataReceiver);
    float MotorSpeedError = ControlDataReceiver.rpm - SensorDataReceiver.motorSpeed;
    // PRINTF("ControlDataReceiver.rpm: %2.2f\nSensorDataReceiver.motorSpeed: %2.2f\n", ControlDataReceiver.rpm, SensorDataReceiver.motorSpeed);
    PID(&PID_MotorSpeed2, MotorSpeedError);
    // PRINTF("Usat: %2.2f\n", PID_MotorSpeed.Usat/1000.);
    return PID_MotorSpeed2.Usat / 1000.;
}

float get_current_PWM(int n) {
	if (n == 1) {
		return get_current_PWM1();
	}
	else {
		return get_current_PWM2();
	}
}
