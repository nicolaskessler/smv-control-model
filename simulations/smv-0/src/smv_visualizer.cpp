extern "C" {
#include <SDL2/SDL.h>
#include "lvgl/lvgl.h"
#include "lvgl_init.h"
}

#include "rodos.h"
#include <cmath>
#include <functional>

#include "beacon.hpp"
#include "ping_responder.hpp"

#include "tutorial_pose.hpp"

#include "topics.h"

static CommBuffer<sControl> command_cb1;
static Subscriber command_sub1(ControlTopic, command_cb1, "sControl Command Subscriber 1");

static CommBuffer<pose> pose_cb1;
static Subscriber posecbs1(tutorial_pose, pose_cb1, "Pose_Subs 1");

static CommBuffer<sPositionData> st_pose_cb1;
static Subscriber st_posecbs1(PositionDataTopic, st_pose_cb1, "Star_Tracker_Pose_Subs 1");

static CommBuffer<sControl> command_cb2;
static Subscriber command_sub2(ControlTopic2, command_cb2, "sControl Command Subscriber 2");

static CommBuffer<pose> pose_cb2;
static Subscriber posecbs2(tutorial_pose2, pose_cb2, "Pose_Subs 2");

static CommBuffer<sPositionData> st_pose_cb2;
static Subscriber st_posecbs2(PositionDataTopic2, st_pose_cb2, "Star_Tracker_Pose_Subs 2");

static Application a("SMV-Visualizer");

static Semaphore gui_sem;

class ScreenRunner: public Thread {

    public:

    ScreenRunner(): Thread(name="Screen Runner", 1) {}

    void run () {
        TIME_LOOP(NOW() + 1*SECONDS, 5*MILLISECONDS) {
            PROTECT_IN_SCOPE(gui_sem);
            PRINTF("drawing\n");
            lv_task_handler();
            lv_tick_inc(5);
        }
    }

} sr;

class RobotVisualizer: public Thread {
    
    public:

    struct vehic_point
    {
        double x;
        double y;
    };

    static int counter;

    RobotVisualizer(): Thread(name="SMV Visualizer", 10) {}

    void init() {
        /*Initialize LittlevGL*/
        lv_init();

        /*Initialize the HAL (display, input devices, tick) for LittlevGL*/
        hal_init();

        init_gui();
    }

    void run() {

        sControl c;
        pose p;
        sPositionData st_p;

        sControl c2;
        pose p2;
        sPositionData st_p2;

        PRINTF("SMV visualizer starting...\n");

        while (1) {

            AT(NOW() + 50*MILLISECONDS);

            command_cb1.get(c);
            pose_cb1.get(p);
            st_pose_cb1.get(st_p);

            command_cb2.get(c2);
            pose_cb2.get(p2);
            st_pose_cb2.get(st_p2);

            PRINTF("X: %2.2f, Y: %2.2f, t: %2.2f\n", p.position.x, p.position.y, angle_from_quaternion(p.orientation));

            update_gui(
                // SMV 1
                p.position.x, 
                p.position.y, 
                angle_from_quaternion(p.orientation), 
                st_p.PosX, st_p.PosY, 
                st_p.theta, 
                c.thruster_theta[0], 
                c.thruster_theta[1],
                // SMV 2
                p2.position.x, 
                p2.position.y, 
                angle_from_quaternion(p2.orientation), 
                st_p2.PosX, st_p2.PosY, 
                st_p2.theta, 
                c2.thruster_theta[0], 
                c2.thruster_theta[1]
                );
        }
    }

    float angle_from_quaternion(quaternion q) {
        return 2 * atan2(q.k, q.r);
    }

    static void event_handler(lv_obj_t * obj, lv_event_t event)
    {
        
    }

    static void init_gui() {

        lv_event_cb_t cb = (void (*)(_lv_obj_t*, unsigned char))&RobotVisualizer::event_handler;

        canv1 = lv_canvas_create(lv_scr_act(), NULL);
        static lv_color_t cbuf[LV_CANVAS_BUF_SIZE_TRUE_COLOR(LV_HOR_RES_MAX, LV_VER_RES_MAX)];
        lv_canvas_set_buffer(canv1, cbuf, LV_HOR_RES_MAX, LV_VER_RES_MAX, LV_IMG_CF_TRUE_COLOR);
        lv_obj_set_pos(canv1, 0, 0);
        lv_obj_set_event_cb(canv1, event_handler);
        
        draw_background();
    }

    static void draw_background() {

        // static lv_draw_rect_dsc_t rect_dsc;
        // lv_draw_rect_dsc_init(&rect_dsc);

        static lv_draw_label_dsc_t label_dsc;
        lv_draw_label_dsc_init(&label_dsc);
        label_dsc.color = LV_COLOR_WHITE;

        static lv_draw_line_dsc_t style_line;
        lv_draw_line_dsc_init(&style_line);
        style_line.color = LV_COLOR_WHITE;

        lv_canvas_fill_bg(canv1, LV_COLOR_BLACK, LV_OPA_COVER);
        lv_canvas_draw_text(canv1, 5, 5, 100, &label_dsc, "MAP", LV_LABEL_ALIGN_LEFT);
        const lv_point_t points[] = {{5, 40}, {35, 45}, {30, 80}, {10, 90}, {5, 40}};

        const lv_point_t ctr = {LV_HOR_RES_MAX/2, LV_VER_RES_MAX/2};
        const lv_point_t ml = {0, LV_VER_RES_MAX/2};
        const lv_point_t mr = {LV_HOR_RES_MAX, LV_VER_RES_MAX/2};
        const lv_point_t mu = {LV_HOR_RES_MAX/2, 0};
        const lv_point_t md = {LV_HOR_RES_MAX/2, LV_VER_RES_MAX};
        
        const lv_point_t x_axis[] = {ml, mr};
        lv_canvas_draw_line(canv1, x_axis, 2, &style_line);
        
        const lv_point_t y_axis[] = {mu, md};
        lv_canvas_draw_line(canv1, y_axis, 2, &style_line);
    }

    void update_gui(
        float x, 
        float y, 
        float phi, 
        float st_x, 
        float st_y, 
        float st_phi, 
        float phi_1, 
        float phi_2,
        float x_2, 
        float y_2, 
        float phi_2_, 
        float st_x_2, 
        float st_y_2, 
        float st_phi_2, 
        float phi_1_2, 
        float phi_2_2
        ) {

        PROTECT_IN_SCOPE(gui_sem);

        draw_background();

        /*
        ////////////////////////////////// SMV 1 /////////////////////////////////////////////////
        */

        // static lv_draw_rect_dsc_t rect_dsc;
        // lv_draw_rect_dsc_init(&rect_dsc);

        static lv_draw_label_dsc_t label_dsc;
        lv_draw_label_dsc_init(&label_dsc);
        label_dsc.color = LV_COLOR_YELLOW;

        // static lv_draw_line_dsc_t style_line;
        // lv_draw_line_dsc_init(&style_line);
        // style_line.color = LV_COLOR_SILVERLV_COLOR_GREEN;

        static lv_draw_line_dsc_t style_line_st;
        lv_draw_line_dsc_init(&style_line_st);
        style_line_st.color = LV_COLOR_YELLOW;

        static char text[50] = {0};
        sprintf(text, "X: %2.2f, Y: %2.2f, Phi 1: %2.2f, Phi 2: %2.2f", x, y, phi_1, phi_2);
        lv_canvas_draw_text(canv1, 5, LV_VER_RES_MAX-20, 600, &label_dsc, text, LV_LABEL_ALIGN_LEFT);
    
        // draw_vehicle(x, y, phi, style_line);

        // convert star tracker coordinates to our coordinates
        float st_x_conv = (st_x - 909.) / 1000.;
        float st_y_conv = -1. * (st_y - 909.) / 1000.;
        float st_phi_conv = st_phi + M_PI;
        sprintf(text, "st_X: %4.2f, st_Y: %4.2f", st_x, st_y);

        lv_canvas_draw_text(canv1, 5, LV_VER_RES_MAX-40, 600, &label_dsc, text, LV_LABEL_ALIGN_LEFT);

        draw_vehicle(st_x_conv, st_y_conv, st_phi_conv, style_line_st);


        /*
        ////////////////////////////////// SMV 2 /////////////////////////////////////////////////
        */

        static lv_draw_label_dsc_t label_dsc_2;
        lv_draw_label_dsc_init(&label_dsc_2);
        label_dsc_2.color = LV_COLOR_CYAN;

        static lv_draw_line_dsc_t style_line_st_2;
        lv_draw_line_dsc_init(&style_line_st_2);
        style_line_st_2.color = LV_COLOR_CYAN;

        st_x_conv = (st_x_2 - 909.) / 1000.;
        st_y_conv = -1. * (st_y_2 - 909.) / 1000.;
        st_phi_conv = st_phi_2 + M_PI;

        sprintf(text, "X: %2.2f, Y: %2.2f, Phi 1: %2.2f, Phi 2: %2.2f", x_2, y_2, phi_1_2, phi_2_2);
        lv_canvas_draw_text(canv1, 420+5, LV_VER_RES_MAX-20, 600, &label_dsc_2, text, LV_LABEL_ALIGN_LEFT);

        sprintf(text, "st_X: %4.2f, st_Y: %4.2f", st_x_2, st_y_2);
        lv_canvas_draw_text(canv1, 420+5, LV_VER_RES_MAX-40, 600, &label_dsc_2, text, LV_LABEL_ALIGN_LEFT);

        draw_vehicle(st_x_conv, st_y_conv, st_phi_conv, style_line_st_2);

        // force update
        lv_obj_invalidate(canv1);
    }

    void draw_vehicle(float x, float y, float phi, lv_draw_line_dsc_t &style_line) {
        static vehic_point chassis_pts_tmp[4];
        static vehic_point port_0_pts_tmp[4];
        static vehic_point port_1_pts_tmp[4];
        static vehic_point port_2_pts_tmp[4];
        static vehic_point thruster_right_pts_tmp[4];
        static vehic_point thruster_left_pts_tmp[4];
        static vehic_point thruster_front_pts_tmp[4];
        static vehic_point cockpit_pts_tmp[4];

        static lv_point_t chassis[4];
        static lv_point_t port_0[4];
        static lv_point_t port_1[4];
        static lv_point_t port_2[4];
        static lv_point_t thruster_right[4];
        static lv_point_t thruster_left[4];
        static lv_point_t thruster_front[4];
        static lv_point_t cockpit[4];

        for ( int i = 0; i < 4; i++ ) {

            // compute the rotation
            chassis_pts_tmp[i] = rotate(chassis_pts[i], phi);
            port_0_pts_tmp[i] = rotate(port_0_pts[i], phi);
            port_1_pts_tmp[i] = rotate(port_1_pts[i], phi);
            port_2_pts_tmp[i] = rotate(port_2_pts[i], phi);
            thruster_right_pts_tmp[i] = rotate(thruster_right_pts[i], phi);
            thruster_left_pts_tmp[i] = rotate(thruster_left_pts[i], phi);
            thruster_front_pts_tmp[i] = rotate(thruster_front_pts[i], phi);
            cockpit_pts_tmp[i] = rotate(cockpit_pts[i], phi);

            //compute the offset
            chassis_pts_tmp[i] = offset(chassis_pts_tmp[i], x, y);
            port_0_pts_tmp[i] = offset(port_0_pts_tmp[i], x, y);
            port_1_pts_tmp[i] = offset(port_1_pts_tmp[i], x, y);
            port_2_pts_tmp[i] = offset(port_2_pts_tmp[i], x, y);
            thruster_right_pts_tmp[i] = offset(thruster_right_pts_tmp[i], x, y);
            thruster_left_pts_tmp[i] = offset(thruster_left_pts_tmp[i], x, y);
            thruster_front_pts_tmp[i] = offset(thruster_front_pts_tmp[i], x, y);
            cockpit_pts_tmp[i] = offset(cockpit_pts_tmp[i], x, y);

            // convert to pixel
            chassis[i] = xy_to_pixel(chassis_pts_tmp[i].x, chassis_pts_tmp[i].y);
            port_0[i] = xy_to_pixel(port_0_pts_tmp[i].x, port_0_pts_tmp[i].y);
            port_1[i] = xy_to_pixel(port_1_pts_tmp[i].x, port_1_pts_tmp[i].y);
            port_2[i] = xy_to_pixel(port_2_pts_tmp[i].x, port_2_pts_tmp[i].y);
            thruster_right[i] = xy_to_pixel(thruster_right_pts_tmp[i].x, thruster_right_pts_tmp[i].y);
            thruster_left[i] = xy_to_pixel(thruster_left_pts_tmp[i].x, thruster_left_pts_tmp[i].y);
            thruster_front[i] = xy_to_pixel(thruster_front_pts_tmp[i].x, thruster_front_pts_tmp[i].y);
            cockpit[i] = xy_to_pixel(cockpit_pts_tmp[i].x, cockpit_pts_tmp[i].y);
        }

        //draw
        lv_canvas_draw_line(canv1, chassis, 4, &style_line);
        lv_canvas_draw_line(canv1, port_0, 4, &style_line);
        lv_canvas_draw_line(canv1, port_1, 4, &style_line);
        lv_canvas_draw_line(canv1, port_2, 4, &style_line);
        lv_canvas_draw_line(canv1, thruster_right, 4, &style_line);
        lv_canvas_draw_line(canv1, thruster_left, 4, &style_line);
        lv_canvas_draw_line(canv1, thruster_front, 4, &style_line);
        lv_canvas_draw_line(canv1, cockpit, 4, &style_line);
    }

    static vehic_point rotate(vehic_point p, float phi) {
        float new_x = cos(phi) * p.x - sin(phi) * p.y;
        float new_y = sin(phi) * p.x + cos(phi) * p.y;
        return {new_x, new_y};
    }

    static vehic_point offset(vehic_point p, float x, float y) {
        float new_x = p.x + x;
        float new_y = p.y + y;
        return {new_x, new_y};
    }

    static lv_point_t xy_to_pixel(float x, float y) {
        x = x * pixels_per_meter + LV_HOR_RES_MAX/2;
        y = -y * pixels_per_meter + LV_VER_RES_MAX/2;
        return {(lv_coord_t) x, (lv_coord_t) y};
    }
    
    static vehic_point chassis_pts[4];
    static vehic_point port_0_pts[4];
    static vehic_point port_1_pts[4];
    static vehic_point port_2_pts[4];
    static vehic_point thruster_right_pts[4];
    static vehic_point thruster_left_pts[4];
    static vehic_point thruster_front_pts[4];
    static vehic_point cockpit_pts[4];

    static lv_obj_t *ta1;
    static lv_obj_t *canv1;
    static void *buf;
    static float pixels_per_meter;
    static double vehicle_scale;

    private:

} rv;

lv_obj_t *RobotVisualizer::ta1;
lv_obj_t *RobotVisualizer::canv1;
void *RobotVisualizer::buf;
float RobotVisualizer::pixels_per_meter = 200;
double RobotVisualizer::vehicle_scale = 0.1;

RobotVisualizer::vehic_point RobotVisualizer::chassis_pts[] = {
    {-0.433*RobotVisualizer::vehicle_scale, -0.75*RobotVisualizer::vehicle_scale}, 
    {-0.433*RobotVisualizer::vehicle_scale, 0.75*RobotVisualizer::vehicle_scale}, 
    {(1.29-0.433)*RobotVisualizer::vehicle_scale, 0*RobotVisualizer::vehicle_scale}, 
    {-0.433*RobotVisualizer::vehicle_scale, -0.75*RobotVisualizer::vehicle_scale}
    };
RobotVisualizer::vehic_point RobotVisualizer::port_0_pts[] = {
    {-0.433*RobotVisualizer::vehicle_scale, 0*RobotVisualizer::vehicle_scale}, 
    {(-0.433-0.26)*RobotVisualizer::vehicle_scale, 0.15*RobotVisualizer::vehicle_scale}, 
    {(-0.433-0.26)*RobotVisualizer::vehicle_scale, -0.15*RobotVisualizer::vehicle_scale}, 
    {-0.433*RobotVisualizer::vehicle_scale, 0*RobotVisualizer::vehicle_scale}
    };
RobotVisualizer::vehic_point RobotVisualizer::port_1_pts[] = {
    {-0.433*RobotVisualizer::vehicle_scale, 0*RobotVisualizer::vehicle_scale}, 
    {(-0.433-0.26)*RobotVisualizer::vehicle_scale, 0.15*RobotVisualizer::vehicle_scale}, 
    {(-0.433-0.26)*RobotVisualizer::vehicle_scale, -0.15*RobotVisualizer::vehicle_scale}, 
    {-0.433*RobotVisualizer::vehicle_scale, 0*RobotVisualizer::vehicle_scale}
    };
RobotVisualizer::vehic_point RobotVisualizer::port_2_pts[] = {
    {-0.433*RobotVisualizer::vehicle_scale, 0*RobotVisualizer::vehicle_scale}, 
    {(-0.433-0.26)*RobotVisualizer::vehicle_scale, 0.15*RobotVisualizer::vehicle_scale}, 
    {(-0.433-0.26)*RobotVisualizer::vehicle_scale, -0.15*RobotVisualizer::vehicle_scale}, 
    {-0.433*RobotVisualizer::vehicle_scale, 0*RobotVisualizer::vehicle_scale}
    };
RobotVisualizer::vehic_point RobotVisualizer::thruster_right_pts[] = {
    {-0.433*RobotVisualizer::vehicle_scale, -0.75*RobotVisualizer::vehicle_scale}, 
    {(-0.433-0.3)*RobotVisualizer::vehicle_scale, -0.75*RobotVisualizer::vehicle_scale}, 
    {(-0.433-0.3)*RobotVisualizer::vehicle_scale, -0.45*RobotVisualizer::vehicle_scale}, 
    {-0.433*RobotVisualizer::vehicle_scale, -0.45*RobotVisualizer::vehicle_scale}
    };
RobotVisualizer::vehic_point RobotVisualizer::thruster_left_pts[] = {
    {-0.433*RobotVisualizer::vehicle_scale, 0.75*RobotVisualizer::vehicle_scale}, 
    {(-0.433-0.3)*RobotVisualizer::vehicle_scale, 0.75*RobotVisualizer::vehicle_scale}, 
    {(-0.433-0.3)*RobotVisualizer::vehicle_scale, 0.45*RobotVisualizer::vehicle_scale}, 
    {-0.433*RobotVisualizer::vehicle_scale, 0.45*RobotVisualizer::vehicle_scale}
    };
RobotVisualizer::vehic_point RobotVisualizer::thruster_front_pts[] = {
    {(1.2-0.433)*RobotVisualizer::vehicle_scale, 0.15*RobotVisualizer::vehicle_scale}, 
    {(1.5-0.433)*RobotVisualizer::vehicle_scale, 0.15*RobotVisualizer::vehicle_scale}, 
    {(1.5-0.433)*RobotVisualizer::vehicle_scale, -0.15*RobotVisualizer::vehicle_scale}, 
    {(1.2-0.433)*RobotVisualizer::vehicle_scale, -0.15*RobotVisualizer::vehicle_scale}
    };
RobotVisualizer::vehic_point RobotVisualizer::cockpit_pts[] = {
    {-0.433*RobotVisualizer::vehicle_scale, 0.25*RobotVisualizer::vehicle_scale}, 
    {0*RobotVisualizer::vehicle_scale, 0.25*RobotVisualizer::vehicle_scale}, 
    {0*RobotVisualizer::vehicle_scale, -0.25*RobotVisualizer::vehicle_scale}, 
    {-0.433*RobotVisualizer::vehicle_scale, -0.25*RobotVisualizer::vehicle_scale}
    };
