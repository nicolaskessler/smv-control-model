#include "rodos.h"

#include "velocity_controller_topics.hpp"

Topic<desired_track> desired_track_topic(1488, "Velocity Controller Desired Track Topic");
