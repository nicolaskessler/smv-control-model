#include "rodos.h"
#include "gateway.h"

// Provides a link to a second UDP Broadcastnetwork on a different port.

static UDPInOut udp(-50002);  //A different broadcast network
static LinkinterfaceUDP linkinterfaceUDP(&udp);
static Gateway gateway(&linkinterfaceUDP, true);
