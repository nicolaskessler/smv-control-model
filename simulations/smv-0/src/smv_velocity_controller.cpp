/**
 * 
 * Controls SMV v_x, v_y and v_theta
 * We use a linearized model around 0
 * -> we assume coriolis forces to be 0
 * Thrusters do always point to COM
 * thus v_theta is a SISO plant
 * And we can control the velocity by calculating the
 * desired forces in v_x and v_y direction. And then
 * calculating the required thruster forces.
 * Later we can include a feedback linarization for the coriolis forces
 *
 * The controller takes the following setpoint:
 * float desired_velocity_forward [m/s]
 * float desired_velocity_port [m/s]
 * float desired_velocity_theta [m/s]
 * 
 * The controller looks at the following state variables:
 * float current_velocity_forward [m/s]
 * float current_velocity_port [m/s]
 * float current_velocity_theta [m/s]
 * 
 * It outputs sControl on its topic
 * 
 * It works asynchronously using commbuffers with a sampling rate
 * of 20 Hz
 * Currently, it combines 3 PI controllers
*/

#include "rodos.h"
#include "matlib.h"

#include "velocity_controller_topics.hpp"
#include "topics.h"

static Application a("Velocity Control and Experiment Node");

static CommBuffer<sPositionData> st_pose_cb;
static Subscriber st_posecbs(PositionDataTopic, st_pose_cb, "Star_Tracker_Pose_Subs");

static CommBuffer<desired_track> track_cb;
static Subscriber trackcbs(desired_track_topic, track_cb, "Desired_Track_Subs");

static CommBuffer<sSensorData> sensor_data_cb;
static Subscriber sensor_datacbs(SensorDataTopic, sensor_data_cb, "SensorData_Track_Subs");

Semaphore smv_velocity_controller_sem;

template <typename T> int32_t sgn(T val) {
    return (T(0) < val) - (val < T(0));
}

class LevantObserver {

    private:

    float h;
    float alpha;
    float lambda;
    float u;
    float x;

    public:

    LevantObserver(float h, float alpha, float lambda, float x_0 = 0, float u_0=0) :
    h(h),
    alpha(alpha),
    lambda(lambda),
    x(x_0),
    u(u_0)
    {
    }

    float update(float y) {
        const float e = y-x;
        const float sign = sgn(e);
        const float du = + alpha * sign;
        u += h * du;
        const float dx = u + lambda * LevantObserver::sqrt(abs(e)) * sign;
        x += h * dx;
        return dx;
    }

    float sqrt(float a) {
        float b = 1.f;
        float c = a/b;
        for (int i = 0; i < 20; i++){
            b = (c+b) / 2.f;
            c = a / b;
        }
        return c;
    }
};

class VelocityController : public Thread {

    public:

    int64_t first_loop;
    int64_t observe_only = 20*SECONDS;

    desired_track initial_track;

    int32_t missed_read_count;
    int32_t max_missed_read_count = 5;

    float j_smv = 0.099;

    float vel_kp = 300.;
    float vel_ki = 0.0 / 20.;
    float vel_x_i = 0.;
    float vel_y_i = 0.;

    float theta_kp = 100.0;
    float theta_ki = 1.;
    float theta_beta = 0.;
    float theta_i = 0.;
    float cur_rpm = 0.;

    LevantObserver obs_x;
    LevantObserver obs_y;
    LevantObserver obs_theta;

    sPositionData last_position;
    float theta_correction = 0;

    sControl stop_cmd;

    VelocityController() : 
    Thread("VelocityController", 20), missed_read_count(max_missed_read_count),
    obs_x(0.05f, 0.5f, 1.3f),
    obs_y(0.05f, 0.5f, 1.3f),
    obs_theta(0.05f, 0.3f, 0.5f)
    {

        initial_track.vel_x = 0.;
        initial_track.vel_y = 0.;
        initial_track.vel_theta = 0.;

        stop_cmd.th_time[0] = 0;
        stop_cmd.th_time[1] = 0;
        stop_cmd.th_time[2] = 0;
        stop_cmd.thruster_theta[0] = 0;
        stop_cmd.thruster_theta [1]= 0;
        stop_cmd.rpm = 0;
        stop_cmd.rw_time = 50;
        stop_cmd.dock = 0;
        stop_cmd.ambient1 = 0;
        stop_cmd.ambient2 = 0;
        stop_cmd.range1 = 0;
        stop_cmd.range2 = 0;

    }

    void init() {}

    void run() {

        PRINTF("SMV velocity control initializing...\n");

        AT(NOW() + 1 * SECONDS);

        desired_track t;
        sPositionData s;
        sSensorData d;

        // first setpoint is hold
        t = initial_track;

        // this also makes sure, we have data in last_position
        st_pose_cb.get(last_position);
        first_loop = NOW();

        PRINTF("SMV velocity control running now...\n");

        TIME_LOOP(0, 50*MILLISECONDS) {

            // block while controller is disabled
            PRINTF("enteirng sem\n");
            smv_velocity_controller_sem.enter();
            PRINTF("entered sem\n");

            if (track_cb.getOnlyIfNewData(t)) {
                // CONTROL HERE
                missed_read_count = 0;
                st_pose_cb.get(s);
                sensor_data_cb.get(d);

                sControl c = control_value(s, t, d);
                last_position = s;

                if(NOW() < first_loop + observe_only) {
                    ControlTopic.publish(stop_cmd);
                    theta_beta = 0.;
                    theta_i = 0.;
                }
                else {
                    ControlTopic.publish(c);
                }
            }
            else {
                missed_read_count++;
                if (max_missed_read_count <= missed_read_count) {
                    // EMERGENCY STOP HERE
                    ControlTopic.publish(stop_cmd);
                }
            }

            // remember to release the semaphore
            smv_velocity_controller_sem.leave();

            yield();
        }

    }

    sControl control_value(sPositionData s, desired_track t, sSensorData d) {

        // naive differentiator implementation
        // better than nothing
        
        // float vel_X = (s.PosX - last_position.PosX) * 20 / 1000;
        float vel_Y = -(s.PosY - last_position.PosY) * 20 / 1000;
        // float vel_theta = (s.theta - last_position.theta) * 20;

        float check_jump = (s.theta - last_position.theta);
        if (check_jump > 5.) {
            theta_correction -= 2.*3.14159265;
        }
        if (check_jump < -5.) {
            theta_correction += 2.*3.14159265;
        }

        float vel_X = obs_x.update(s.PosX / 1000.);
        //float vel_Y = obs_y.update(s.PosY / 1000.);
        float vel_theta = obs_theta.update(s.theta + theta_correction);
        PRINTF("vel_X: %2.4f\n", vel_X);
        PRINTF("vel_Y: %2.4f\n", vel_Y);
        PRINTF("Theta: %2.4f\n", s.theta + theta_correction);
        PRINTF("vel_Theta: %2.4f\n", vel_theta);

        // rotate the velocity to vehicle frame
        float vel_x = vel_X * cos(s.theta + M_PI) + vel_Y * sin(s.theta + M_PI);
        float vel_y = - vel_X * sin(s.theta + M_PI) + vel_Y * cos(s.theta + M_PI);

        // compute the desired forces

        float dx = t.vel_x - vel_x;
        vel_x_i += 0.05 * dx;
        float f_x = vel_kp * dx + vel_ki * vel_x_i;
        PRINTF("vel_x_ref: %2.4f, vel_x: %2.4f\n", t.vel_x, vel_x);
        PRINTF("f_x: %2.4f\n", f_x);

        float dy = t.vel_y - vel_y;
        vel_y_i += 0.05 * dy;
        float f_y = vel_kp * dy + vel_ki * vel_y_i;
        PRINTF("vel_y_ref: %2.4f, vel_y: %2.4f\n", t.vel_y, vel_y);
        PRINTF("f_y: %2.4f\n", f_y);

        float dtheta = t.vel_theta - vel_theta;
        theta_beta += 0.05 * dtheta * dtheta / j_smv;
        theta_i += 0.05 * dtheta / j_smv;
        float f_theta = theta_kp * dtheta + theta_beta * dtheta + theta_ki * theta_i;
        PRINTF("vel_t_ref: %2.4f, vel_t: %2.4f\n", t.vel_theta, vel_theta);
        // feed-forward the velocity of the RW from the principle of conservation of angular momentum
        //f_theta += t.vel_theta * 1000;
        f_theta *= 1;

        // convert the forces to thruster PWM

        float t0 = 0; // front
        float t1 = 0; // port
        float t2 = 0; // starboard

        // thruster force in x direction

        if (f_x >= 0) {
            // two thrusters
            // they are off by 60 degrees cos(60d) = 1/2
            t1 += f_x;
            t2 += f_x;
        }
        else {
            t0 += -1. * f_x;
        }

        // thruster force in port direction

        if (f_y >= 0 ) {
            // force in port direction -> activate starboard thruster
            // thrusters are off by 30 degrees sin(30d) = cos(30d) = sqrt(3)/2
            t2 += f_y * 2. / 1.73205;
        }
        else {
            t1 += -1. * f_y * 2. / 1.73205;
        }

        // cancel out the f_y forward thrust with t0:
        // cos(60d) = 1/2
        t0 += abs(f_y) * 0.5;

        // reaction wheel: undo the underlying P-controller
        float rw_kp = 1.;
        float rpm_set = f_theta * 3. / rw_kp + d.motorSpeed;

        // clamp the values
        int t0i = (int) t0;
        t0i = clampi(t0i, 0, 50);
        int t1i = (int) t1;
        t1i = clampi(t1i, 0, 50);
        int t2i = (int) t2;
        t2i = clampi(t2i, 0, 50);

        int rpm = (int) rpm_set;
        rpm = clampi(rpm, -11000, 11000);

        // construct the data structure
        sControl c;
        c.th_time[0] = t0i;
        c.th_time[1] = t1i;
        c.th_time[2] = t2i;
        c.thruster_theta[0] = 0;
        c.thruster_theta[1] = 0;
        c.rpm = rpm;
        c.rw_time = 50;
        c.dock = 0;
        c.ambient1 = 0;
        c.ambient2 = 0;
        c.range1 = 0;
        c.range2 = 0;
        
        return c;
    }

    int clampi(int x, int min, int max) {

      if (x >= max) {
        return max;
      }
      else if (x <= min) {
        return min;
      }

      return x;
    }

} vc;
