#include "position_controller_topics.hpp"

Topic<docking_command> DockingCommandTopic1(1778,"Start Docking Maneuver SMV 1");
Topic<docking_command> DockingCommandTopic2(1779,"Start Docking Maneuver SMV 2");
