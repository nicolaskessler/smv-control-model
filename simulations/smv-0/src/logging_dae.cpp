#include "rodos.h"
#include <iostream>
#include <fstream>
#include <iomanip>
#include <ctime>

#define LOGGING_BUFFER_SIZE 8192

class SMVCSVGenerator {

    public:

    struct Item {

        uint32_t iteration;
        int64_t time_stamp;
        double qx_1;
        double qy_1;
        double qtheta_1;
        double qx_2;
        double qy_2;
        double qtheta_2;
        double vx_1;
        double vy_1;
        double vtheta_1;
        double vx_2;
        double vy_2;
        double vtheta_2;

    };

    const char * generate_header() const noexcept;

    const char * generate_line(const Item item) const noexcept;

    const char * autolog() noexcept;

    private:

    char buffer[LOGGING_BUFFER_SIZE];

    const uint32_t buffer_size = LOGGING_BUFFER_SIZE;

    uint32_t iteration = 0;

};

struct state_dae {

    double qx_1;
    double qy_1;
    double qtheta_1;
    double qx_2;
    double qy_2;
    double qtheta_2;
    double vx_1;
    double vy_1;
    double vtheta_1;
    double vx_2;
    double vy_2;
    double vtheta_2;

};

CommBuffer<state_dae> x_cb;
Topic<state_dae> StateDaeTopic(1520,"DAE State Topic");
static Subscriber StateDaeSubs(StateDaeTopic, x_cb);

const char * SMVCSVGenerator::generate_header() const noexcept {

    sprintf(const_cast<char*>(buffer),
        "iteration,"
        "time_stamp,"
        "qx_1,"
        "qy_1,"
        "qtheta_1,"
        "qx_2,"
        "qy_2,"
        "qtheta_2,"
        "vx_1,"
        "vy_1,"
        "vtheta_1,"
        "vx_2,"
        "vy_2,"
        "vtheta_2\n"
        );

    return buffer;
}

const char * SMVCSVGenerator::generate_line(Item item) const noexcept {

    sprintf(const_cast<char*>(buffer),
    "%d,"
    "%lld,"
    "%lf,"
    "%lf,"
    "%lf,"
    "%lf,"
    "%lf,"
    "%lf,"
    "%lf,"
    "%lf,"
    "%lf,"
    "%lf,"
    "%lf,"
    "%lf\n",
    item.iteration,
    item.time_stamp,
    item.qx_1,
    item.qy_1,
    item.qtheta_1,
    item.qx_2,
    item.qy_2,
    item.qtheta_2,
    item.vx_1,
    item.vy_1,
    item.vtheta_1,
    item.vx_2,
    item.vy_2,
    item.vtheta_2
    );

    return buffer;
}

const char * SMVCSVGenerator::autolog() noexcept {

    state_dae state;

    x_cb.get(state);

    Item item {
        .iteration = iteration,
        .time_stamp = NOW(),
        .qx_1 = state.qx_1,
        .qy_1 = state.qy_1,
        .qtheta_1 = state.qtheta_1,
        .qx_2 = state.qx_2,
        .qy_2 = state.qy_2,
        .qtheta_2 = state.qtheta_2,
        .vx_1 = state.vx_1,
        .vy_1 = state.vy_1,
        .vtheta_1 = state.vtheta_1,
        .vx_2 = state.vx_2,
        .vy_2 = state.vy_2,
        .vtheta_2 = state.vtheta_2
    };

    iteration++;

    return generate_line(item);
}

Fifo<std::string, 1024> line_fifo;

class LoggerOnLinux : public StaticThread<8192> {

    private:

    SMVCSVGenerator generator;

    public:

    LoggerOnLinux() : StaticThread<8192>("Logger On Linux Thread", 50), generator() {

    }

    void init() {

        // send header to FIFO
        line_fifo.put(std::string(generator.generate_header()));
    }

    void run() {

        // get next log line
        // send line to FIFO
        TIME_LOOP(NOW(), 50 * MILLISECONDS) {
            PRINTF("Sending log line... at %2.4f\n", SECONDS_NOW());
            if ( !line_fifo.put(std::string(generator.autolog())) ) {
                PRINTF("LOG FIFO OVERFLOWED!\n");
            }
            PRINTF("Sent log line...... at %2.4f\n", SECONDS_NOW());
        }
    }
} lol;

class WriterOnLinux : public StaticThread<8192> {

    public:

    std::ofstream logfile;

    WriterOnLinux() : StaticThread<8192>("Writer On Linux Thread", 1), logfile() {

    }

    void init() {

        // get date
        char buffer[80];
        auto t = std::time(nullptr);
        auto tm = std::localtime(&t);
        strftime(buffer, sizeof(buffer), "%d-%m-%Y-%H:%M:%S", tm);
        std::string filename = std::string("/tmp/smv-dae-log-") + std::string(buffer) + ".csv";

        // create 
        std::ios_base::sync_with_stdio(false);
        const size_t bufsize = 256*1024;
        char buf[bufsize];
        logfile.rdbuf()->pubsetbuf(buf, bufsize);
        PRINTF("Log file: %s\n", filename);
        logfile.open (filename, std::ios::out);
    }

    void run() {

        while (true) {
            // get all lines from FIFO
            std::string line;
            int64_t t = NOW();
            while(line_fifo.get(line)) {
                // write them
                PRINTF("Writing...\n");
                PRINTF("Writing............ at %2.4f\n", SECONDS_NOW());
                logfile << line;
                logfile.flush();
                PRINTF("Finished writing... at %2.4f\n", SECONDS_NOW());
            }
            AT(t+5*MILLISECONDS);
        }

    }
} wol; // TODO Logger is not interruptible
