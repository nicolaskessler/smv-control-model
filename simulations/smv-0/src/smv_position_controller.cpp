#include "rodos.h"
#include "matlib.h"

#include "topics.h"

#include "smv_feedforward.hpp"
#include "position_controller_topics.hpp"

static Application a("Velocity Control and Experiment Node");

static CommBuffer<sPositionData> st_pose_cb1;
static Subscriber st_posecbs(PositionDataTopic, st_pose_cb1, "Star_Tracker_Pose_Subs");

static CommBuffer<sSensorData> sensor_data_cb1;
static Subscriber sensor_datacbs(SensorDataTopic, sensor_data_cb1, "SensorData_Track_Subs");

static CommBuffer<docking_command> dc_cb1;
static Subscriber cd_cbs(DockingCommandTopic1, dc_cb1, "Docking Command Subscriber 1");

static CommBuffer<sPositionData> st_pose_cb2;
static Subscriber st_posecbs2(PositionDataTopic2, st_pose_cb2, "Star_Tracker_Pose_Subs 2");

static CommBuffer<sSensorData> sensor_data_cb2;
static Subscriber sensor_datacbs2(SensorDataTopic2, sensor_data_cb2, "SensorData_Track_Subs 2");

static CommBuffer<docking_command> dc_cb2;
static Subscriber cd_cbs2(DockingCommandTopic2, dc_cb2, "Docking Command Subscriber 2");

#define DIVISIONS 750

class PositionController : public StaticThread<1600000> {

    private:

    CommBuffer<sPositionData> &st_pose_cb;
    CommBuffer<sSensorData> &sensor_data_cb;
    CommBuffer<docking_command> &dc_cb;

    Topic<sControl> &ControlTopic;

    float source_x;
    float target_x;
    float source_y;
    float target_y;
    float source_t;
    float target_t;
    float t_float;
    float vel_dock;

    const float t_end = 15;
    const float h = 0.02;
    const int divisions = t_end / h;

    float last_x;
    float last_y;
    float last_t;

    enum class PositionControllerState { idle, st_synchronize, computing, halt, dock, hold};

    PositionControllerState state; 

    public:

    PositionController(
        CommBuffer<sPositionData> &st_pose_cb_ref, 
        CommBuffer<sSensorData> &sensor_data_cb_ref, 
        CommBuffer<docking_command> &dc_cb_ref,
        Topic<sControl> &ControlTopicRef) : 
    StaticThread<1600000>("PositionController", 20),
    st_pose_cb(st_pose_cb_ref),
    sensor_data_cb(sensor_data_cb_ref),
    dc_cb(dc_cb_ref),
    ControlTopic(ControlTopicRef),
    state(PositionControllerState::idle)
    {}

    void init() {

    }

    void run() {

        // PRINTF("Waiting 9 Seconds ...\n");
        // AT(NOW() + 9*SECONDS);

        //move(1., 0, 6.2, 0.0, 0, 0);
        //move(0., 0.5, 6.2, 0.0, 0, 0);
        //move(0., 0, 4.711, 0.0, 0, 0);
        //move(1., 0, 4.711, 0.0, 0, 0);
        // move(0.8, 0.5, 4.711, 0.0, 0, 0);

        // AT(NOW() + 2*SECONDS);

        TIME_LOOP(0, 50*MILLISECONDS) {

            // evaluate new messages, if there are any

            docking_command dc;
            bool newData = dc_cb.getOnlyIfNewData(dc);

            if (newData) {
                
                // process dc

                if(state == PositionControllerState::idle || state == PositionControllerState::halt) {

                    move(
                        dc.x,
                        dc.y,
                        dc.theta,
                        dc.velocity,
                        dc.t_float,
                        dc.t_end
                    );

                }

            }

            if (state == PositionControllerState::st_synchronize) {

                st_synchronize();

                // float
                AT(NOW() + t_float * SECONDS);

                // done
                begin_halt();
            }

            if (state == PositionControllerState::idle) {
            }

            if (state == PositionControllerState::hold) {

                hold();
            }
        }
    }

    void move(float x, float y, float theta, float vel_dock_, float t_float_, float t_end_) {

        if (state == PositionControllerState::computing || state == PositionControllerState::st_synchronize) {
            PRINTF("Warning: Moving already in progress\n");
            return;
        }

        sPositionData s;
        st_pose_cb.get(s);

        // convert position to generalized coordinates
        star_tracker_to_generalized(s.PosX, s.PosY, s.theta, source_x, source_y, source_t);
        target_x = x;
        target_y = y;
        target_t = theta;
        vel_dock = vel_dock_;
        t_float = t_float_;
        // t_end = t_end_;

        PRINTF("Starting Move from:\n");
        PRINTF("x: %2.4f\n, y: %2.4f\n, theta: %2.4f\n", source_x, source_y, source_t);
        PRINTF("To:\n");
        PRINTF("x: %2.4f\n, y: %2.4f\n, theta: %2.4f\n", x, y, theta);
        PRINTF("In time: %2.4f s with final docking velocity: %2.4f\n", t_end, vel_dock);

        state = PositionControllerState::st_synchronize;

    }

    private:

    void begin_halt() {

        state = PositionControllerState::hold;

        sPositionData s;
        st_pose_cb.get(s);
        float x;
        float y;
        float theta;

        // convert position to generalized coordinates
        star_tracker_to_generalized(s.PosX, s.PosY, s.theta, x, y, theta);

        target_x = x;
        target_y = y;
        target_t = theta;

    }

    void hold() {
        Matrix<750, 9> data;
        data.set(0, 0, target_x);
        data.set(0, 1, target_y);
        data.set(0, 2, target_t);
        data.set(0, 3, 0);
        data.set(0, 4, 0);
        data.set(0, 5, 0);
        data.set(0, 6, 0);
        data.set(0, 7, 0);
        data.set(0, 8, 0);
        control_move(data, 0);
    }

    void st_synchronize() {

        state = PositionControllerState::computing;

        sPositionData s;
        st_pose_cb.get(s);
        float x;
        float y;
        float theta;

        // convert position to generalized coordinates
        star_tracker_to_generalized(s.PosX, s.PosY, s.theta, x, y, theta);

        last_x = x;
        last_y = y;
        last_t = theta;

        PRINTF("Computing Trajectory for %d datapoints...\n", DIVISIONS);

        Matrix<DIVISIONS, 9> data = get_feedforward_data2<DIVISIONS>(
            source_x, 
            target_x, 
            source_y, 
            target_y, 
            source_t, 
            target_t, 
            vel_dock, 
            t_end);

        Matrix<1, 9> start = data.get_row(0);
        Matrix<1, 9> end = data.get_row(DIVISIONS-1);
        PRINTF("Start:\n");
        start.print();
        PRINTF("end:\n");
        end.print();

        // supervise the movement
        int64_t begin = NOW();
        TIME_LOOP(NOW(), 50*MILLISECONDS) {

            int iteration = (NOW() - begin) / (50 * MILLISECONDS);

            // 100 DIVISIONS
            if (iteration >= DIVISIONS) {
                break;
            }

            control_move(data, iteration);
        }

        PRINTF("Done.\n");

    }

    void control_move(Matrix<DIVISIONS, 9> &data, int iteration) {

        PRINTF("Iteration %d\n", iteration);

        float f_x = 0;
        float f_y = 0;
        float t = 0.0;

        // worked for move(1., 0, 6.2, 0.0, 0);
        const float kp_x = 10 * 10;
        const float kd_x = 100 / 20.;

        const float kp_y = kp_x;
        const float kd_y = kd_x;

        const float kp_t = 20.;
        const float kd_t = 10. / 20.;

        // const float kp_x = 40;
        // const float kd_x = 2;

        // const float kp_y = kp_x;
        // const float kd_y = kd_x;

        // const float kp_t = 30;
        // const float kd_t = 0.1;

        // get the new position

        sPositionData s;
        st_pose_cb.get(s);
        float x;
        float y;
        float theta;

        // convert position to generalized coordinates
        star_tracker_to_generalized(s.PosX, s.PosY, s.theta, x, y, theta);

        // do PD control for now

        /////////
        // worked well with kp_x = 10 * 10 kd_x = 0.02
        float dpos_x = data.get(iteration, 0) - x;
        float vel_x = 20 * (x - last_x);
        float dvel_x = data.get(iteration, 6) - vel_x;
        PRINTF("X: %2.4f Last X: %2.4f\n", x, last_x);
        PRINTF("X: %2.4f, Expected: %2.4f\n", x, data.get(iteration, 0));
        PRINTF("vX: %2.4f, Expected: %2.4f\n", vel_x, data.get(iteration, 6));
        PRINTF("Difference PosX: %2.4f\n", dpos_x);
        PRINTF("Difference VelX: %2.4f\n", dvel_x);
        // f_x += kp_x * (data.get(iteration, 0) - x);
        f_x += kp_x * dpos_x + kd_x * dvel_x;
        // f_x += beta_x * sliding_line(dpos_x, dvel_x, alpha_x);
        PRINTF("extra force x: %2.4f\n", f_x);
        ////////

        /////////
        // worked well with kp_x = 10 * 10 kd_x = 0.02
        float dpos_y = data.get(iteration, 1) - y;
        float vel_y = 20 * (y - last_y);
        float dvel_y = data.get(iteration, 7) - vel_y;
        PRINTF("Y: %2.4f Last Y: %2.4f\n", y, last_y);
        PRINTF("Y: %2.4f, Expected: %2.4f\n", y, data.get(iteration, 1));
        PRINTF("vY: %2.4f, Expected: %2.4f\n", vel_y, data.get(iteration, 7));
        PRINTF("Difference PosY: %2.4f\n", dpos_y);
        PRINTF("Difference VelY: %2.4f\n", dvel_y);
        // f_x += kp_x * (data.get(iteration, 0) - x);
        f_y += kp_y * dpos_y + kd_y * dvel_y;
        // f_x += beta_x * sliding_line(dpos_x, dvel_x, alpha_x);
        PRINTF("extra force y: %2.4f\n", f_y);
        ////////

        const float f_X = f_x;
        const float f_Y = f_y;

        // transform forces to rotated coordinate frame
        f_x =   f_X * cos(theta) + f_Y * sin(theta);
        f_y = - f_X * sin(theta) + f_Y * cos(theta);

        ////////
        // worked well with kp_t = 4 * 6 kd_t = 
        float dpos_t = angle_diff(data.get(iteration, 2), theta);
        float vel_t = 20. * (theta - last_t);
        float dvel_t = data.get(iteration, 8) - vel_t;
        PRINTF("Theta: %2.4f, Expected: %2.4f\n", theta, data.get(iteration, 2));
        PRINTF("Difference: %2.4f\n",  dpos_t);
        t += kp_t * dpos_t + kd_t * dvel_t;
        PRINTF("extra torque: %2.4f\n", t);
        ////////

        // apply the input
        f_x += 1.0 * data.get(iteration, 3);
        f_y += 1.0 * data.get(iteration, 4);
        // 1.84375
        t += 3 * data.get(iteration, 5);

        sControl command = forcesToCommand(f_x, f_y, t);
        ControlTopic.publish(command);

        // handle states

        last_x = x;
        last_y = y;
        last_t = theta;
    }

    sControl forcesToCommand(float f_x, float f_y, float t) {

        sSensorData d;
        sensor_data_cb.get(d);

        // convert the forces to thruster PWM

        float t0 = 0; // front
        float t1 = 0; // port
        float t2 = 0; // starboard

        // thruster force in x direction
        int alpha = 7;
        float angle = M_PI * (60 - alpha) / 180.f;
        float c = cos(angle);
        float s = sin(angle);

        if (f_x >= 0) {
            // two thrusters
            // they are off by 60 degrees cos(60d) = 1/2
            t1 += 0.5 * f_x * c;
            t2 += 0.5 * f_x * c;
        }
        else {
            t0 += -1. * f_x;
        }

        // thruster force in port direction

        if (f_y >= 0 ) {
            // force in port direction -> activate starboard thruster
            // thrusters are off by 30 degrees sin(60d) = cos(30d) = sqrt(3)/2
            t2 += f_y / s;
        }
        else {
            t1 += -1. * f_y / s;
        }

        // cancel out the f_y forward thrust with t0:
        // cos(60d) = 1/2
        t0 += abs(f_y) * c;

        // reaction wheel: undo the underlying P-controller
        float rw_kp = 1.;
        float rpm_set = (t * 100.f / 0.029f) / rw_kp + d.motorSpeed;

        // smalles thruster unit:
        const float t_min = 7.359 / 50.f;
        t0 /= t_min;
        t1 /= t_min;
        t2 /= t_min;

        // clamp the values
        int t0i = (int) t0;
        t0i = clampi(t0i, 0, 50);
        int t1i = (int) t1;
        t1i = clampi(t1i, 0, 50);
        int t2i = (int) t2;
        t2i = clampi(t2i, 0, 50);

        int rpm = (int) rpm_set;
        rpm = clampi(rpm, -11000, 11000);

        // PRINTF("f_x: %2.4f\n", f_x);
        // PRINTF("t0: %d\n", t0i);

        // construct the data structure
        sControl cd;
        cd.th_time[0] = t0i;
        cd.th_time[1] = t1i;
        cd.th_time[2] = t2i;
        cd.thruster_theta[0] = alpha;
        cd.thruster_theta[1] = alpha;
        cd.rpm = rpm;
        cd.rw_time = 50;
        cd.dock = 0;
        cd.ambient1 = 0;
        cd.ambient2 = 0;
        cd.range1 = 0;
        cd.range2 = 0;

        return cd;
    }

    int clampi(int x, int min, int max) {

      if (x >= max) {
        return max;
      }
      else if (x <= min) {
        return min;
      }

      return x;
    }

    float sliding_line(float dpos, float dvel, float alpha) {
        if (-alpha * dpos > dvel) {
            return -1.f;
        }
        else {
            return 1.f;
        }
    }

    void star_tracker_to_generalized(float x_st, float y_st, float theta_st, float &x, float &y, float &theta) {
        x = (x_st - 909.f) / 1000.f;
        y = -(y_st - 909.f) / 1000.f;
        theta = theta_st + M_PI;
    }

    float angle_diff(float x, float y) {
        return atan2(sin(x-y), cos(x-y));
    }

};

PositionController pc1(st_pose_cb1, sensor_data_cb1, dc_cb1, ControlTopic);
PositionController pc2(st_pose_cb2, sensor_data_cb2, dc_cb2, ControlTopic2);

// Test

class CommandTest : public StaticThread<> {

    void run() {

        docking_command dc1 {
            .x = 0.1,
            .y = 0.2,
            .theta = 3.141-0.0,
            .velocity = 0.0,
            .t_float = 0.,
            .t_end = 0.,
            .dock = false
        };

        docking_command dc2 {
            .x = 0.24,
            .y = 0.2,
            .theta = 6.25-0.0,
            .velocity = 0.0,
            .t_float = 0.,
            .t_end = 0.,
            .dock = false
        };

        AT(NOW() + 9*SECONDS);

        DockingCommandTopic1.publish(dc1);
        DockingCommandTopic2.publish(dc2);
    }

} ct;
