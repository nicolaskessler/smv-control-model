#include "rodos.h"

#include "velocity_controller_topics.hpp"
#include "topics.h"

extern Semaphore smv_velocity_controller_sem;

static Application a("Automatic Experiment Pilot");

class AutoExperiment : public StaticThread<> {

    public:

    AutoExperiment() : StaticThread("Autoamtic Experiment Pilot Thread", 20) {}

    void init() {}

    void run() {

        // PRINTF("Generating PRBS9\n");
        // uint32_t a = 0x02;
        // uint8_t vals[4599];
        // for (int i = 0; i < 511; i++){
        //     int newbit = (((a >> 8) ^ (a >> 4)) & 1);
        //     a = ((a << 1) | newbit) & 0x1ff;
        //     for (int j = 0; j < 8; j++) {
        //         vals[i*9+j] = ((a & (1 << j)) > 0);
        //     }
        // }

        PRINTF("Automatic Experiment Starting\n");

        // wait for SMV to stabilize for a minute
        // don't publish anything, the controller will automatically halt the SMV then
        PRINTF("Wait for SMV to stabilize for a minute\n");
        int32_t cycles = 1200;
        for (int i = 0; i < cycles; i++) {
            desired_track t;
            t.vel_x = 0.;
            t.vel_y = 0.;
            t.vel_theta = 0.;
            desired_track_topic.publish(t);
            AT(NOW() + 50 * MILLISECONDS);
        }

        // thrusters forwards for 1.5 seconds
        PRINTF("Thrusters forwards for 1.5 seconds\n");
        smv_velocity_controller_sem.enter();
        AT(NOW() + 50 * MILLISECONDS);
        cycles = 30;
        for (int i = 0; i < cycles; i++) {

            // prepare s_control structure
            sControl c;
            c.th_time[0] = 0;
            c.th_time[1] = 20;
            c.th_time[2] = 20;
            c.thruster_theta[0] = 0;
            c.thruster_theta[1] = 0;
            c.rpm = 0;
            c.rw_time = 50;
            c.dock = 0;
            c.ambient1 = 0;
            c.ambient2 = 0;
            c.range1 = 0;
            c.range2 = 0;

            // send
            ControlTopic.publish(c);

            //wait
            AT(NOW() + 50 * MILLISECONDS);
        }
        // break
        for (int i = 0; i < cycles; i++) {

            // prepare s_control structure
            sControl c;
            c.th_time[0] = 20;
            c.th_time[1] = 0;
            c.th_time[2] = 0;
            c.thruster_theta[0] = 0;
            c.thruster_theta[1] = 0;
            c.rpm = 0;
            c.rw_time = 50;
            c.dock = 0;
            c.ambient1 = 0;
            c.ambient2 = 0;
            c.range1 = 0;
            c.range2 = 0;

            // send
            ControlTopic.publish(c);

            //wait
            AT(NOW() + 50 * MILLISECONDS);
        }
        smv_velocity_controller_sem.leave();

        // stop for a minute
        // PRINTF("Stop for a minute\n");
        // cycles = 1200;
        // for (int i = 0; i < cycles; i++) {
        //     desired_track t;
        //     t.vel_x = 0.;
        //     t.vel_y = 0.;
        //     t.vel_theta = 0.;
        //     desired_track_topic.publish(t);
        //     AT(NOW() + 50 * MILLISECONDS);
        // }

        // // PRBS for 30 seconds
        // PRINTF("PRBS for 3 seconds\n");
        // smv_velocity_controller_sem.enter();
        // cycles = 60;
        // int time_scale = 10;
        // for (int i = 0; i < cycles / 5; i++) {

        //     // prepare s_control structure
        //     sControl c;

        //     if (vals[i]) {
        //         c.th_time[0] = 50;
        //         c.th_time[1] = 0;
        //         c.th_time[2] = 0;
        //     }
        //     else {
        //         c.th_time[0] = 0;
        //         c.th_time[1] = 50;
        //         c.th_time[2] = 50;
        //     }

        //     c.thruster_theta[0] = 0;
        //     c.thruster_theta[1] = 0;
        //     c.rpm = 0;
        //     c.rw_time = 50;
        //     c.dock = 0;
        //     c.ambient1 = 0;
        //     c.ambient2 = 0;
        //     c.range1 = 0;
        //     c.range2 = 0;

        //     // send
        //     ControlTopic.publish(c);

        //     //wait
        //     AT(NOW() + 50 * time_scale * MILLISECONDS);
        // }
        // smv_velocity_controller_sem.leave();

        // stop for a minute
        PRINTF("Stop for 1 minute\n");
        cycles = 1200;
        for (int i = 0; i < cycles; i++) {
            desired_track t;
            t.vel_x = 0.;
            t.vel_y = 0.;
            t.vel_theta = 0.;
            desired_track_topic.publish(t);
            AT(NOW() + 50 * MILLISECONDS);
        }

        // only one thruster forwards for 1 seconds
        PRINTF("Only one thruster forwards for 2 seconds directed to front 2x\n");
        smv_velocity_controller_sem.enter();
        AT(NOW() + 50 * MILLISECONDS);
        cycles = 40;
        // turn thruster
        for (int i = 0; i < cycles; i++) {

            // prepare s_control structure
            sControl c;
            c.th_time[0] = 0;
            c.th_time[1] = 0;
            c.th_time[2] = 0;
            c.thruster_theta[0] = 60;
            c.thruster_theta[1] = 60;
            c.rpm = 0;
            c.rw_time = 50;
            c.dock = 0;
            c.ambient1 = 0;
            c.ambient2 = 0;
            c.range1 = 0;
            c.range2 = 0;

            // send
            ControlTopic.publish(c);

            //wait
            AT(NOW() + 50 * MILLISECONDS);
        }
        // execute
        for (int i = 0; i < cycles; i++) {

            // prepare s_control structure
            sControl c;
            c.th_time[0] = 10;
            c.th_time[1] = 0;
            c.th_time[2] = 10;
            c.thruster_theta[0] = 60;
            c.thruster_theta[1] = 60;
            c.rpm = 0;
            c.rw_time = 0;
            c.dock = 0;
            c.ambient1 = 0;
            c.ambient2 = 0;
            c.range1 = 0;
            c.range2 = 0;

            // send
            ControlTopic.publish(c);

            //wait
            AT(NOW() + 50 * MILLISECONDS);
        }
        for (int i = 0; i < cycles; i++) {

            // prepare s_control structure
            sControl c;
            c.th_time[0] = 10;
            c.th_time[1] = 10;
            c.th_time[2] = 0;
            c.thruster_theta[0] = 60;
            c.thruster_theta[1] = 60;
            c.rpm = 0;
            c.rw_time = 0;
            c.dock = 0;
            c.ambient1 = 0;
            c.ambient2 = 0;
            c.range1 = 0;
            c.range2 = 0;

            // send
            ControlTopic.publish(c);

            //wait
            AT(NOW() + 50 * MILLISECONDS);
        }
        // turn thruster back
        for (int i = 0; i < cycles; i++) {

            // prepare s_control structure
            sControl c;
            c.th_time[0] = 0;
            c.th_time[1] = 0;
            c.th_time[2] = 0;
            c.thruster_theta[0] = 0;
            c.thruster_theta[1] = 0;
            c.rpm = 0;
            c.rw_time = 50;
            c.dock = 0;
            c.ambient1 = 0;
            c.ambient2 = 0;
            c.range1 = 0;
            c.range2 = 0;

            // send
            ControlTopic.publish(c);

            //wait
            AT(NOW() + 50 * MILLISECONDS);
        }
        smv_velocity_controller_sem.leave();

        // stop for a minute
        PRINTF("Stop for a minute\n");
        cycles = 1200;
        for (int i = 0; i < cycles; i++) {
            desired_track t;
            t.vel_x = 0.;
            t.vel_y = 0.;
            t.vel_theta = 0.;
            desired_track_topic.publish(t);
            AT(NOW() + 50 * MILLISECONDS);
        }

        // turn roughly 180 degrees for 30 seconds
        // roughly 0.10472 radians per second (pi/30)
        PRINTF("Turn for 30 seconds\n");
        cycles = 600;
        // for (int i = 0; i < cycles; i++) {
        //     desired_track t;
        //     t.vel_x = 0.;
        //     t.vel_y = 0.;
        //     t.vel_theta = 0.10472*3;
        //     desired_track_topic.publish(t);
        //     AT(NOW() + 50 * MILLISECONDS);
        // }
        smv_velocity_controller_sem.enter();
        for (int i = 0; i < cycles; i++) {

            // prepare s_control structure
            sControl c;
            c.th_time[0] = 0;
            c.th_time[1] = 0;
            c.th_time[2] = 0;
            c.thruster_theta[0] = 0;
            c.thruster_theta[1] = 0;
            c.rpm = 8000;
            c.rw_time = 50;
            c.dock = 0;
            c.ambient1 = 0;
            c.ambient2 = 0;
            c.range1 = 0;
            c.range2 = 0;

            // send
            ControlTopic.publish(c);

            //wait
            AT(NOW() + 50 * MILLISECONDS);
        }
        smv_velocity_controller_sem.leave();

        // stop forever
        PRINTF("Finished\n");
        cycles = INT32_MAX;
        for (int i = 0; i < cycles; i++) {
            desired_track t;
            t.vel_x = 0.;
            t.vel_y = 0.;
            t.vel_theta = 0.;
            desired_track_topic.publish(t);
            AT(NOW() + 50 * MILLISECONDS);
        }
    }

} ae;
