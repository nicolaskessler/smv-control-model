#include "rodos.h"
#include "hw_specific.h"
#include <math.h>
#include <limits.h>

#include "tutorial_twist_command.hpp"
#include "tutorial_pose.hpp"
#include "topics.h"

#include "rw_velocity_control.hpp"

extern "C" {
    #include "system_model_dae.h"
}

static Application a("SMVSimulation");

static CommBuffer<sControl> command_cb;
static Subscriber command_sub(ControlTopic, command_cb, "sControl Command Subscriber");

struct state_dae {

    double qx_1;
    double qy_1;
    double qtheta_1;
    double qx_2;
    double qy_2;
    double qtheta_2;
    double vx_1;
    double vy_1;
    double vtheta_1;
    double vx_2;
    double vy_2;
    double vtheta_2;

};

CommBuffer<state_dae> x_cb;
Topic<state_dae> StateDaeTopic(1520,"DAE State Topic");
static Subscriber StateDaeSubs(StateDaeTopic, x_cb);

struct joystickData {
  float right_stick_x;
  float right_stick_y;
  float left_stick_x;
  float left_stick_y;
  bool  button_left_pressed;
  bool  button_right_pressed;
};

extern CommBuffer<joystickData> latestJSData;

class ControlCommanderRendezvous : public Thread {

    public:
    ControlCommanderRendezvous(): Thread(name="Control Commander Rendezvous", 20), theta(0) {}

    void init() {

      // DO NOT wait for SMV to come online
    }

    void run() {
      PRINTF("I am a rendezvouz commander and I am having a great day!\n");
      joystickData d;

      TIME_LOOP(0, 10*MILLISECONDS) {
        // use joystick value from buffer
        latestJSData.get(d);
        publishControl(d);
      }
    }

    void publishControl(joystickData d) {

      sControl c;
      
      // get thruster time
      // x stick is inverted: down is positive

      if (d.left_stick_x >= 0.f) {
        c.th_time[0] = +1.f * d.left_stick_x * 50.f / ( (double)SHRT_MAX );
        c.th_time[1] = 0.f;
        c.th_time[2] = 0.f;
      }
      else {
        c.th_time[0] = 0.f;
        c.th_time[1] = -1.f * d.left_stick_x * 50.f / ( (double)SHRT_MAX );
        c.th_time[2] = -1.f * d.left_stick_x * 50.f / ( (double)SHRT_MAX );
      }

      // evaluate d.right_stick_y
      if (d.right_stick_y >= 0) {
        c.th_time[2] += +1. * d.right_stick_y * 50.f / ( (double) SHRT_MAX );
      }
      else {
        c.th_time[1] += -1. * d.right_stick_y * 50.f / ( (double) SHRT_MAX );
      }

      // clamp
      c.th_time[1] = clampi(c.th_time[1], 0, 50);
      c.th_time[2] = clampi(c.th_time[2], 0, 50);

      // compute new theta
      int dtheta = 1 * d.button_right_pressed - 1 * d.button_left_pressed;
      theta = clampi(theta + dtheta, -30, 60);

      c.thruster_theta[0] = theta;
      c.thruster_theta[1] = theta;

      // reaction wheel
      // y stick is inverted
      c.rpm = -1.f * 100.f * d.left_stick_y / ( (double)SHRT_MAX );
      c.rw_time = 50;

      c.dock = 0;
      c.ambient1 = 0;
      c.ambient2 = 0;
      c.range1 = 0;
      c.range2 = 0;

      PRINTF("th_0: %d, rpm: %d\n", c.th_time[0], c.rpm);

      ControlTopic.publish(c);

    }

    int clampi(int x, int min, int max) {

      if (x >= max) {
        return max;
      }
      else if (x <= min) {
        return min;
      }

      return x;
    }

    private:

    int theta;

} cc;

class RobotSimulation : public Thread {

    public:
    RobotSimulation(): Thread(name="Robot Simulation", 10), x{0.12,0,0,-0.12,0,M_PI,0,0,0,0,0,0} {}

    void run() {
        PRINTF("Hello World! "
        "I am a robot simulator and I am having a great day!\n");

        int64_t i = 0;
        int64_t iteration_begin = NOW();
        int64_t nextbeat = iteration_begin;
        sControl c;

        while (1) {

            i++;
            iteration_begin = NOW();

            command_cb.get(c);

            for(int i = 0; i < n; i++) {
                old_x[i] = x[i];
            }
            sControl_to_forces(c, f);
            system_model_dae(f, old_x, x);

            // publish the executed motor thrust
            publish_executed_u(u);
            // finally publish the resulting IMU message
            publish_state(x);
            nextbeat = iteration_begin + 50*MILLISECONDS;

            // print the new state
            PRINTF("Iteration %lld\n", i);
            PRINTF("Started: %lld, Now: %lld\n", iteration_begin, NOW());
            PRINTF("Next beat %lld\n", nextbeat);
            PRINTF("Inputs: f_x: %2.2f f_y: %2.2f, T: %2.2f\n", f[0], f[1], f[2]);
            PRINTF("Observed state: X1: %2.2f Y1: %2.2f, t1: %2.2f\n", x[0], x[1], x[2]);
            PRINTF("Observed state: X2: %2.2f Y2: %2.2f, t2: %2.2f\n", x[3], x[4], x[5]);

            // time delay
            if (nextbeat < iteration_begin + 10*MILLISECONDS) {
                PRINTF("%s is starving!\n", this->name);
            }
            AT(nextbeat);
        }
    }

    int n = 12;
    double old_x[12];
    double x[12];
    double u[6];
    double f[6];

    void sControl_to_forces(sControl c, double f[6]) {
        double torque_max = 0.02943;
        double force_max = 7.5 * 0.1; // I need 0.1 to drive it manually
        double fx = 0.;
        double fy = 0.;
        double t = torque_max * c.rw_time / 50.f * c.rpm / 100.f;

        fx += -1.f * force_max * (c.th_time[0] / 50.f);
        
        double alpha_0 = c.thruster_theta[0] / 360.f - 60.;
        double alpha_1 = c.thruster_theta[1] / 360.f + 60.;
        alpha_0 *= M_PI / 180.;
        alpha_1 *= M_PI / 180.;
        fx += +1.f * force_max * cos(alpha_0) * (c.th_time[1] / 50.f);
        fx += +1.f * force_max * cos(alpha_1) * (c.th_time[2] / 50.f);
        fy += -1.f * force_max * sin(alpha_0) * (c.th_time[1] / 50.f);
        fy += -1.f * force_max * sin(alpha_1) * (c.th_time[2] / 50.f);

        f[0] = fx;
        f[1] = fy;
        f[2] = t;
        f[3] = 0.f;
        f[4] = 0.f;
        f[5] = 0.f;
    }

    void publish_state(double x[12]) {
        pose p;

        point pt{0};
        pt.x = x[0];
        pt.y = x[1];
        p.position = pt;

        // set the orientation
        quaternion o(cos(x[2]/2), 0, 0, sin(x[2]/2));
        p.orientation = o;

        tutorial_pose.publish(p);

        // publish RW speed as sSensorData in SensorDataTopic
        sSensorData s;
        s.scale = 0;
        s.Vbat = 0;
        s.motorSpeed = 0;
        PRINTF("NOT IMPLEMENTED Motorspeed: %2.2f\n", s.motorSpeed);
        SensorDataTopic.publish(s);

        state_dae state {
          .qx_1 = x[0],
          .qy_1 = x[1],
          .qtheta_1 = x[2],
          .qx_2 = x[3],
          .qy_2 = x[4],
          .qtheta_2 = x[5],
          .vx_1 = x[6],
          .vy_1 = x[7],
          .vtheta_1 = x[8],
          .vx_2 = x[9],
          .vy_2 = x[10],
          .vtheta_2 = x[11]
        };
        StateDaeTopic.publish(state);
    }

    void publish_executed_u(double u[3]) {
        twist t = {
            .vel_fb = u[0],
            .vel_lr = u[1],
            .anv_z = u[2]
        };
        tutorial_twist_command_executed.publish(t);
    }
    
} ss;
