#include "rodos.h"
#include <iostream>
#include <fstream>
#include <iomanip>
#include <ctime>

#include "logging.hpp"

Fifo<std::string, 1024> line_fifo;

class LoggerOnLinux : public StaticThread<8192> {

    private:

    SMVCSVGenerator generator;

    public:

    LoggerOnLinux() : StaticThread<8192>("Logger On Linux Thread", 50), generator() {

    }

    void init() {

        // send header to FIFO
        line_fifo.put(std::string(generator.generate_header()));
    }

    void run() {

        // get next log line
        // send line to FIFO
        TIME_LOOP(NOW(), 50 * MILLISECONDS) {
            PRINTF("Sending log line... at %2.4f\n", SECONDS_NOW());
            if ( !line_fifo.put(std::string(generator.autolog())) ) {
                PRINTF("LOG FIFO OVERFLOWED!\n");
            }
            PRINTF("Sent log line...... at %2.4f\n", SECONDS_NOW());
        }
    }
} lol;

class WriterOnLinux : public StaticThread<8192> {

    public:

    std::ofstream logfile;

    WriterOnLinux() : StaticThread<8192>("Writer On Linux Thread", 1), logfile() {

    }

    void init() {

        // get date
        char buffer[80];
        auto t = std::time(nullptr);
        auto tm = std::localtime(&t);
        strftime(buffer, sizeof(buffer), "%d-%m-%Y-%H:%M:%S", tm);
        std::string filename = std::string("/tmp/smv-log-") + std::string(buffer) + ".csv";

        // create 
        std::ios_base::sync_with_stdio(false);
        const size_t bufsize = 256*1024;
        char buf[bufsize];
        logfile.rdbuf()->pubsetbuf(buf, bufsize);
        PRINTF("Log file: %s\n", filename);
        logfile.open (filename, std::ios::out);
    }

    void run() {

        while (true) {
            // get all lines from FIFO
            std::string line;
            int64_t t = NOW();
            while(line_fifo.get(line)) {
                // write them
                PRINTF("Writing...\n");
                PRINTF("Writing............ at %2.4f\n", SECONDS_NOW());
                logfile << line;
                logfile.flush();
                PRINTF("Finished writing... at %2.4f\n", SECONDS_NOW());
            }
            AT(t+5*MILLISECONDS);
        }

    }
} wol; // TODO Logger is not interruptible
