#include "rodos.h"

#include "smv_feedforward.hpp"

class Test : public StaticThread<1600000> {

  public:
  Test(): StaticThread<1600000>(name="Test", 20) {}

  void init() {

  }

  void run() {
    PRINTF("I am a test and I am having a great day!\n");

    Matrix<2, 2> i;
    i.set(0, 0, 1);
    i.set(0, 1, 0);
    i.set(1, 0, 0);
    i.set(1, 1, 1);

    Matrix<2, 2> m;
    m.set(0, 0, 1);
    m.set(0, 1, 2);
    m.set(1, 0, 0);
    m.set(1, 1, 1);

    Matrix<2, 1> v;
    v.set(0, 0, 3);
    v.set(1, 0, 4);

    Matrix<2, 1> x = i.multiply(m.multiply(v));

    PRINTF("Resulting vector:\n");
    for (int i = 0; i < 2; i++) {
      PRINTF("%d: %2.4f\n", i, x.get(i,0));
    }

    // matrix multiplication with n uneq m

    Matrix<1, 2> w;
    w.set(0, 0, 1);
    w.set(0, 1, 2);

    PRINTF("Outer product of v and w:\n");
    Matrix<2, 2> outer = v.multiply(w);
    for (int i = 0; i< 2; i++) {
      for (int j = 0; j < 2; j++) {
        PRINTF("%2.4f ", outer.get(i, j));
      }
      PRINTF("\n");
    }

    PRINTF("Inner product of v and w:\n");
    Matrix<1, 1> inner = w.multiply(v);
    PRINTF("%2.4f\n", inner.get(0, 0));

    PRINTF("copying\n");
    Matrix<2, 2> o;
    o.copy_from(m);
    for (int i = 0; i< 2; i++) {
      for (int j = 0; j < 2; j++) {
        PRINTF("%2.4f ", o.get(i, j));
      }
      PRINTF("\n");
    }

    PRINTF("Testing L-R decomposition\n");
    Matrix<4, 4> lr;
    Matrix<4, 4> l;
    Matrix<4, 4> r;
    lr.set(0, 0, 2);
    lr.set(0, 1, -2);
    lr.set(0, 2, 1);
    lr.set(0, 3, -2);
    lr.set(1, 0, 1);
    lr.set(1, 1, 2);
    lr.set(1, 2, 0);
    lr.set(1, 3, 2);
    lr.set(2, 0, -6);
    lr.set(2, 1, 6);
    lr.set(2, 2, 1);
    lr.set(2, 3, 8);
    lr.set(3, 0, 7);
    lr.set(3, 1, -10);
    lr.set(3, 2, 10);
    lr.set(3, 3, -5);
    lr.decomposition_l_r(l, r);
    PRINTF("L\n");
    l.print();
    PRINTF("R\n");
    r.print();
    PRINTF("L@R\n");
    Matrix<4, 4> A = l.multiply(r);
    A.print();

    PRINTF("Testing solve\n");
    Matrix<4, 1> b;
    b.set(0, 0, 3);
    b.set(1, 0, -21);
    b.set(2, 0, -1);
    b.set(3, 0, 41);
    PRINTF("b\n");
    b.print();
    Matrix<4, 1> sol = A.solve(b);
    PRINTF("x\n");
    sol.print();
    
    PRINTF("Done.\n");

    PRINTF("Testing feed forward control\n");
    const int n = 120 * 50;
    float t = 120.f;
    Matrix<8, 3> cv = constraintvectors(0, 4.0, 0, 0.3, 0, -3.141, 0.01);
    PRINTF("constraint vectors:\n");
    cv.print();
    Matrix<8, 8> psi = generateDataMatrix(t);
    PRINTF("Datamatrix:\n");
    psi.print();
    Matrix<8, 1> cv_x = cv.get_col(0);
    Matrix<n, 1> pos = get_feedforward_position<n>(cv_x, t);
    PRINTF("Start X: %2.4f\n", pos.get(0, 0));
    PRINTF("End X: %2.4f\n", pos.get(n-1, 0));
    Matrix<n, 1> acc = get_feedforward_acceleration<n>(cv_x, t);
    PRINTF("Start acc-X: %2.4f\n", acc.get(0, 0));
    PRINTF("End acc-X: %2.4f\n", acc.get(n-1, 0));

    PRINTF("Testing full stack feed forward control");
    Matrix<n, 6> data = get_feedforward_data<n>(0, 4.0, 0, 0.3, 0, -3.141, 0.01, t);
    Matrix<1, 6> start = data.get_row(0);
    Matrix<1, 6> end = data.get_row(n-1);
    PRINTF("Start:\n");
    start.print();
    PRINTF("end:\n");
    end.print();

    TIME_LOOP(0, 10*MILLISECONDS) {
      PRINTF(".");
    }
  }

} teststuff;
