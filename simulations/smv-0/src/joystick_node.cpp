#include "rodos.h"
#include <math.h>
#include <limits.h>

#include "tutorial_twist_command.hpp"
#include "velocity_controller_topics.hpp"
#include "joystick.hh"
#include "topics.h"

static Application a("JoystickCommander");

struct joystickData {
  float right_stick_x;
  float right_stick_y;
  float left_stick_x;
  float left_stick_y;
  bool  button_left_pressed;
  bool  button_right_pressed;
};

CommBuffer<joystickData> latestJSData;

class JoystickToSetpoint : public Thread {

  int32_t theta;

  public:
  JoystickToSetpoint(): Thread(name="JoysticktoSetpoint", 20), theta(0) {}

  void init() {

  }

  void run() {
    PRINTF("I am a setpoint generator and I am having a great day!\n");
    joystickData d;

    TIME_LOOP(0, 10*MILLISECONDS) {
      // use joystick value from buffer
      latestJSData.get(d);
      publishSetpoint(d);
    }
  }

  void publishSetpoint(joystickData d) {
    desired_track t;
    t.vel_x = -d.left_stick_x / ( (double)SHRT_MAX ) / 10.;
    t.vel_y = -d.left_stick_y / ( (double)SHRT_MAX ) / 10.;
    t.vel_theta = - d.right_stick_y / ( (double)SHRT_MAX ) / 1.0;

    desired_track_topic.publish(t);
  }

} jts;

// decision for CommBuffer sync is local to this file
class ControlCommanderRendezvous : public Thread {

    public:
    ControlCommanderRendezvous(): Thread(name="Control Commander Rendezvous", 20), theta(0) {}

    void init() {

      // DO NOT wait for SMV to come online
    }

    void run() {
      PRINTF("I am a rendezvouz commander and I am having a great day!\n");
      joystickData d;

      TIME_LOOP(0, 10*MILLISECONDS) {
        // use joystick value from buffer
        latestJSData.get(d);
        publishControl(d);
      }
    }

    void publishControl(joystickData d) {

      sControl c;
      
      // get thruster time
      // x stick is inverted: down is positive

      if (d.left_stick_x >= 0.f) {
        c.th_time[0] = +1.f * d.left_stick_x * 50.f / ( (double)SHRT_MAX );
        c.th_time[1] = 0.f;
        c.th_time[2] = 0.f;
      }
      else {
        c.th_time[0] = 0.f;
        c.th_time[1] = -1.f * d.left_stick_x * 50.f / ( (double)SHRT_MAX );
        c.th_time[2] = -1.f * d.left_stick_x * 50.f / ( (double)SHRT_MAX );
      }

      // evaluate d.right_stick_y
      if (d.right_stick_y >= 0) {
        c.th_time[2] += +1. * d.right_stick_y * 50.f / ( (double) SHRT_MAX );
      }
      else {
        c.th_time[1] += -1. * d.right_stick_y * 50.f / ( (double) SHRT_MAX );
      }

      // clamp
      c.th_time[1] = clampi(c.th_time[1], 0, 50);
      c.th_time[2] = clampi(c.th_time[2], 0, 50);

      // compute new theta
      int dtheta = 1 * d.button_right_pressed - 1 * d.button_left_pressed;
      theta = clampi(theta + dtheta, -30, 60);

      c.thruster_theta[0] = theta;
      c.thruster_theta[1] = theta;

      // reaction wheel
      // y stick is inverted
      c.rpm = -1.f * 100.f * d.left_stick_y / ( (double)SHRT_MAX );
      c.rw_time = 50;

      c.dock = 0;
      c.ambient1 = 0;
      c.ambient2 = 0;
      c.range1 = 0;
      c.range2 = 0;

      // PRINTF("th_0: %d, rpm: %d\n", c.th_time[0], c.rpm);

      ControlTopic.publish(c);

    }

    int clampi(int x, int min, int max) {

      if (x >= max) {
        return max;
      }
      else if (x <= min) {
        return min;
      }

      return x;
    }

    private:

    int theta;

};

class JSNode : public Thread {

private:
    Joystick joystick;
public:
    JSNode():
    Thread("JSNode", 1),
    joystick("/dev/input/js0", false)
    {
    }

  void init() {
    if (!joystick.isFound())
    {
      printf("open failed.\n");
    }
  }
    
  void run() {

      PRINTF("JSNode starting\n");

      bool active = false;
      bool left_button = false;
      bool right_button = false;
      int16_t val_fb = 0;
      int16_t val_lr = 0;
      int16_t val_z = 0;
      twist t;
      joystickData d;
      
      while(1) {

        JoystickEvent event;
        //PRINTF("NO Joystick Event\n");
        if (joystick.sample(&event))
        {
            // deadman's switch
            if (event.isButton() && event.number == 0)
            {
              //printf("Button %u is %d\n", event.number, event.value != 0);
              active = event.value != 0;
            }
            // right stick l/r
            if (event.isAxis() && event.number == 3)
            {
              //printf("Axis %u is at position %d\n", event.number, event.value);
              val_lr = event.value;
            }
            // left stick f/b
            else if (event.isAxis() && event.number == 1)
            {
              //printf("Axis %u is at position %d\n", event.number, event.value);
              val_fb = event.value;
            }
            // left stick l/r
            else if (event.isAxis() && event.number == 0)
            {
              //printf("Axis %u is at position %d\n", event.number, event.value);
              val_z = event.value;
            }
            else if (event.isButton() && event.number == 15)
            {
              // left button
              left_button = event.value != 0;
            }
            else if (event.isButton() && event.number == 16)
            {
              // right button
              right_button = event.value != 0;
            }
            d = DataFromRaw(active, val_fb, val_z, 0, val_lr, left_button, right_button);
            latestJSData.put(d);
        }
        yield();
      }
  }

  joystickData DataFromRaw(bool active, float lx, float ly, float rx, float ry, bool lb, bool rb) {
    joystickData d{0};
    if(active) {
      d.left_stick_x = lx;
      d.left_stick_y = ly;
      d.right_stick_x = rx;
      d.right_stick_y = ry;
      d.button_left_pressed = lb;
      d.button_right_pressed = rb;
    }
    return d;
  }
    
} jsnode;
