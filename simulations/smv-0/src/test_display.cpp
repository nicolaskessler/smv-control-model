extern "C" {
#include <SDL2/SDL.h>
#include "lvgl/lvgl.h"
#include "lvgl_init.h"
#include "lv_examples/lv_examples.h"
}

#include "rodos.h"
#include <math.h>

#include "beacon.hpp"
#include "ping_responder.hpp"

static Application a("TestDisplay");

class TestDisplay: public Thread {
    
    public:

    TestDisplay(): Thread(name="Test Display", 10, 1000000) {}

    void init() {

        /*Initialize LittlevGL*/
        lv_init();

        /*Initialize the HAL (display, input devices, tick) for LittlevGL*/
        hal_init();
    }

    void run() {

        PRINTF("Test Display starting...\n");

        /*Create a demo*/
        lv_demo_widgets();

        while (1) {
            AT(NOW() + 5*MILLISECONDS);
            PRINTF("running..\n");
            lv_task_handler();
            lv_tick_inc(5);
        }
    }
    private:

} td;
