#include "SITLInterface.hpp"

#include "rodos.h"
#include <math.h>

#include "tutorial_pose.hpp"
#include "topics.h"

static Application a("StarTracker");

static CommBuffer<pose> pose_cb1;
static Subscriber pose_sub1(tutorial_pose, pose_cb1, "Pose Subscriber 1");

static CommBuffer<pose> pose_cb2;
static Subscriber pose_sub2(tutorial_pose2, pose_cb2, "Pose Subscriber 2");

class StarTracker : public StaticThread<8192000> {

    private:

    CommBuffer<pose> &pose_cb;
    Topic<sPositionData> &PositionDataTopic;

    public:

    sPositionData old_p;
    StarTrackerSITL starTracker;

    // sampling rate
    double h = 0.05;
    float old_x = 0;
    float old_y = 0;
    float old_theta = 0;

    // conversion dynamics are assumed to be fast -> passthrough
    StarTracker(CommBuffer<pose> &pose_cb_ref, Topic<sPositionData> &PositionDataTopicRef)
    :StaticThread<8192000>("StarTracker", 10),
    pose_cb(pose_cb_ref),
    PositionDataTopic(PositionDataTopicRef)
    {
        old_p.PosX = 0.f;
        old_p.PosY = 0.f;
        old_p.theta = 0.f;

        // load the file in constructor
        // this is ok in linux, because all HW is fully initialized as well as iostream
        // HARDCODED
        // starTracker.init("SMDMiniStarsFinal.bmp");
        starTracker.init("noise_map.npy");
    }

    void run() {

        pose p;

        while(true) {

            if (pose_cb.getOnlyIfNewData(p)) {
                
                sPositionData new_p = sPositionDataFromPose(p, old_p);
                PositionDataTopic.publish(new_p);
                old_p = new_p;
            }
            yield();
        }
        
    }

    sPositionData sPositionDataFromPose(pose current, sPositionData old) {

        PRIORITY_CEILER_IN_SCOPE();

        // run the Star-Tracker SITL
        // here's the arrangement of the coordinate system:
        // star tracker begins in x=94mm, y=94mm in the top left corner
        // it ranges to x=1912mm, y=1912mm
        // that is a width and heigth of 1818mm
        // we put the center of the world frame into the center of the star map
        // thus our origin lies in x=909mm, y=909mm in the star map
        // also the star map swaps the direction of the y axis
        // and the direction of the rotation axis
        // both thetas point along x axis if 0
        double st_x = current.position.x * 1000. + 909.;
        double st_y = -current.position.y * 1000. + 909.;
        PRINTF("T: %2.4f\n", angle_from_quaternion(current.orientation));
        double st_theta = angle_from_quaternion(current.orientation) - M_PI / 2.;

        st_x = clamp(st_x, 94., 1912.);
        st_y = clamp(st_y, 94., 1912.);

        PRINTF("Original coordinates:\n");
        PRINTF("X: %2.4f\n", st_x);
        PRINTF("Y: %2.4f\n", st_y);
        PRINTF("T: %2.4f\n", st_theta);

        // run the star tracker SITL
        double x_;
        double y_;
        double theta_;

        starTracker.run(x_, y_, theta_, st_x, st_y, st_theta);

        PRINTF("Star-Tra coordinates:\n");
        PRINTF("X: %2.4f\n", x_);
        PRINTF("Y: %2.4f\n", y_);
        PRINTF("T: %2.4f\n", theta_);

        double vel_x = (x_ - old_x) / h;
        double vel_y = (y_ - old_y) / h;
        double vel_theta = (theta_ - old_theta) / h;

        old_x = x_;
        old_y = y_;
        old_theta = theta_;

        // note: sPositionData uses mm, we use m.
        // note: sPositionData uses angles, we use quaternions
        // TODO: implement proper derivative
        return sPositionData{
            .PosX = x_,
            .PosY = y_,
            .theta = theta_,
            .Velx = vel_x,
            .Vely = vel_y,
            .Veltheta = vel_theta
        };
    }

    double angle_from_quaternion(quaternion q) {
        return 2 * atan2(q.k, q.r);
    }

    double clamp(double x, double min, double max) {
        if (x >= max) {
            return max;
        }
        if (x < min) {
            return min;
        }
        return x;
    }

};

StarTracker st1(pose_cb1, PositionDataTopic);
StarTracker st2(pose_cb2, PositionDataTopic2);
