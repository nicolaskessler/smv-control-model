#include "rodos.h"

struct sPID_Data
{
	 float Kp, Ki, Kd;

	 float P, I, D;

	 float e, e_1, Upid, Upid_1, Usat, Usat_1, Kr;

	 float Umax, Umin, T;

	 bool AntiWindup;
};

extern uint32_t ModesPeriod; // Modes period in ms

float get_current_PWM(int n);
