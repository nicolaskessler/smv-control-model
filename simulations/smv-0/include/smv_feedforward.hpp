#include "rodos.h"

#include <cmath>

// broken on 32 bit port linux-makecontext

template <int32_t n, int32_t m>
class Matrix {

    private:

    const int32_t nrows = n;
    const int32_t ncols = m;
    double buf[n][m];

    public:

    void set(int32_t i, int32_t j, double x) {
        buf[i][j] = x;
    }

    double get(int32_t i, int32_t j) const {
        return buf[i][j];
    }

    int32_t rows() const{
        return n;
    }

    int32_t cols() const {
        return m;
    }

    Matrix<n, 1> get_col(int c) const {
        Matrix<n, 1> res;
        for (int i = 0; i< n; i++) {
            res.set(i, 0, this->get(i, c));
        }
        return res;
    }

    Matrix<1, m> get_row(int c) const {
        Matrix<1, m> res;
        for (int i = 0; i< m; i++) {
            res.set(0, i, this->get(c, i));
        }
        return res;
    }

    void copy_from(const Matrix<n, m> src) {
        
        for (int i = 0; i < n; i++) {

            for (int j = 0; j < m; j++) {

                this->set(i, j, src.get(i, j));
            }
        }
    }

    template <int32_t o, int32_t p>
    Matrix<n, p> multiply(const Matrix<o, p> &b) {

        Matrix<n, p> res;
        // this->print();
        // b.print();

        for (int i = 0; i < n; i++) {

            // ROWS

            for (int j = 0; j < p; j++) {

                // COLUMNS
                
                // PRINTF("New loop over k for i: %d, j:%d\n", i , j);
                res.set(i, j, 0);

                for (int k = 0; k < m; k++) {
                    double gotten = res.get(i, j);
                    double added = this->get(i, k) * b.get(k, j);
                    // PRINTF("From value of k: %2.4f\n", gotten);
                    // PRINTF("And addition of: %2.4f\n", added);
                    double sum = gotten + added;
                    // PRINTF("Giving: %2.4f\n", sum);
                    res.set(i, j, gotten + added);
                    // PRINTF("i: %d, k: %d a: %2.4f\n", i, k, this->get(i, k));
                    // PRINTF("k: %d, j: %d b: %2.4f\n", k, j, b.get(k, j));
                    // PRINTF("k: %d res_i_j: %2.4f\n", k, res.get(i, j));
                }

                // PRINTF("res_i_j: %2.4f\n", res.get(i, j));

            }
            // PRINTF("%d\n",i);
            // res.print();

        }

        return res;
    }

    void print() const {
        for (int i = 0; i< n; i++) {
            for (int j = 0; j < m; j++) {
                PRINTF("%2.4f ", this->get(i, j));
            }
            PRINTF("\n");
        }
    }


    template<int k>
    Matrix<k, 1> solve(Matrix<k, 1> &b) {

        Matrix<n, n> l;
        Matrix<n, n> r;
        this->decomposition_l_r(l, r);

        Matrix<k, 1> y;

        for (int i = 0; i < k; i++) {
            
            double y_i = b.get(i, 0);

            for (int j = 0; j < i; j++) {

                y_i -= l.get(i, j) * y.get(j, 0);
                
            }

            y.set(i, 0, y_i);
        }

        Matrix<k, 1> x;
        
        for (int i = k - 1; i >= 0; i--) {

            double x_i = y.get(i, 0);

            for (int j = k - 1; j > i; j--) {

                x_i -= r.get(i, j) * x.get(j, 0);
            }

            x_i /= r.get(i, i);

            x.set(i, 0, x_i);
        }

        return x;

    }

    // either RREF or matrix inverse or decomposition
    void decomposition_l_r(Matrix<n, n> &l, Matrix<n, n> &r) {

        // I did not have time, so I assumed pivot elements are already on diagonal

        // copy this to r
        r.copy_from(*this);
        
        for (int i = 0; i < n; i++) {

            double pivot = r.get(i, i);

            // go through each remaining row (down)

            for (int j = i + 1; j < n; j++) {

                double multiplier = r.get(j, i) / pivot;

                // for each colum subtract the pivot row multiplied with multiplier from each row j in R

                for (int k = i; k < n; k++) {

                    double new_r_j_k = r.get(j, k) - multiplier * r.get(i, k);
                    r.set(j, k, new_r_j_k);
                }

                // remember the multiplier? it goes into L.

                l.set(j, i, multiplier);

            }
        }

        // fill in the ones in l:

        for (int i = 0; i < n; i++) {

            l.set(i,i, 1.0f);

        }
    }

};

// double pow(double t, int n) {
//     double r = 1;
//     for(int i = 0; i < n; i++){
//         r *= t;
//     }
//     return r;
// }

Matrix<8, 8> generateDataMatrix(double t) {

    Matrix<8, 8> psi;

    psi.set(0, 0, 1);
    psi.set(0, 1, 0);
    psi.set(0, 2, 0);
    psi.set(0, 3, 0);
    psi.set(0, 4, 0);
    psi.set(0, 5, 0);
    psi.set(0, 6, 0);
    psi.set(0, 7, 0);

    psi.set(1, 0, 0);
    psi.set(1, 1, 1);
    psi.set(1, 2, 0);
    psi.set(1, 3, 0);
    psi.set(1, 4, 0);
    psi.set(1, 5, 0);
    psi.set(1, 6, 0);
    psi.set(1, 7, 0);

    psi.set(2, 0, 0);
    psi.set(2, 1, 0);
    psi.set(2, 2, 2);
    psi.set(2, 3, 0);
    psi.set(2, 4, 0);
    psi.set(2, 5, 0);
    psi.set(2, 6, 0);
    psi.set(2, 7, 0);

    psi.set(3, 0, 0);
    psi.set(3, 1, 0);
    psi.set(3, 2, 0);
    psi.set(3, 3, 6);
    psi.set(3, 4, 0);
    psi.set(3, 5, 0);
    psi.set(3, 6, 0);
    psi.set(3, 7, 0);

    psi.set(4, 0, 1);
    psi.set(4, 1, t);
    psi.set(4, 2, pow(t, 2));
    psi.set(4, 3, pow(t, 3));
    psi.set(4, 4, pow(t, 4));
    psi.set(4, 5, pow(t, 5));
    psi.set(4, 6, pow(t, 6));
    psi.set(4, 7, pow(t, 7));

    psi.set(5, 0, 0);
    psi.set(5, 1, 1);
    psi.set(5, 2, 2. * t);
    psi.set(5, 3, 3. * pow(t, 2));
    psi.set(5, 4, 4. * pow(t, 3));
    psi.set(5, 5, 5. * pow(t, 4));
    psi.set(5, 6, 6. * pow(t, 5));
    psi.set(5, 7, 7. * pow(t, 6));

    psi.set(6, 0, 0);
    psi.set(6, 1, 0);
    psi.set(6, 2, 2);
    psi.set(6, 3, 6. * pow(t, 1));
    psi.set(6, 4, 12. * pow(t, 2));
    psi.set(6, 5, 20. * pow(t, 3));
    psi.set(6, 6, 30. * pow(t, 4));
    psi.set(6, 7, 42. * pow(t, 5));

    psi.set(7, 0, 0);
    psi.set(7, 1, 0);
    psi.set(7, 2, 0);
    psi.set(7, 3, 6);
    psi.set(7, 4, 24. * pow(t, 1));
    psi.set(7, 5, 60. * pow(t, 2));
    psi.set(7, 6, 120. * pow(t, 3));
    psi.set(7, 7, 210. * pow(t, 4));

    return psi;
}

Matrix<8, 3> constraintvectors(double x_0, double x_end, double y_0, double y_end, double theta_0, double theta_end, double v_dock) {
    Matrix<8, 3> v;

    v.set(0, 0, x_0);
    v.set(1, 0, 0);
    v.set(2, 0, 0);
    v.set(3, 0, 0);
    v.set(4, 0, x_end);
    v.set(5, 0, v_dock * cos(-theta_end));
    v.set(6, 0, 0);
    v.set(7, 0, 0);

    v.set(0, 1, y_0);
    v.set(1, 1, 0);
    v.set(2, 1, 0);
    v.set(3, 1, 0);
    v.set(4, 1, y_end);
    v.set(5, 1, v_dock * sin(-theta_end));
    v.set(6, 1, 0);
    v.set(7, 1, 0);

    v.set(0, 2, theta_0);
    v.set(1, 2, 0);
    v.set(2, 2, 0);
    v.set(3, 2, 0);
    v.set(4, 2, theta_end);
    v.set(5, 2, 0);
    v.set(6, 2, 0);
    v.set(7, 2, 0);

    return v;
}

double z_1(Matrix<8, 1> p, double t) {
    double z = 0;
    z += p.get(0, 0);
    z += p.get(1, 0) * pow(t, 1);
    z += p.get(2, 0) * pow(t, 2);
    z += p.get(3, 0) * pow(t, 3);
    z += p.get(4, 0) * pow(t, 4);
    z += p.get(5, 0) * pow(t, 5);
    z += p.get(6, 0) * pow(t, 6);
    z += p.get(7, 0) * pow(t, 7);
    return z;
}

double z_2(Matrix<8, 1> p, double t) {
    double z = 0;
    z += 1 * p.get(1, 0);
    z += 2 * p.get(2, 0) * pow(t, 1);
    z += 3 * p.get(3, 0) * pow(t, 2);
    z += 4 * p.get(4, 0) * pow(t, 3);
    z += 5 * p.get(5, 0) * pow(t, 4);
    z += 6 * p.get(6, 0) * pow(t, 5);
    z += 7 * p.get(7, 0) * pow(t, 6);
    return z;
}

double z_3(Matrix<8, 1> p, double t) {
    double z = 0;
    z += 2 * p.get(2, 0);
    z += 6 * p.get(3, 0) * pow(t, 1);
    z += 12 * p.get(4, 0) * pow(t, 2);
    z += 20 * p.get(5, 0) * pow(t, 3);
    z += 30 * p.get(6, 0) * pow(t, 4);
    z += 42 * p.get(7, 0) * pow(t, 5);
    return z;
}

double z_4(Matrix<8, 1> p, double t) {
    double z = 0;
    z += 6 * p.get(3, 0);
    z += 24 * p.get(4, 0) * pow(t, 1);
    z += 60 * p.get(5, 0) * pow(t, 2);
    z += 120 * p.get(6, 0) * pow(t, 3);
    z += 210 * p.get(7, 0) * pow(t, 4);
    return z;
}

template<int n>
Matrix<n, 1> get_feedforward_position(Matrix<8, 1> cv, double t) {

    Matrix<8, 1> params = (generateDataMatrix(t)).solve(cv);
    Matrix<n, 1> positions;

    for (int i = 0; i < n; i++) {

        double t_k = (((double) t) * ((double) i)) / ((double) n);
        positions.set(i, 0, z_1(params, t_k));

    }

    return positions;
}

template<int n>
Matrix<n, 1> get_feedforward_velocities(Matrix<8, 1> cv, double t) {

    Matrix<8, 1> params = (generateDataMatrix(t)).solve(cv);
    Matrix<n, 1> velocities;

    for (int i = 0; i < n; i++) {

        double t_k = (((double) t) * ((double) i)) / ((double) n);
        velocities.set(i, 0, z_2(params, t_k));

    }

    return velocities;
}

template<int n>
Matrix<n, 1> get_feedforward_acceleration(Matrix<8, 1> cv, double t) {

    Matrix<8, 1> params = (generateDataMatrix(t)).solve(cv);
    Matrix<n, 1> accelerations;

    for (int i = 0; i < n; i++) {

        double t_k = (((double) t) * ((double) i)) / ((double) n);
        accelerations.set(i, 0, z_3(params, t_k));

    }

    return accelerations;
}

template<int n>
Matrix<n, 3> get_feedforward_inputs(Matrix<8, 3> cvs, double t) {

    const double m = 7.5 + 0.296;
    const double j = 0.099 + 1.17e-4;

    Matrix<8, 1> cv_x = cvs.get_col(0);
    Matrix<8, 1> cv_y = cvs.get_col(1);
    Matrix<8, 1> cv_theta = cvs.get_col(2);

    Matrix<n, 1> acc_x = get_feedforward_acceleration<n>(cv_x, t);
    Matrix<n, 1> acc_y = get_feedforward_acceleration<n>(cv_y, t);
    Matrix<n, 1> acc_theta = get_feedforward_acceleration<n>(cv_theta, t);

    Matrix<n, 1> pos_theta = get_feedforward_position<n>(cv_theta, t);

    Matrix<n, 1> f_x;
    Matrix<n, 1> f_y;
    Matrix<n, 1> f_theta;

    for (int i = 0; i < n; i++) {

        double pos_theta_ = pos_theta.get(i, 0);
        double acc_x_ = acc_x.get(i, 0);
        double acc_y_ = acc_y.get(i, 0);
        double acc_theta_ = acc_theta.get(i, 0);

        double f_x_ = m * ( cos(pos_theta_) * acc_x_ + sin(pos_theta_) * acc_y_);
        double f_y_ = m * (-sin(pos_theta_) * acc_x_ + cos(pos_theta_) * acc_y_);
        double f_theta_ = j * acc_theta_;

        f_x.set(i, 0, f_x_);
        f_y.set(i, 0, f_y_);
        f_theta.set(i, 0, f_theta_);
    }

    Matrix<n, 3> inputs;

    for (int i = 0; i < n; i++) {

        inputs.set(i, 0, f_x.get(i, 0));
        inputs.set(i, 1, f_y.get(i, 0));
        inputs.set(i, 2, f_theta.get(i, 0));
    }

    return inputs;
}

template<int n>
Matrix<n, 6> get_feedforward_data(double x_0, double x_end, double y_0, double y_end, double theta_0, double theta_end, double v_dock, double t_end) {

    Matrix<8, 3> cv = constraintvectors(x_0, x_end, y_0, y_end, theta_0, theta_end, v_dock);

    Matrix<8, 1> cv_x = cv.get_col(0);
    Matrix<8, 1> cv_y = cv.get_col(1);
    Matrix<8, 1> cv_t = cv.get_col(2);
    
    Matrix<n, 1> ff_x_0 = get_feedforward_position<n>(cv_x, t_end);
    Matrix<n, 1> ff_y_0 = get_feedforward_position<n>(cv_y, t_end);
    Matrix<n, 1> ff_t_0 = get_feedforward_position<n>(cv_t, t_end);
    
    Matrix<n, 3> inputs = get_feedforward_inputs<n>(cv, t_end);

    Matrix<n, 6> data;
    for (int i = 0; i < n; i++) {

        data.set(i, 0, ff_x_0.get(i, 0));
        data.set(i, 1, ff_y_0.get(i, 0));
        data.set(i, 2, ff_t_0.get(i, 0));
        data.set(i, 3, inputs.get(i, 0));
        data.set(i, 4, inputs.get(i, 1));
        data.set(i, 5, inputs.get(i, 2));

    }

    return data;
}

template<int n>
Matrix<n, 9> get_feedforward_data2(double x_0, double x_end, double y_0, double y_end, double theta_0, double theta_end, double v_dock, double t_end) {

    Matrix<8, 3> cv = constraintvectors(x_0, x_end, y_0, y_end, theta_0, theta_end, v_dock);

    Matrix<8, 1> cv_x = cv.get_col(0);
    Matrix<8, 1> cv_y = cv.get_col(1);
    Matrix<8, 1> cv_t = cv.get_col(2);
    
    Matrix<n, 1> ff_x_0 = get_feedforward_position<n>(cv_x, t_end);
    Matrix<n, 1> ff_y_0 = get_feedforward_position<n>(cv_y, t_end);
    Matrix<n, 1> ff_t_0 = get_feedforward_position<n>(cv_t, t_end);
    
    Matrix<n, 1> ff_x_1 = get_feedforward_velocities<n>(cv_x, t_end);
    Matrix<n, 1> ff_y_1 = get_feedforward_velocities<n>(cv_y, t_end);
    Matrix<n, 1> ff_t_1 = get_feedforward_velocities<n>(cv_t, t_end);
    
    Matrix<n, 3> inputs = get_feedforward_inputs<n>(cv, t_end);

    Matrix<n, 9> data;
    for (int i = 0; i < n; i++) {

        data.set(i, 0, ff_x_0.get(i, 0));
        data.set(i, 1, ff_y_0.get(i, 0));
        data.set(i, 2, ff_t_0.get(i, 0));
        data.set(i, 3, inputs.get(i, 0));
        data.set(i, 4, inputs.get(i, 1));
        data.set(i, 5, inputs.get(i, 2));
        data.set(i, 6, ff_x_1.get(i, 0));
        data.set(i, 7, ff_y_1.get(i, 1));
        data.set(i, 8, ff_t_1.get(i, 2));

    }

    return data;
}
