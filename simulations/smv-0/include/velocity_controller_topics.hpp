#include "rodos.h"

struct desired_track {
    float vel_x;
    float vel_y;
    float vel_theta;
};

extern Topic<desired_track> desired_track_topic;
