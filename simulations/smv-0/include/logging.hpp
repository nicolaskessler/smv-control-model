#pragma once

#include "rodos.h"
#include <string>

#include "topics.h"

#define LOGGING_BUFFER_SIZE 8192

// CommBuffers

// extern CommBuffer<sPositionData> cbPositionData;
// extern CommBuffer<sControl> cbControl;
// extern CommBuffer<sHouseKeeping> cbHouseKeeping;

// extern Subscriber subPositionData;
// extern Subscriber subControl;
// extern Subscriber subHousekeeping;

// CSV Columns
/*
 * iteration
 * time_stamp
 * input_th_time_0
 * input_th_time_1
 * input_th_time_2
 * input_thruster_theta_0
 * input_thruster_theta_1
 * input_rpm
 * input_rw_time
 * output_pos_x
 * output_pos_y
 * output_theta
 * output_vel_x
 * output_vel_y
 * output_vel_theta
 * output_tank_value
 * output_battery_voltage
 * output_docking_status_left
 * output_docking_status_right
*/

class SMVCSVGenerator {

    public:

    struct Item {

        uint32_t iteration;
        int64_t time_stamp;
        int32_t input_th_time_0;
        int32_t input_th_time_1;
        int32_t input_th_time_2;
        int32_t input_thruster_theta_0;
        int32_t input_thruster_theta_1;
        int32_t input_rpm;
        int32_t input_rw_time;
        float output_pos_x;
        float output_pos_y;
        float output_theta;
        float output_vel_x;
        float output_vel_y;
        float output_vel_theta;
        int32_t output_tank_value;
        float output_battery_voltage;
        int32_t output_docking_status_left;
        int32_t output_docking_status_right;
        float output_motor_speed;

        float output_pos_x_2;
        float output_pos_y_2;
        float output_theta_2;

    };

    const char * generate_header() const noexcept;

    const char * generate_line(const Item item) const noexcept;

    const char * autolog() noexcept;

    private:

    char buffer[LOGGING_BUFFER_SIZE];

    const uint32_t buffer_size = LOGGING_BUFFER_SIZE;

    uint32_t iteration = 0;

};
