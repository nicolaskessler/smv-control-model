#include "rodos.h"

struct docking_command {
    float x; // ST X in mm Target
    float y; // ST Y in mm Target
    float theta; // ST Heading Target
    float velocity; // Final Velocity
    float t_float; // Float freely for this time in s after maneuver
    float t_end; // when docking should be reached, unused and always 15s.
    bool dock; // dock or just move? // unused
};

extern Topic<docking_command> DockingCommandTopic1;
extern Topic<docking_command> DockingCommandTopic2;
