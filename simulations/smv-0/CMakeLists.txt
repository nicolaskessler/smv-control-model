project("SMV-Simulator-0")

add_subdirectory(external/rodos)

add_subdirectory(external/messages)

add_subdirectory(external/topics)

add_subdirectory(external/beacon)

#add_subdirectory(external/star-tracker/SimulateStarTracker)
add_subdirectory(external/star-tracker-mock)

if(NOT DEFINED LVGLPORT)
  MESSAGE(FATAL_ERROR "Please specify -DLVGLPORT= for this project")
else()
  add_subdirectory(external/lvgl-port-${LVGLPORT})
endif()

add_library(position_controller_topics
  src/position_controller_topics.cpp
)

target_include_directories(position_controller_topics PUBLIC
  $<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}/include>
  $<INSTALL_INTERFACE:include>
)

target_link_libraries(position_controller_topics
  rodos
)

add_library(joystick
  external/joystick/joystick.cc
)

target_include_directories(joystick PUBLIC
  $<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}/external/joystick>
  $<INSTALL_INTERFACE:include>
)

add_library(system_model
  generated/system_model
)

target_include_directories(system_model PUBLIC
  $<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}/generated>
  $<INSTALL_INTERFACE:include>
)

add_library(system_model_dae
  generated_dae/system_model_dae
)

target_include_directories(system_model_dae PUBLIC
  $<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}/generated_dae>
  $<INSTALL_INTERFACE:include>
)

add_library(smv_topics
  topics/topics.cpp
)

target_include_directories(smv_topics PUBLIC
  $<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}/topics>
  $<INSTALL_INTERFACE:include>
)

target_link_libraries(smv_topics
  rodos
)

add_library(logging
  src/logging.cpp
)

target_include_directories(logging PUBLIC
  $<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}/include>
  $<INSTALL_INTERFACE:include>
)

target_link_libraries(logging
  smv_topics
  rodos
)

add_rodos_executable(logging_dae
  src/logging_dae.cpp
  src/gateway.cpp
)

target_link_libraries(logging_dae
  rodos
)

add_library(velocity-controller-topics
  src/velocity_controller_topics.cpp
)

target_include_directories(velocity-controller-topics PUBLIC
  $<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}/include>
  $<INSTALL_INTERFACE:include>
)

target_link_libraries(velocity-controller-topics
  rodos
)

add_rodos_executable(smv_simulator
  src/smv_model.cpp
  src/joystick_node.cpp
  src/gateway.cpp
# src/smv_velocity_controller.cpp
  src/smv_position_controller
  src/rw_velocity_control.cpp
)

target_link_libraries(smv_simulator
  kamaro-rodos-beacon
  joystick
  system_model
  smv_topics
  velocity-controller-topics
  smv_feedforward
  position_controller_topics
)

add_rodos_executable(smv_simulator_dae
  src/smv_model_dae.cpp
  src/joystick_node.cpp
  src/gateway.cpp
#  src/smv_velocity_controller.cpp
#  src/rw_velocity_control.cpp
)

target_link_libraries(smv_simulator_dae
  kamaro-rodos-beacon
  joystick
  system_model_dae
  smv_topics
  velocity-controller-topics
)

add_rodos_executable(smv_datagen
  src/smv_model.cpp
  src/auto-experiment.cpp
  src/gateway.cpp
  src/smv_velocity_controller.cpp
  src/rw_velocity_control.cpp
)

target_link_libraries(smv_datagen
  kamaro-rodos-beacon
  system_model
  smv_topics
  velocity-controller-topics
)

add_rodos_executable(smv_visualizer
#  src/smv_reader.cpp
  src/smv_visualizer.cpp
  src/gateway.cpp
)

target_link_libraries(smv_visualizer
  lvgl
  kamaro-rodos-beacon
  smv_topics
  position_controller_topics
)

add_rodos_executable(star_tracker
  src/star_tracker.cpp
  src/gateway.cpp
)

target_link_libraries(star_tracker
  star-tracker-lib
  kamaro-rodos-beacon
  smv_topics
)

add_rodos_executable(test_display
  src/test_display.cpp
#  src/gateway.cpp
)

target_link_libraries(test_display
  kamaro-rodos-beacon
  lvgl
)

add_rodos_executable(logger-on-linux
  src/logger-on-linux.cpp
  src/gateway.cpp
)

target_link_libraries(logger-on-linux
  kamaro-rodos-beacon
  logging
)

add_library(smv_feedforward
  src/smv_feedforward.cpp
)

target_include_directories(smv_feedforward PUBLIC
  $<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}/include>
  $<INSTALL_INTERFACE:include>
)

target_link_libraries(smv_feedforward
  rodos
)

add_rodos_executable(matrixtest
  src/matrix_tests
)

target_link_libraries(matrixtest
  kamaro-rodos-beacon
  smv_feedforward
)

add_rodos_executable(position_mock
  src/smv_position_controller
  src/gateway.cpp
)

target_link_libraries(position_mock
  kamaro-rodos-beacon
  smv_feedforward
  smv_topics
  position_controller_topics
)
