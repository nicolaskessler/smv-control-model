#!/bin/bash

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
cd "$DIR"

if [ ! -d "opencv4" ]; then
  git clone https://github.com/opencv/opencv.git opencv4
  git clone https://github.com/opencv/opencv_contrib.git
  cd opencv4
  mkdir install
  mkdir build
  cd build
  cmake \
  -DCMAKE_BUILD_TYPE=DEBUG \
  -DCMAKE_INSTALL_PREFIX="$DIR/opencv4/install" \
  -DOPENCV_EXTRA_MODULES_PATH="$DIR/opencv_contrib/modules" \
  -DWITH_IPP=ON \
  -DWITH_TBB=ON \
  -DWITH_OPENMP=ON \
  -DWITH_PTHREADS_PF=ON \
  -DWITH_CUDA=ON \
  -DENABLE_FAST_MATH=1 \
  -DCUDA_FAST_MATH=1 \
  -DWITH_CUBLAS=1 \
  ..
  make -j8 all
  make install
  cd "$DIR"
fi

rm -rf build32
mkdir build32
cd build32
export OpenCV_DIR="/usr/lib/x86_64-linux-gnu/cmake/opencv4/"
echo $OpenCV_DIR

cmake -DCMAKE_TOOLCHAIN_FILE=../external/rodos/cmake/port/linux-makecontext.cmake -DEXECUTABLE=ON -DLVGLPORT=linux -DENABLE_RODOS_RTTI=ON -Wno-dev ..
make smv_datagen smv_visualizer logger-on-linux

cd ..
rm -rf build64
mkdir build64
cd build64
export OpenCV_DIR="$DIR/opencv4/install"
echo $OpenCV_DIR

cmake -DCMAKE_TOOLCHAIN_FILE=../external/rodos/cmake/port/posix.cmake -DEXECUTABLE=ON -DLVGLPORT=linux -DENABLE_RODOS_RTTI=ON -Wno-dev ..
make star_tracker

# ./test_display
# exit 0

cd ..

xterm -e taskset -c 0 ./build32/smv_datagen &
xterm -e taskset -c 1 ./build32/smv_visualizer &
xterm -e taskset -c 1 ./build32/logger-on-linux &
cd build64
xterm -e taskset -c 2 ./star_tracker &

read  -n 1 -p "Press Enter to stop execution..." inputstr
kill -9 %1
kill -9 %2
kill -9 %3
kill -9 %4
killall -9 smv_datagen
killall -9 smv_visualizer
killall -9 logger-on-linux
killall -9 star_tracker

cd ..

mv /tmp/smv-* ./

# echo " "
# echo "generating Star Tracker's simulatedFrame.gif"
# convert -delay 20 -loop 0 simulatedFrame*.bmp simulatedFrame.gif
# cd ..
