#!/bin/bash

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
cd "$DIR"

rm external_old.tar.gz
tar -czvf external_old.tar.gz external
vcs pull external
