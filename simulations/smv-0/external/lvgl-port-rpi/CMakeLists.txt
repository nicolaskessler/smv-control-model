### LVGL ###

INCLUDE_DIRECTORIES(${CMAKE_CURRENT_SOURCE_DIR})
file(GLOB_RECURSE LGVL_INCLUDES "lv_drivers/*.h" "lv_examples/*.h"  "lvgl/*.h"  "./*.h" )
file(GLOB_RECURSE LVGL_SOURCES  "lv_drivers/*.c" "lv_examples/*.c"  "lvgl/*.c" )

add_library(lvgl
  ${LVGL_SOURCES}
  ${INCLUDES}
  ${CMAKE_CURRENT_SOURCE_DIR}/lvgl_init.c
)

target_include_directories(lvgl PUBLIC
  $<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}>
  $<INSTALL_INTERFACE:include>
)
