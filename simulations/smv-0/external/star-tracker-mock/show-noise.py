#!/usr/bin/env python3

import argparse
import numpy as np
import matplotlib.pyplot as plt

FLAGS = []


def main():

    global FLAGS

    # Argument parsing
    parser = argparse.ArgumentParser()
    parser.add_argument(
        '--filename',
        type=str,
        default="noise_map.npy",
        required=False,
        help='Output filename for .npy noise data')

    FLAGS = parser.parse_args()
    filename = FLAGS.filename

    noise = np.load(filename)
    noise_x = noise[0]
    noise_y = noise[1]
    noise_theta = noise[2]

    fig = plt.figure()
    ax = plt.subplot(1, 3, 1)
    im1 = ax.imshow(noise_x, cmap='gray', interpolation='lanczos')
    plt.colorbar(im1, ax=ax, orientation='vertical')
    ax = plt.subplot(1, 3, 2)
    im2 = ax.imshow(noise_y, cmap='gray', interpolation='lanczos')
    plt.colorbar(im2, ax=ax, orientation='vertical')
    ax = plt.subplot(1, 3, 3)
    im3 = ax.imshow(noise_theta, cmap='gray', interpolation='lanczos')
    plt.colorbar(im3, ax=ax, orientation='vertical')
    
    plt.show()


if __name__ == '__main__':
    main()
