#include <iostream>
#include <vector>
#include "SimulatedSensor.h"
#include "StarIdentification.h"

class StarTrackerSITL {

    public:

    StarTrackerSITL() {};

    void init(const std::string &fileName);

    void run(double &x_out, double &y_out, double &theta_out, double x, double y, double theta);

    private:

    SimulatedSensor sensor;
    StarIdentification *identifyStars;
    double *data;
    std::vector<size_t> s;
    std::vector<std::vector<double>> data_x;
    std::vector<std::vector<double>> data_y;
    std::vector<std::vector<double>> data_theta;

    double scale_x = 20.;
    double scale_y = 20.;
    double scale_theta = 0.15;

};
