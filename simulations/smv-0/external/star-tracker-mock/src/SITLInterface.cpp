#include "SITLInterface.hpp"
#include "cnpy.h"
#include <cmath>

void StarTrackerSITL::init(const std::string &fileName) {

    static cnpy::NpyArray arr = cnpy::npy_load(fileName);
    s = arr.shape;

    // assumption about shape: (3, n_x, n_y)
    double *data = arr.data<double>();
    assert(arr.word_size == sizeof(double));
    assert(arr.shape[0] == 3);

    for (size_t i = 0; i < s[1]; i++) {

        std::vector<double> row_x;
        std::vector<double> row_y;
        std::vector<double> row_theta;

        for (size_t j = 0; j < s[2]; j++) {
            row_x.emplace_back(data[0 * s[1] * s[2] + i * s[2] + j]);
            row_y.emplace_back(data[1 * s[1] * s[2] + i * s[2] + j]);
            row_theta.emplace_back(data[2 * s[1] * s[2] + i * s[2] + j]);
        }

        data_x.emplace_back(row_x);
        data_y.emplace_back(row_y);
        data_theta.emplace_back(row_theta);
    }
}

void StarTrackerSITL::run(double &x_out, double &y_out, double &theta_out, double x, double y, double theta) {

    std::cout << "shape: " << s[0] << " " << s[1] << " " << s[2] << std::endl;
    size_t idx_0 = (s[1] * s[2]);
    std::cout << idx_0 << std::endl;
    size_t idx_x = (x / 2000.) * ( (double)  s[1]);
    std::cout << idx_x << std::endl;
    size_t idx_y = (y / 2000.) * ( (double)  s[2]);
    std::cout << idx_y << std::endl;

    x_out = x;
    y_out = y;
    theta_out = theta;

    double dx = data_x[idx_x][idx_y];
    std::cout << dx << std::endl;
    double dy = data_y[idx_x][idx_y];
    std::cout << dy << std::endl;
    double dtheta = data_theta[idx_x][idx_y];
    std::cout << dtheta << std::endl;

    x_out += dx * scale_x;
    y_out += dy * scale_y;
    theta_out += dtheta * scale_theta - 3.141/2.;

    if (theta_out > M_PI) {
        theta_out -= 2 * M_PI;
    }
    if (theta_out < -M_PI) {
        theta_out += 2 * M_PI;
    }
}
