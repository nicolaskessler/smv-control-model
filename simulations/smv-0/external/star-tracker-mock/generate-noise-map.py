#!/usr/bin/env python3

import argparse
import numpy as np

from perlin_numpy import generate_perlin_noise_2d

FLAGS = []


def main():

    global FLAGS

    # Argument parsing
    parser = argparse.ArgumentParser()
    parser.add_argument(
        '--x_grid_pts',
        type=int,
        default=512,
        help='Number of x grid points. MULTIPLE of 8!')
    parser.add_argument(
        '--y_grid_pts',
        type=int,
        default=512,
        help='Number of y grid points. MULTIPLE of 8!')
    parser.add_argument(
        '--filename',
        type=str,
        default=None,
        required=True,
        help='Output filename for .npy noise data')

    FLAGS = parser.parse_args()

    num_x = FLAGS.x_grid_pts
    num_y = FLAGS.y_grid_pts
    filename = FLAGS.filename

    np.random.seed(0)
    x_noise = generate_perlin_noise_2d((512, 512), (8, 8))
    np.random.seed(1)
    y_noise = generate_perlin_noise_2d((512, 512), (8, 8))
    np.random.seed(2)
    theta_noise = generate_perlin_noise_2d((512, 512), (8, 8))
    noise = np.stack([x_noise, y_noise, theta_noise], 0)

    np.save(filename, noise, False)


if __name__ == '__main__':
    main()