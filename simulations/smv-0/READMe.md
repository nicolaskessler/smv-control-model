# RODOS SMV Simulation

RODOS SMV simulator

## Fetch Sources

```shell script
$ pip install vcstool
$ mkdir external
$ vcs import external < repos.txt
```

## Update Sources

```shell script
$ vcs pull external
```

## Build

```shell script
$ mkdir build
$ cd build
$ cmake -DCMAKE_TOOLCHAIN_FILE=../external/rodos/cmake/port/linux-makecontext.cmake -DEXECUTABLE=ON ..
```

## Project Layout / Design
See [The Pitchfork Layout PFL](https://api.csswg.org/bikeshed/?force=1&url=https://raw.githubusercontent.com/vector-of-bool/pitchfork/develop/data/spec.bs)

The dependency hierarchy is:
* RODOS
* Messages
* Topics
* Other nodes, like kamaro-rodos-beacon
* Little VGL
* The project
